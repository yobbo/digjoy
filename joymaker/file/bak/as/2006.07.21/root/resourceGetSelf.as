﻿class resourceGetSelf implements getResourceInterface {
	public var swfResource:Array;
	public var picResource:Array;
	public var musicResource:Array;
	public function resourceGetSelf() {
		picResource = _root.SELF_PICS.split(",");
		swfResource = _root.SELF_SWFS.split(",");
		musicResource = _root.SELF_MP3S.split(",");
		for (var i:Number = 0; i<picResource.length; i++) {
			picResource[i] = "mySelfResource\\pics\\"+picResource[i];
		}
		for (var i:Number = 0; i<swfResource.length; i++) {
			swfResource[i] = "mySelfResource\\swfs\\"+swfResource[i];
		}
		for (var i:Number = 0; i<musicResource.length; i++) {
			musicResource[i] = "mySelfResource\\mp3s\\"+musicResource[i];
		}
		picResource.pop();
		swfResource.pop();
		musicResource.pop();
	}
	public function getswfResource():Array {
		return swfResource;
	}
	public function getpicResource():Array {
		return picResource;
	}
	public function getmusicResource():Array {
		return musicResource;
	}
}
