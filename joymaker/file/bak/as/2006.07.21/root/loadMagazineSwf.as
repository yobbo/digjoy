﻿class loadMagazineSwf {
	var workAreaX:Number;
	var workAreaY:Number;
	public function loadMagazineSwf() {
		_root.workArea._lockroot = true;
		var mclListener:Object = new Object();
		mclListener.onLoadInit = function(target_mc:MovieClip) {
			fscommand("fullscreen", false);
			target_mc.face_mc.gotoAndStop(2);
			target_mc.face_mc.stencil_mc.gotoAndStop(2);
			target_mc.face_mc.stencil_mc.txt1 = 1;
			target_mc.face_mc.stencil_mc.txt2 = 2;
			workAreaX = 105;
			workAreaY = 35;
			_root.workArea._x = workAreaX;
			_root.workArea._y = workAreaY;
		};
		var image_mcl:MovieClipLoader = new MovieClipLoader();
		image_mcl.addListener(mclListener);
		image_mcl.loadClip(_root.loadStencil[0], _root.workArea);
		var setmm = setInterval(function () {
			if (_root.resource.backgroundUrl != "") {
				var mclListenerp:Object = new Object();
				mclListenerp.onLoadInit = function(targetp_mc:MovieClip) {
					var w:Number = targetp_mc._width;
					var h:Number = targetp_mc._height;
					targetp_mc._height = 768;
					targetp_mc._width = 1024;
				};
				var image_mclpp:MovieClipLoader = new MovieClipLoader();
				image_mclpp.addListener(mclListenerp);
				image_mclpp.loadClip(_root.resource.backgroundUrl, _root.workArea.face_mc.bg_mc);
			}
			var workAreaMask:MovieClip = _root.createEmptyMovieClip("workAreaMask", 30077);
			_root.workAreaMask.beginFill(0xFF0000, 20);
			_root.workAreaMask.lineStyle(1, 0xFF0000, 100, true, "none", "round", "round", 1);
			_root.workAreaMask.moveTo(0, 0);
			_root.workAreaMask.lineTo(800, 0);
			_root.workAreaMask.lineTo(800, 600);
			_root.workAreaMask.lineTo(0, 600);
			_root.workAreaMask.lineTo(0, 0);
			_root.workAreaMask.endFill();
			_root.workAreaMask._x = _root.workArea._x;
			_root.workAreaMask._y = _root.workArea._y;
			_root.workAreaMask.onEnterFrame = function() {
				this._x = _root.workArea._x;
				this._y = _root.workArea._y;
			};
			_root.workArea.setMask(_root.workAreaMask);
			clearInterval(setmm);
		}, 1000);
	}
}
