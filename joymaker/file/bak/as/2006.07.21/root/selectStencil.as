﻿class selectStencil {
	private var seleP:String;//定义项目选择
	private var selectMenu:Array = ["selectStencil_scollUp", "selectStencil_scollBottom", "stencil_pic","prButton","neButton"];
	private var nowPage:Number = 1;//定义现在的页码
	private var totalPage:Number;//定义总共页码
	private var totalStencil:Number;//定义总共模板数
	private var pST:Number;//记录以前的duplicateMC开始数用于删除
	private var pEN:Number;//记录以前的duplicateMC开始数用于删除
	
	private function getAllStencil(){
		var nowThis:selectStencil = this;
		var stylePath="/resource/"+seleP+"/style";
		var styleNodes:Array = mx.xpath.XPathAPI.selectNodeList(_root.xml.firstChild, stylePath);
		var styleHeight:Number=0;
		for(var i:Number=0;i<styleNodes.length;i++){
			_root.tumb_mc.tumbEY.attachMovie("styleS","styleS"+i,i);
			_root.tumb_mc.tumbEY["styleS"+i]._x=0;
			_root.tumb_mc.tumbEY["styleS"+i].styleName.text=styleNodes[i].attributes.styleName;
			_root.tumb_mc.tumbEY["styleS"+i]._y=styleHeight;
			styleHeight+=(_root.tumb_mc.tumbEY["styleS"+i]._height+5);
			_root.tumb_mc.tumbEY["styleS"+i].thisURL=styleNodes[i];
			_root.tumb_mc.tumbEY["styleS"+i].onRollOver=function(){
				this.gotoAndStop(2);
			}
			_root.tumb_mc.tumbEY["styleS"+i].onRollOut=function(){
				this.gotoAndStop(1);
			}
			_root.tumb_mc.tumbEY["styleS"+i].onRelease=function(){
				nowThis.getAllPageAndAllTemp(this.thisURL);
			}
			_root.tumb_mc.tumbEY["styles"+i].onReleaseOutside=function(){
				this.gotoAndStop(2);
			}

		}			
		if(styleNodes.length>0){
			getAllPageAndAllTemp(styleNodes[0]);
		}
	}
	public function selectStencil(nowProject:String){
		var shadowNow:flash.filters.DropShadowFilter = new flash.filters.DropShadowFilter(15, 90, 0x000000, 0.8, 16, 16, 1, 3, false, false, false);
		_root.zoom_area.filters = [shadowNow];
		_root.tumb_mc.filters = [shadowNow];
		zoomAsc();
		setInterval(this, "thumbAsc", 500);
		//以上将舞台上的物件显示出来，特效，最先显示
		seleP=nowProject;//将第二帧上选择的项目设置近来
		
		for(var i:Number=3;i<5;i++){
			_root[selectMenu[i]].onRollOver=function(){
				this.gotoAndStop(2);
			}
			_root[selectMenu[i]].onRollOut=function(){
				this.gotoAndStop(1);
			}
			_root[selectMenu[i]].onReleaseOutside=function(){
				this.gotoAndStop(1);
			}
			if(i==3){
				_root[selectMenu[i]].onRelease=function(){
					_root.gotoAndStop("loadStencil");
				}
			}else if(i==4){
				_root[selectMenu[i]].onRelease=function(){
					_root.gotoAndStop("beginWork");
				}
			}
		}//上一步与下一步开始起效果
		getAllStencil();
	}
	private function zoomAsc() {
		_root.zoom_area.onEnterFrame = function() {
			this._alpha += 15;
			if (this._alpha>100) {
				this._alpha = 100;
				delete this.onEnterFrame;
			}
		};
	}
	private function thumbAsc() {
		_root.tumb_mc.onEnterFrame = function() {
			this._alpha += 15;
			if (this._alpha>100) {
				this._alpha = 100;
				delete this.onEnterFrame;
			}
		};
	}
	//开始效果
	public function getAllPageAndAllTemp(nowStencil:Array){
		if(nowStencil.hasChildNodes){
			for(var i:Number=0;i<nowStencil.childNodes.length;i++){
				_root["stencil_"+i] = new Array();
				if(nowStencil.childNodes[i].hasChildNodes){
					for(var p:Number=0;p<nowStencil.childNodes[i].childNodes.length;p++){
						_root["stencil_"+i].push(nowStencil.childNodes[i].childNodes[p].firstChild.nodeValue);
					}
				}
			}
		}
		totalStencil = nowStencil.childNodes.length;//共有多少个模板
		totalPage = Math.floor(nowStencil.childNodes.length/15)+1;//共有多少页
		MCcolor();//按钮开始起作用
		MCsetPage();//按钮产生动作确定页码
		goPage(1);//马上到第一页
	}
	//获得总的模板数目
	private function MCcolor() {
		for (var sM:Number = 0; sM<selectMenu.length; sM++) {
			_root.tumb_mc[selectMenu[sM]].onRollOver = function() {
				this.gotoAndStop(2);
			};
			_root.tumb_mc[selectMenu[sM]].onRollOut = function() {
				this.gotoAndStop(1);
			};
			_root.tumb_mc[selectMenu[sM]].onReleaseOutside = function() {
				this.gotoAndStop(1);
			};
		}
	}
	//按钮外观动作
	private function MCsetPage() {
		var nowThis:selectStencil = this;
		_root.tumb_mc[selectMenu[0]].onRelease = function() {
			if (nowThis.nowPage>1) {
				nowThis.nowPage = nowThis.nowPage-1;
				nowThis.goPage(nowThis.nowPage);
			}
		};
		_root.tumb_mc[selectMenu[1]].onRelease = function() {
			if (nowThis.nowPage<nowThis.totalPage) {
				nowThis.nowPage = nowThis.nowPage+1;
				nowThis.goPage(nowThis.nowPage);
			}
		};
	}//按钮产生动作选择页码
	private function goPage(nowP:Number) {
		var textNowPage:String = nowP.toString();
		_root.tumb_mc.nowPage.text = textNowPage;
		var thisIDB:Number;//起始的数
		var thisIDE:Number;//结束的数
		var thisPageID:Number;//这页显示的数目
		nowPage = nowP;
		var goPageNowPage:Number = nowPage-1;
		thisIDB = goPageNowPage*15;
		thisIDE = goPageNowPage*15+15;
		if (thisIDE>totalStencil) {
			thisIDE = goPageNowPage*15+totalStencil%15;
		}
		thisPageID = thisIDE-thisIDB;
		getPage(thisPageID, thisIDB, thisIDE);
		
	}//将这页的多少模板，开始ID与结束ID传递给具体页排版
	private function getPage(nowS:Number, startIDB:Number, endIDB:Number) {
		var thisRow:Number = Math.ceil(nowS/5);//定义现在的这页列数;
		var thisPageStart:Number;//定义这页的开始;
		var thisPageEnd:Number;//定义这页的结束；
		var pic_y:Number = 5;
		clearPST(startIDB,endIDB);
		for (var i:Number = 0; i<thisRow; i++) {
			var pic_x:Number = 10;
			thisPageStart = startIDB+i*5;
			thisPageEnd = startIDB+i*5+5;
			if (thisPageEnd>endIDB) {
				thisPageEnd = startIDB+i*5+nowS%5;
			}
		
			for (var t:Number = thisPageStart; t<thisPageEnd; t++) {
				_root.tumb_mc.attachMovie("stencil_pic","dpNM"+t,t);
				_root.tumb_mc["dpNM"+t].picLoad_Empty.PE.loadMovie(_root["stencil_"+t][2]);
				_root.tumb_mc["dpNM"+t]._y = pic_y;
				_root.tumb_mc["dpNM"+t]._x = pic_x;
				_root.tumb_mc["dpNM"+t].onRollOver = function() {
					this.gotoAndStop(2);
				};
				_root.tumb_mc["dpNM"+t].onRollOut = function() {
					this.gotoAndStop(1);
				};
				_root.tumb_mc["dpNM"+t].doWhild = t;
				_root.tumb_mc["dpNM"+t].onRelease = function() {
					_root.loadStencil = [_root["stencil_"+this.doWhild][0], _root["stencil_"+this.doWhild][3]];
					_root.zoom_area.picLBIG.loadMovie(_root["stencil_"+this.doWhild][1]);
				};
				_root.tumb_mc["dpNM"+t].onReleaseOutside = function() {
					this.gotoAndStop(1);
				};
				pic_x += 90;
			}

			pic_y += 135;
		}
		_root.loadStencil = [_root["stencil_"+0][0], _root["stencil_"+0][3]];
		_root.zoom_area.picLBIG.loadMovie(_root["stencil_"+0][1]);
		getPS(startIDB, endIDB);
	}
	private function getPS(startIB:Number, endIB:Number) {
		pST = startIB;
		pEN = endIB;
	}//取的先前的页中数目
	private function clearPST(startIB:Number, endIB:Number) {
		for (var ii:Number = pST; ii<pEN; ii++) {
			_root.tumb_mc["dpNM"+ii].removeMovieClip();
		}
	}//删除先前的页中数目
}
