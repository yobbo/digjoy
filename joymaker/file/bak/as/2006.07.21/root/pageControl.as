﻿import arrays;
import listObject;
import workAreaPlay;
import assistant;
import basicFunction;
import exportXML;
import helpPanelList;
class pageControl extends rmcName {
	public var maxPage:Number = 60;
	//确定最大的页码数，用户定义的页码数不能超过此页码数；
	public var maxPageCycle:Number;
	//定义总共的页码轮数；
	public var nowPageCycle:Number;
	//定义现在的页码轮数；
	public var maxPageNumber:Number;
	//定义现在的页码总数；
	public var nowPage:Number;
	//定义现在的页码数；
	private var pST:Number;
	//记录以前的duplicateMC开始数用于删除
	private var pEN:Number;
	//记录以前的duplicateMC开始数用于删除
	public function pageControl() {
		var nowThis:pageControl = this;
		nowPageCycle = 1;
		nowPage = 1;
		calPage();
		//计算总共多少页码轮数，开始执行页码轮数转换；
		beginStartADPage();
		//开始加页减页的动作；
		_root[nP[0]][nP[1]].onRelease = function() {
			if (_root["maxPageArr"].length<80) {
				this.gotoAndStop(1);
				nowThis.addOnePage(_root.thisPage.nowPage);
			} else {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.alert("您进行的操作大于80页"+newline+"请妥善保存您的文件，以便于下次操作");
			}
		};
		_root[nP[0]][nP[2]].onRelease = function() {
			if (_root.thisPage.nowPage == 0) {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.alert("不能删除封面"+newline+"请跳转到其它页码进行删除");
			} else if (_root.thisPage.nowPage == (_root["maxPageArr"].length-1)) {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.alert("不能删除封底"+newline+"请跳转到其它页码进行删除");
			} else {
				this.gotoAndStop(1);
				nowThis.deleteOnePage(_root.thisPage.nowPage);
			}
		};
		for (var i:Number = 1; i<3; i++) {
			_root[nP[0]][nP[i]].arrayP = i;
			_root[nP[0]][nP[i]].onRollOver = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.getHelp(nowThis.nP[this.arrayP]);
				this.gotoAndStop(2);
			};
			_root[nP[0]][nP[i]].onRollOut = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.deleteTip();
				this.gotoAndStop(1);
			};
			_root[nP[0]][nP[i]].onReleaseOutside = function() {
				this.gotoAndStop(1);
			};
			_root[nP[0]][nP[i]].onPress = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.deleteTip();
				this.gotoAndStop(3);
			};
		}
	}
	public function addOnePage(getNowPage:Number):Void {
		var arrLength:Number = _root["maxPageArr"].length;
		_root["page"+arrLength] = new Array();
		_root["maxPageArr"].push(_root["page"+arrLength]);
		for (var i:Number = arrLength; i>getNowPage; i--) {
			for (var t:Number = _root["page"+(i-1)].length-1; t>=0; t--) {
				_root["page"+i][t] = _root["page"+(i-1)].pop();
			}
		}
		calPage();
	}
	public function deleteOnePage(getNowPage:Number):Void {
		var arrLength:Number = _root["maxPageArr"].length;
		var newlistObject:listObject = new listObject();
		newlistObject.listObjectClear();
		newlistObject.clearPageArray();
		for (var i:Number = getNowPage; i<arrLength-1; i++) {
			for (var t:Number = _root["page"+(i+1)].length-1; t>=0; t--) {
				_root["page"+i][t] = _root["page"+(i+1)].pop();
			}
		}
		trace(_root["maxPageArr"].pop());
		calPage();
	}
	public function getMaxPageNumber():Number {
		var newarrays:arrays = new arrays();
		maxPageNumber = newarrays.getMaxPageArr().length;
		//新建立一个 arrays 对象，从里面的 maxPageArr 获得目前最大的页码；
		return maxPageNumber;
	}
	//返回最大的页码数；
	public function calPage() {
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
		maxPageCycle = Math.floor(getMaxPageNumber()/10)+1;
		//算出最大的页码轮；
		setPageNumButton();
		//按钮动作开始；
		goPageCycle(nowPageCycle);
		//开始页码列表了；
		goPage(nowPage);
		//现在的页码开始跳转；
	}
	//计算出最大的页码轮数，并且开始进行按钮动作；
	private function goPageCycle(pagenu:Number) {
		var thisIDB:Number;
		//起始的数
		var thisIDE:Number;
		//结束的数
		var thisPageID:Number;
		//这页轮显示的数目
		var goPageNowPage:Number = pagenu-1;
		//将目前的页码数减去1就是该页的数了；
		thisIDB = goPageNowPage*10;
		thisIDE = goPageNowPage*10+10;
		if (thisIDE>getMaxPageNumber()) {
			thisIDE = goPageNowPage*10+getMaxPageNumber()%10;
		}
		thisPageID = thisIDE-thisIDB;
		getPage(thisPageID, thisIDB, thisIDE);
	}
	//算出该页应该存在的数目；
	private function getPage(nowS:Number, startIDB:Number, endIDB:Number):Void {
		clearPST(startIDB, endIDB);
		var nowThis:pageControl = this;
		var thisRow:Number = Math.ceil(nowS/2);
		//定义现在的这页列数;
		var thisPageStart:Number;
		//定义这页的开始;
		var thisPageEnd:Number;
		//定义这页的结束；
		var pic_y:Number = 80;
		//定义高度；
		for (var i:Number = 0; i<thisRow; i++) {
			var pic_x:Number = 10;
			thisPageStart = startIDB+i*2;
			thisPageEnd = startIDB+i*2+2;
			if (thisPageEnd>endIDB) {
				thisPageEnd = startIDB+i*2+nowS%2;
			}
			for (var t:Number = thisPageStart; t<thisPageEnd; t++) {
				_root[nP[0]].attachMovie("selectPage", "dpNM"+t, t);
				_root[nP[0]]["dpNM"+t]._y = pic_y;
				_root[nP[0]]["dpNM"+t]._x = pic_x;
				_root[nP[0]]["dpNM"+t].realPageName = t;
				var pageN = t+1;
				var newarrays = new arrays();
				if (pageN == 1) {
					if (_root.sStencil.select == "magazine") {
						_root[nP[0]]["dpNM"+t].pageTextN.text = "封面";
					} else {
						_root[nP[0]]["dpNM"+t].pageTextN.text = "封皮";
					}
				} else if (pageN == (newarrays.getMaxPageArr().length)) {
					if (_root.sStencil.select == "magazine") {
						_root[nP[0]]["dpNM"+t].pageTextN.text = "封底";
					} else {
						_root[nP[0]]["dpNM"+t].pageTextN.text = "封底";
					}
				} else {
					_root[nP[0]]["dpNM"+t].pageTextN.text = pageN+"页";
				}
				//显示的时候为 +1 的值；
				_root[nP[0]]["dpNM"+t].onRollOver = function() {
					if (nowThis.nowPage != this.realPageName) {
						this.gotoAndStop(2);
					}
				};
				_root[nP[0]]["dpNM"+t].onRollOut = function() {
					if (nowThis.nowPage != this.realPageName) {
						this.gotoAndStop(1);
					}
				};
				_root[nP[0]]["dpNM"+t].onRelease = function() {
					if (nowThis.nowPage != this.realPageName) {
						_root[nowThis.nP[0]]["dpNM"+nowThis.nowPage].gotoAndStop(1);
						nowThis.goPage(this.realPageName);
						nowThis.nowPage = this.realPageName;
						this.gotoAndStop(3);
					}
				};
				_root[nP[0]]["dpNM"+t].onReleaseOutside = function() {
					if (nowThis.nowPage != this.realPageName) {
						this.gotoAndStop(1);
					}
				};
				if (nowThis.nowPage == _root[nP[0]]["dpNM"+t].realPageName) {
					_root[nP[0]]["dpNM"+_root[nP[0]]["dpNM"+t].realPageName].gotoAndStop(3);
				}
				pic_x += 40;
			}
			pic_y += 40;
		}
		getPS(startIDB, endIDB);
	}
	//开始列表了，以5排的方式一排2列的方式开始排列;
	private function getPS(startIB:Number, endIB:Number) {
		pST = startIB;
		pEN = endIB;
	}
	//取的先前的页中数目
	private function clearPST(startIB:Number, endIB:Number) {
		for (var i:Number = pST; i<pEN; i++) {
			_root[nP[0]]["dpNM"+i].removeMovieClip();
		}
	}
	//删除先前的页中数目
	private function setPageNumButton():Void {
		var nowThis:pageControl = this;
		for (var i:Number = 3; i<5; i++) {
			_root[nP[0]][nP[i]].arrayP = i;
			_root[nP[0]][nP[i]].onRollOver = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.getHelp(nowThis.nP[this.arrayP]);
				this.gotoAndStop(2);
			};
			_root[nP[0]][nP[i]].onRollOut = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.deleteTip();
				this.gotoAndStop(1);
			};
			_root[nP[0]][nP[i]].onReleaseOutside = function() {
				this.gotoAndStop(1);
			};
		}
		_root[nP[0]][nP[3]].onRelease = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			if (nowThis.nowPageCycle>1) {
				nowThis.nowPageCycle = nowThis.nowPageCycle-1;
				nowThis.goPageCycle(nowThis.nowPageCycle);
				//进入前一轮；
			}
		};
		_root[nP[0]][nP[4]].onRelease = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			if (nowThis.nowPageCycle<nowThis.maxPageCycle) {
				nowThis.nowPageCycle = nowThis.nowPageCycle+1;
				nowThis.goPageCycle(nowThis.nowPageCycle);
				//进入后一轮；
			}
		};
	}
	//页码轮数跳转按钮动作；
	private function beginStartADPage():Void {
		var nowThis:pageControl = this;
		for (var i:Number = 1; i<3; i++) {
			_root[nP[0]][nP[i]].onRollOver = function() {
				this.gotoAndStop(2);
			};
			_root[nP[0]][nP[i]].onRollOut = function() {
				this.gotoAndStop(1);
			};
			_root[nP[0]][nP[i]].onReleaseOutside = function() {
				this.gotoAndStop(1);
			};
			_root[nP[0]][nP[i]].onPress = function() {
				this.gotoAndStop(3);
			};
		}
	}
	private function goPage(page:Number) {
		var newlistObject:listObject = new listObject();
		newlistObject.listObjectClear();
		//先执行清除；
		_root[nP[0]]["dpNM"+page].gotoAndStop(3);
		_root.thisPage.nowPage = page;
		//设置主场景上的当前页为目前页；
		newlistObject.listObjectThis();
		//再执行建立；
		var newworkAreaPlay:workAreaPlay = new workAreaPlay();
		newworkAreaPlay.goWorkAreaPlay();
		//页面变换;
		var newassistant:assistant = new assistant();
		newassistant.clearreopqs();
		//删除物件包围的匡;
	}
}
