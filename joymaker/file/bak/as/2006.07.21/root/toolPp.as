﻿import assistant;
import exportXML;
class toolPp extends rmcName {
	private var menu:Array = ["moveTop", "movePre", "moveNext", "moveBottom", "clearShape", "deleteMC", "setBg"];
	public function toolPp() {
	}
	public function setToolPp() {
		var nowThis:toolPp = this;
		_root[pP[0]][pP[3]].attachMovie("objectPM", "objectPM", 0);
		_root[pP[0]][pP[3]].objectPM[menu[6]]._alpha = 0;
		if (_root["page"+_root.thisPage.nowPage][_root.toolState.nowObject.id][0] == "load") {
			var getChart:String;
			var startChart:Number;
			startChart = _root["page"+_root.thisPage.nowPage][_root.toolState.nowObject.id][5].lastIndexOf(".");
			getChart = _root["page"+_root.thisPage.nowPage][_root.toolState.nowObject.id][5].slice(startChart+1, _root["page"+_root.thisPage.nowPage][_root.toolState.nowObject.id][5].length);
			if (getChart == "jpg" || getChart == "jpeg" || getChart == "gif" || getChart == "png") {
				_root[pP[0]][pP[3]].objectPM[menu[6]]._alpha = 100;
			}
		}
		var id = setInterval(function () {
			_root[nowThis.pP[0]][nowThis.pP[3]].objectPM.nalpha.value = _root.toolState.nowObject._alpha;
			clearInterval(id);
		}, 10);
		for (var i:Number = 0; i<7; i++) {
			_root[pP[0]][pP[3]].objectPM[menu[i]].onRollOver = function() {
				this.gotoAndStop(2);
			};
			_root[pP[0]][pP[3]].objectPM[menu[i]].onRollOut = function() {
				this.gotoAndStop(1);
			};
			_root[pP[0]][pP[3]].objectPM[menu[i]].onReleaseOutside = function() {
				this.gotoAndStop(1);
			};
			_root[pP[0]][pP[3]].objectPM[menu[i]].onPress = function() {
				this.gotoAndStop(3);
			};
		}
		_root[pP[0]][pP[3]].objectPM[menu[6]].onRelease = function() {
			_root.resource.backgroundUrl = _root["page"+_root.thisPage.nowPage][_root.toolState.nowObject.id][5];
			var mclListener:Object = new Object();
			mclListener.onLoadInit = function(target_mc:MovieClip) {
				var w:Number = target_mc._width;
				var h:Number = target_mc._height;
				target_mc._height = 768;
				target_mc._width = 1024;
			};
			var image_mcl:MovieClipLoader = new MovieClipLoader();
			image_mcl.addListener(mclListener);
			image_mcl.loadClip(_root["page"+_root.thisPage.nowPage][_root.toolState.nowObject.id][5], _root.workArea.face_mc.bg_mc);
			var setMC:MovieClip = _root.toolState.nowObject;
			delete _root["page"+_root.thisPage.nowPage][setMC.id];
			setMC.removeMovieClip();
			var newassistant:assistant = new assistant();
			newassistant.clearreopqs();
			var newexportXML:exportXML = new exportXML();
			_root.XMLSTRING = newexportXML.exportRunXML();
			_root.FILELIST = newexportXML.exportFileListXML();
			_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
		};
		_root[pP[0]][pP[3]].objectPM[menu[0]].onRelease = function() {
			var totalLayer:Number = _root["page"+_root.thisPage.nowPage].length;
			var setMC:MovieClip = _root.toolState.nowObject;
			//定义目前所选择的mc;
			for (var i:Number = setMC.getDepth(); i<totalLayer; i++) {
				var newMC:MovieClip = _root.objectInner.getInstanceAtDepth(i);
				setMC.swapDepths(newMC);
				_root["page"+_root.thisPage.nowPage][newMC.id][6] = newMC.getDepth();
				_root["page"+_root.thisPage.nowPage][setMC.id][6] = setMC.getDepth();
			}
			var newexportXML:exportXML = new exportXML();
			_root.XMLSTRING = newexportXML.exportRunXML();
			_root.FILELIST = newexportXML.exportFileListXML();
			_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
		};
		_root[pP[0]][pP[3]].objectPM[menu[1]].onRelease = function() {
			var setMC:MovieClip = _root.toolState.nowObject;
			//定义目前所选择的mc;
			var newMC:MovieClip = _root.objectInner.getInstanceAtDepth(setMC.getDepth()+1);
			setMC.swapDepths(newMC);
			_root["page"+_root.thisPage.nowPage][newMC.id][6] = newMC.getDepth();
			_root["page"+_root.thisPage.nowPage][setMC.id][6] = setMC.getDepth();
			var newexportXML:exportXML = new exportXML();
			_root.XMLSTRING = newexportXML.exportRunXML();
			_root.FILELIST = newexportXML.exportFileListXML();
			_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
		};
		_root[pP[0]][pP[3]].objectPM[menu[2]].onRelease = function() {
			var setMC:MovieClip = _root.toolState.nowObject;
			//定义目前所选择的mc;
			var newMC:MovieClip = _root.objectInner.getInstanceAtDepth(setMC.getDepth()-1);
			setMC.swapDepths(newMC);
			_root["page"+_root.thisPage.nowPage][newMC.id][6] = newMC.getDepth();
			_root["page"+_root.thisPage.nowPage][setMC.id][6] = setMC.getDepth();
			var newexportXML:exportXML = new exportXML();
			_root.XMLSTRING = newexportXML.exportRunXML();
			_root.FILELIST = newexportXML.exportFileListXML();
			_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
		};
		_root[pP[0]][pP[3]].objectPM[menu[3]].onRelease = function() {
			var totalLayer:Number = _root["page"+_root.thisPage.nowPage].length;
			var setMC:MovieClip = _root.toolState.nowObject;
			//定义目前所选择的mc;
			for (var i:Number = setMC.getDepth(); i>=0; i--) {
				var newMC:MovieClip = _root.objectInner.getInstanceAtDepth(i);
				setMC.swapDepths(newMC);
				_root["page"+_root.thisPage.nowPage][newMC.id][6] = newMC.getDepth();
				_root["page"+_root.thisPage.nowPage][setMC.id][6] = setMC.getDepth();
			}
			var newexportXML:exportXML = new exportXML();
			_root.XMLSTRING = newexportXML.exportRunXML();
			_root.FILELIST = newexportXML.exportFileListXML();
			_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
		};
		_root[pP[0]][pP[3]].objectPM[menu[4]].onRelease = function() {
			var setMC:MovieClip = _root.toolState.nowObject;
			//定义目前所选择的mc;
			setMC["object"]._xscale = 100;
			setMC["object"]._yscale = 100;
			setMC._rotation = 0;
			setMC["object"]._x = -setMC["object"]._width/2;
			setMC["object"]._y = -setMC["object"]._height/2;
			_root["page"+_root.thisPage.nowPage][setMC.id][9] = setMC["object"]._xscale;
			_root["page"+_root.thisPage.nowPage][setMC.id][10] = setMC["object"]._yscale;
			_root["page"+_root.thisPage.nowPage][setMC.id][7] = setMC._rotation;
			var newassistant:assistant = new assistant();
			newassistant.clearreopqs();
			//删除物件包围的匡;
			var newexportXML:exportXML = new exportXML();
			_root.XMLSTRING = newexportXML.exportRunXML();
			_root.FILELIST = newexportXML.exportFileListXML();
			_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
		};
		_root[pP[0]][pP[3]].objectPM[menu[5]].onRelease = function() {
			var setMC:MovieClip = _root.toolState.nowObject;
			delete _root["page"+_root.thisPage.nowPage][setMC.id];
			setMC.removeMovieClip();
			var newassistant:assistant = new assistant();
			newassistant.clearreopqs();
			var newexportXML:exportXML = new exportXML();
			_root.XMLSTRING = newexportXML.exportRunXML();
			_root.FILELIST = newexportXML.exportFileListXML();
			_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
		};
	}
	public function clearToolPp() {
		_root[pP[0]][pP[3]].objectPM.removeMovieClip();
	}
}
