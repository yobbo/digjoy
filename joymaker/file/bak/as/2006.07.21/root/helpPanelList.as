﻿class helpPanelList extends rmcName {
	public function helpPanelList() {
	}
	public function setHelpPanel() {
		var nowThis:helpPanelList = this;
		var idhelp = setInterval(function () {
			if (_root.toolState.nowTool == nowThis.tP[1]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = "选择工具，在工作区点击鼠标左键选择物件并可拖动物件";
			} else if (_root.toolState.nowTool == nowThis.tP[2]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = "缩放工具，在工作区点击鼠标左键选择物件可缩放、拖拉、旋转";
			} else if (_root.toolState.nowTool == nowThis.tP[3]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = "平移工具，移动工作区位置";
			} else if (_root.toolState.nowTool == nowThis.tP[4]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tP[5]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = "文字工具，在工作区点击建立新文字，并进行缩放与改变文字";
			} else if (_root.toolState.nowTool == nowThis.tP[6]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tP[7]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tP[8]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tP[9]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tP[10]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = "画圆工具，在工作拖动画出圆形";
			} else if (_root.toolState.nowTool == nowThis.tP[11]) {
				_root[nowThis.hP[0]][nowThis.hP[1]].text = "画矩形工具，在工作拖动画出矩形";
			}
		}, 10);
	}
	public function getHelp(p:String) {
		var nowThis:helpPanelList = this;
		if (p == tP[1]) {
			showTip("选择工具");
		} else if (p == tP[2]) {
			showTip("缩放工具");
		} else if (p == tP[3]) {
			showTip("平移工作区工具");
		} else if (p == tP[4]) {
			showTip("放大缩小工作区工具");
		} else if (p == tP[5]) {
			showTip("文本工具");
		} else if (p == tP[6]) {
			showTip("素材图片工具");
		} else if (p == tP[7]) {
			showTip("素材Flash工具");
		} else if (p == tP[8]) {
			showTip("关闭当前面板");
		} else if (p == tP[9]) {
			showTip("拖动面板四周可移动面板");
		} else if (p == tP[10]) {
			showTip("画圆工具");
		} else if (p == tP[11]) {
			showTip("画矩形工具");
		} else if (p == tP[12]) {
			showTip("版权声明");
		} else if (p == nP[1]) {
			showTip("添加页码到当前页之前");
		} else if (p == nP[2]) {
			showTip("删除页码");
		} else if (p == nP[3]) {
			showTip("翻到前10页");
		} else if (p == nP[4]) {
			showTip("翻到后10页");
		} else if (p == nP[5]) {
			showTip("关闭当前面板");
		} else if (p == nP[6]) {
			showTip("拖动面板四周可移动面板");
		} else if (p == rP[1]) {
			showTip("");
		} else if (p == rP[2]) {
			showTip("图片类型素材");
		} else if (p == rP[3]) {
			showTip("Flash类型素材");
		} else if (p == rP[4]) {
			showTip("音乐类型素材");
		} else if (p == rP[5]) {
			showTip("上一页素材");
		} else if (p == rP[6]) {
			showTip("下一页素材");
		} else if (p == rP[7]) {
			showTip("关闭此面板");
		} else if (p == rP[8]) {
			showTip("拖动面板四周可移动面板");
		} else if (p == rP[9]) {
			showTip("用户添加素材");
		}
	}
	public function showTip(tip:String) {
		_root.createEmptyMovieClip("showTip", 60000);
		_root.showTip.createEmptyMovieClip("textF", 1);
		_root.showTip.createEmptyMovieClip("bg", 0);
		_root.showTip.textF.createTextField("my_sb", 0, 5, 5, 500, 20);
		_root.showTip.textF.my_sb.text = tip;
		_root.showTip.textF.my_sb.selectable = false;
		_root.showTip.textF.my_sb._width = _root.showTip.textF.my_sb.textWidth+10;
		_root.showTip.bg.beginFill(0xFFFFCC, 100);
		_root.showTip.bg.lineStyle(1, 0x0099CC, 100, true, "none", "round", "round", 2);
		_root.showTip.bg.moveTo(0, 0);
		_root.showTip.bg.lineTo(_root.showTip.textF.my_sb.textWidth+10, 0);
		_root.showTip.bg.lineTo(_root.showTip.textF.my_sb.textWidth+10, _root.showTip.textF.my_sb.textHeight+10);
		_root.showTip.bg.lineTo(0, _root.showTip.textF.my_sb.textHeight+10);
		_root.showTip.bg.lineTo(0, 0);
		_root.showTip.bg.endFill();
		_root.showTip._x = _root._xmouse;
		_root.showTip._y = _root._ymouse;
	}
	public function deleteTip(tip:String) {
		_root.showTip.removeMovieClip();
	}
}
