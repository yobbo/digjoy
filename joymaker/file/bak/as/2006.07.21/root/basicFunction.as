﻿class basicFunction {
	public function basicFunction() {
	}
	public function alert(alert:String):Void {
		_root.createEmptyMovieClip("alert", 50000);
		_root.alert.createEmptyMovieClip("bg", 0);
		_root.alert.createEmptyMovieClip("texta", 1);
		_root.alert.createEmptyMovieClip("buttona", 2);
		_root.alert._x = Stage.width/2;
		_root.alert._y = Stage.height/2;
		_root.alert.bg.beginFill(000000, 60);
		_root.alert.bg.lineStyle(1, 000000, 100, true, "none", "round", "round", 1);
		_root.alert.bg.moveTo(0, 0);
		_root.alert.bg.lineTo(Stage.width, 0);
		_root.alert.bg.lineTo(Stage.width, Stage.height);
		_root.alert.bg.lineTo(0, Stage.height);
		_root.alert.bg.lineTo(0, 0);
		_root.alert.bg.endFill();
		_root.alert.bg._x = -Stage.width/2;
		_root.alert.bg._y = -Stage.height/2;
		_root.alert.bg.onRollOver = function() {
		};
		_root.alert.texta.createClassObject(mx.controls.TextArea, "alertText", 0);
		_root.alert.texta.alertText.setSize(500, 200);
		_root.alert.texta.alertText.text = alert;
		_root.alert.texta.alertText.wordWrap = true;
		_root.alert.texta.alertText.multiline = true;
		_root.alert.texta._x = -_root.alert.texta._width/2;
		_root.alert.texta._y = -_root.alert.texta._height;
		_root.alert.buttona.createClassObject(mx.controls.Button, "okb", 0);
		_root.alert.buttona.okb.label = "确定";
		_root.alert.buttona._x = -_root.alert.buttona.okb.width/2;
		_root.alert.buttona._y = _root.alert.texta._height-150;
		_root.alert.buttona.okb.onRelease = function() {
			this._parent._parent.removeMovieClip();
		};
	}
	public function setMouseD():Void {
		_root.createEmptyMovieClip("alerts", 55000);
		_root.alerts.onEnterFrame = function() {
			_root.mouseMC.nowA._alpha = 0;
			Mouse.show();
		};
	}
	public function getMouseD():Void {
		delete _root.alerts.onEnterFrame;
		_root.alerts.removeMovieClip();
	}
	public function startDRECT():Void {
		_root.objectInner.createEmptyMovieClip("dragRect", 80000);
		var nx:Number = _root._xmouse-_root.workArea._x;
		var ny:Number = _root._ymouse-_root.workArea._y;
		_root.objectInner["dragRect"].nxx = nx;
		_root.objectInner["dragRect"].nyy = ny;
		_root.objectInner["dragRect"].onEnterFrame = function() {
			this.createEmptyMovieClip("Rect", 0);
			this.Rect.beginFill(0x00CCFF, 0);
			this.Rect.lineStyle(1, 0x00CCFF, 100, true, "none", "round", "round", 1);
			this.Rect.moveTo(this.nxx, this.nyy);
			this.Rect.lineTo(_root._xmouse-_root.workArea._x, this.nyy);
			this.Rect.lineTo(_root._xmouse-_root.workArea._x, _root._ymouse-_root.workArea._y);
			this.Rect.lineTo(this.nxx, _root._ymouse-_root.workArea._y);
			this.Rect.lineTo(this.nxx, this.nyy);
			this.Rect.endFill();
			this.widthis = this.Rect._width;
			this.heights = this.Rect._height;
			var xT:String;
			if ((_root._xmouse-_root.workArea._x)<this.nxx) {
				xT = "false";
			} else {
				xT = "True";
			}
			this.xxT = xT;
			var yT:String;
			if ((_root._ymouse-_root.workArea._y)<this.nyy) {
				yT = "false";
			} else {
				yT = "True";
			}
			this.yyT = yT;
		};
	}
	public function stopDRECT():Array {
		var stopDRECT_Array:Array;
		delete _root.objectInner["dragRect"].onEnterFrame;
		if (_root.objectInner["dragRect"].xxT == "false") {
			var xx:Number = _root.objectInner["dragRect"].nxx-_root.objectInner["dragRect"].widthis/2;
		} else {
			var xx:Number = _root.objectInner["dragRect"].nxx+_root.objectInner["dragRect"].widthis/2;
		}
		if (_root.objectInner["dragRect"].yyT == "false") {
			var yy:Number = _root.objectInner["dragRect"].nyy-_root.objectInner["dragRect"].heights/2;
		} else {
			var yy:Number = _root.objectInner["dragRect"].nyy+_root.objectInner["dragRect"].heights/2;
		}
		stopDRECT_Array = [xx, yy, _root.objectInner["dragRect"].widthis, _root.objectInner["dragRect"].heights];
		_root.objectInner["dragRect"].removeMovieClip();
		return stopDRECT_Array;
	}
	public function startDcircle():Void {
		_root.objectInner.createEmptyMovieClip("dragRect", 80000);
		var nx:Number = _root._xmouse-_root.workArea._x;
		var ny:Number = _root._ymouse-_root.workArea._y;
		_root.objectInner["dragRect"].nxx = nx;
		_root.objectInner["dragRect"].nyy = ny;
		_root.objectInner["dragRect"].onEnterFrame = function() {
			this.createEmptyMovieClip("Rect", 0);
			this.Rect.beginFill(0x00CCFF, 0);
			this.Rect.lineStyle(1, 0x00CCFF, 0, true, "none", "round", "round", 1);
			this.Rect.moveTo(this.nxx, this.nyy);
			this.Rect.lineTo(_root._xmouse-_root.workArea._x, this.nyy);
			this.Rect.lineTo(_root._xmouse-_root.workArea._x, _root._ymouse-_root.workArea._y);
			this.Rect.lineTo(this.nxx, _root._ymouse-_root.workArea._y);
			this.Rect.lineTo(this.nxx, this.nyy);
			this.Rect.endFill();
			this.widthis = this.Rect._width;
			this.heights = this.Rect._height;
			this.createEmptyMovieClip("circle", 1);
			this["circle"].beginFill(0x00ff00, 0);
			this["circle"].lineStyle(1, 0x00CCFF, 100, true, "none", "round", "round", 1);
			this["circle"]._x = this.nxx+(_root._xmouse-_root.workArea._x-this.nxx)/2;
			this["circle"]._y = this.nyy+(_root._ymouse-_root.workArea._y-this.nyy)/2;
			this.xx = this["circle"]._x;
			this.yy = this["circle"]._y;
			var scale:Number = Math.cos(Math.PI/8);
			var halfR:Number;
			if (this.widthis>this.heights) {
				halfR = this.widthis/2;
			} else {
				halfR = this.heights/2;
			}
			this.halfRi = halfR;
			this["circle"].moveTo(halfR, 0);
			for (var i:Number = Math.PI/4; i<=Math.PI*2; i += Math.PI/4) {
				var cx = Math.cos(i-Math.PI/8)*halfR/scale;
				var cy = Math.sin(i-Math.PI/8)*halfR/scale;
				var ax = Math.cos(i)*halfR;
				var ay = Math.sin(i)*halfR;
				this["circle"].curveTo(cx, cy, ax, ay);
			}
			this["circle"]._width = this.widthis;
			this["circle"]._height = this.heights;
		};
	}
	public function stopDcircle():Array {
		var stopDRECT_Array:Array;
		delete _root.objectInner["dragRect"].onEnterFrame;
		stopDRECT_Array = [_root.objectInner["dragRect"].xx, _root.objectInner["dragRect"].yy, _root.objectInner["dragRect"].widthis, _root.objectInner["dragRect"].heights, _root.objectInner["dragRect"].halfRi];
		_root.objectInner["dragRect"].removeMovieClip();
		return stopDRECT_Array;
	}
}
