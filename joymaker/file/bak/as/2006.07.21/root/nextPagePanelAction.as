﻿import helpPanelList;
class nextPagePanelAction extends rmcName {
	var initializeX:Number;
	var initializeY:Number;
	var panelX:Number;
	var panelY:Number;
	public function nextPagePanelAction() {
		dragPanel();
		for (var i:Number = 1; i<nP.length; i++) {
		}
	}
	private function dragPanel():Void {
		var nowThist:nextPagePanelAction = this;
		_root[nP[0]][nP[6]].arrayP = 6;
		_root[nP[0]][nP[6]].onRollOver = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.getHelp(nowThist.nP[this.arrayP]);
		};
		_root[nP[0]][nP[6]].onRollOut = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
		};
		_root[nP[0]][nP[6]].onPress = function() {

			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			this._parent.startDrag();
		};
		_root[nP[0]][nP[6]].onRelease = function() {
			this._parent.stopDrag();
		};
		_root[nP[0]][nP[6]].onReleaseOutside = function() {
			this._parent.stopDrag();
		};
		_root[nP[0]][nP[5]].arrayP = 5;
		_root[nP[0]][nP[5]].onRollOver = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.getHelp(nowThist.nP[this.arrayP]);
			this.gotoAndStop(2);
		};
		_root[nP[0]][nP[5]].onRollOut = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			this.gotoAndStop(1);
		};
		_root[nP[0]][nP[5]].onPress = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			_root[nowThist.nP[0]]._x = -500;
			_root[nowThist.nP[0]]._y = -500;
		};
	}
	private function createpxpCC():Void {
		panelX = _root[nP[0]]._x;
		panelY = _root[nP[0]]._y;
		var nowThist:nextPagePanelAction = this;
		_root.createEmptyMovieClip("pxpCC", _root.getNextHighestDepth());
		_root.pxpCC._x = panelX;
		_root.pxpCC._y = panelY;
		_root.pxpCC.createEmptyMovieClip("containerXp", 1);
		//新建立一个空的movieClip；
		_root.pxpCC._alpha = 0;
		_root.pxpCC.containerXp.beginFill(0xFF0000, 0);
		_root.pxpCC.containerXp.lineStyle(1, 0xFF0000, 100, true, "none", "round", "round", 1);
		_root.pxpCC.containerXp.moveTo(0, 0);
		_root.pxpCC.containerXp.lineTo(_root[nP[0]]._width, 0);
		_root.pxpCC.containerXp.lineTo(_root[nP[0]]._width, _root[nP[0]]._height);
		_root.pxpCC.containerXp.lineTo(0, _root[nP[0]]._height);
		_root.pxpCC.containerXp.lineTo(0, 0);
		_root.pxpCC.containerXp.endFill();
		_root.pxpCC.onRollOut = function() {
			nowThist.deletepxpCC();
		};
		_root.pxpCC.onPress = function() {
			this._alpha = 100;
			this.startDrag();
		};
		_root.pxpCC.onRelease = function() {
			nowThist.stopDragGetXY();
		};
		_root.pxpCC.onReleaseOutside = function() {
			nowThist.stopDragGetXY();
		};
	}
	private function deletepxpCC():Void {
		_root.pxpCC._visible = false;
	}
	private function stopDragGetXY():Void {
		initializeX = _root.pxpCC._x;
		initializeY = _root.pxpCC._y;
		_root.pxpCC.stopDrag();
		_root.pxpCC._visible = false;
		_root[nP[0]]._x = initializeX;
		_root[nP[0]]._y = initializeY;
	}
}
