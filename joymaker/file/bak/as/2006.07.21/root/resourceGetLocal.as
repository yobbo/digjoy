﻿class resourceGetLocal implements getResourceInterface {
	public var swfResource:Array;
	public var picResource:Array;
	public var musicResource:Array;
	public function resourceGetLocal() {
		picResource = _root.RES_PICS.split(",");
		swfResource = _root.RES_MOVIETHUMB.split(",");
		musicResource = _root.RES_MP3S.split(",");
		for (var i:Number = 0; i<picResource.length; i++) {
			picResource[i] = "resource\\pics\\"+picResource[i];
		}
		for (var i:Number = 0; i<swfResource.length; i++) {
			swfResource[i] = "resource\\swfs\\movieThumb\\"+swfResource[i];
		}
		for (var i:Number = 0; i<musicResource.length; i++) {
			musicResource[i] = "resource\\mp3s\\"+musicResource[i];
		}
		picResource.pop();
		swfResource.pop();
		musicResource.pop();
	}
	public function getswfResource():Array {
		return swfResource;
	}
	public function getpicResource():Array {
		return picResource;
	}
	public function getmusicResource():Array {
		return musicResource;
	}
}
