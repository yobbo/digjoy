<%
'##############################################################################################################
'调用存储过程类
'
'By Spirit
'##############################################################################################################
class C_OPDB
	private strTmp
	public rs,procedureName,value,sqlStr
	
	Private Sub Class_Initialize()
		set rs = server.CreateObject("ADODB.recordset")
		strTmp = ""
		procedureName = ""
		value = ""
		SQL = ""
	End Sub 

	public sub runProcedureRtrs(procedureName,value)
		strTmp = "EXECUTE " & procedureName & " "
		If not value = "" Then  
			values = Split(value,"###")
			for i = 0 to UBound(values)
				if i<UBound(values) then
					strTmp = strTmp & "'" & values(i) & "',"
				else
					strTmp = strTmp & "'" & values(i) & "'"
				end if
			next
		End If 
'		response.write strTmp
'		response.end
		set rs = conn.Execute(strTmp)
	end sub  

	public sub runTSQLRtrs(procedureName)
		set rs = conn.Execute(procedureName)
	end sub  

	public sub runProcedure(procedureName,value) 
		strTmp = "EXECUTE " & procedureName & " "
		If not value = "" Then  
			values = Split(value,"###")
			for i = 0 to UBound(values)
				if i<UBound(values) then
					strTmp = strTmp & "'" & values(i) & "',"
				else
					strTmp = strTmp & "'" & values(i) & "'"
				end if
			next
		End If 
		conn.Execute(strTmp)
		conn.close()
	end sub  

	public sub runSQL(sqlStr) 
		conn.Execute(sqlStr)
		conn.close()
	end sub  
end class  
%>



<%
'##############################################################################################################
'rs简单分页代码
'设置分页基本数据
'rs=记录集；pageSize=页数据数目；curPage=当前页码
'By Spirit
'##############################################################################################################
function setPageVar(rs_page,pageSize) 
	'页显示记录数目
	rs_page.pageSize = 10
	'页码数目
	pageCount  = rs_page.pageCount
	'当前页码
	If Len(Request.QueryString("Page"))=0 Then 
	CurPage=1 
	Else 
	CurPage=CInt(Request.Querystring("Page")) 
	End If
	'设置当前页
	rs_page.AbsolutePage=CurPage
	counter = 0
end function  
%>



<%
'##############################################################################################################
'设置分页链接
'strUrl=当前页面URL；pageCount=页码数目；CurPage=当前页
'By Spirit
'##############################################################################################################
function setPageHref(rs_page,pageSize) 
	strUrl = GetUrl()
	if InStr(strUrl,"&Page") then 
		foundStr = "&Page=" & CStr(Request.QueryString("Page"))
	else
		foundStr = "?Page=" & CStr(Request.QueryString("Page"))
	end if  
	strUrl = Replace(strUrl,foundStr,"")
	'页显示记录数目
	rs_page.pageSize = 10
	'页码数目
	pageCount  = rs_page.pageCount
	CurPage=CInt(Request.QueryString("Page"))
	CurPage=GetValidPageNO(pageCount,CurPage) 
	Response.Write "<TABLE WIDTH=''100%'' align=center>" 
	Response.Write "<TR><td width=''100%'' align=''right''>" 
	Response.Write "页数：" & CurPage & "/" & pageCount & "  " 

	IF InSTR(strUrl,"?")=0 THEN 
	strPage="?Page=" 
	ELSE 
	strPage="&Page=" 
	END IF 

	IF Curpage>1 THEN 
	Response.Write "&nbsp;&nbsp;<a href=" & strUrl &strPage&"1>第一页</a>&nbsp;&nbsp;|" 
	ELSE 
	Response.Write "&nbsp;&nbsp;第一页&nbsp;&nbsp;" 
	END IF 

	IF CurPage>=2 THEN 
	Response.Write "&nbsp;&nbsp;<a href=" & strUrl & strPage &CurPage-1&">上一页</a>&nbsp;&nbsp;|" 
	ELSE 
	Response.Write "&nbsp;&nbsp;上一页&nbsp;&nbsp;" 
	END IF 

	IF cInt(CurPage)<cInt(pageCount) THEN 
	Response.Write "&nbsp;&nbsp;<a href=" & strUrl & strPage &CurPage+1&">下一页</a>&nbsp;&nbsp;|" 
	ELSE 
	Response.Write "&nbsp;&nbsp;下一页&nbsp;&nbsp;" 
	END IF 

	IF cInt(CurPage) <>cInt(pageCount) THEN 
	Response.Write "&nbsp;&nbsp;<a href=" & strUrl & strPage & pageCount&">最末页</a>&nbsp;&nbsp;" 
	ELSE 
	Response.Write "&nbsp;&nbsp;最末页&nbsp;&nbsp;" 
	END IF 
	Response.Write "</TD></tr></table>" 
end function  


Function GetValidPageNo(PageCount,CurPage) 
	Dim iPage 
	iPage=CurPage 
	IF cInt(CurPage)<1 THEN 
	iPage= 1 
	END IF 
	IF cInt(iPage) > cInt(PageCount) THEN 
	iPage= PageCount 
	END IF 
	GetValidPageNo=iPage 
END Function 

Function ImgInsertSize(picname,thiswidth,thisheight)
    ImgInsertSize=replace(picname,".","_"&thiswidth&"_"&thisheight&".")
End Function

Function formatebydate(thisdate)
formatebydate=year(thisdate)&"年"&month(thisdate)&"月"&day(thisdate)&"日"
End Function
%>

<%
'##############################################################################################################
'
'
'By Spirit
'##############################################################################################################
Function GetUrl() 
	GetUrl = "http://"&Request.ServerVariables("HTTP_HOST")&Request.ServerVariables("PATH_INFO")
	if not Request.ServerVariables("QUERY_STRING") = "" then 
		GetUrl = GetUrl &"?"& Request.ServerVariables("QUERY_STRING")	
	end if
End Function 
%>


<%
'##############################################################################################################
'利用ASPJPEG组件生成图片缩略图
'
'
'##############################################################################################################
%>
<% 
Sub CreatePic(Filename,Width,Height)
	dim jpeg,path
	set jpeg = server.createobject("persits.jpeg")															' 建立实例
	path = server.mappath("UploadFiles") & "\" & Filename													' 图片所在位置
	jpeg.open path																									' 打开
	name1 = Filename																									' 得到大图片名称
	'imagename = split(path,"\")	
	'name1 = imagename(UBound(imagename))																											
	strTmp = Split(name1,".")																						' 得到小图片名称a.jpg----->a_100_100.jpg
	name2 = strTmp(0) & "_" & CStr(Width) & "_" & CStr(Height) & "." & strTmp(1)
	jpeg.width  = Width																								' 设置缩略图大小	
	jpeg.height = Height																							
	jpeg.save server.mappath("UploadFiles") & "\" & name2													' 保存缩略图到指定文件夹下
	set jpeg = nothing																								' 注销实例
End Sub
%>



<%
'##############################################################################################################
'利用ASPJPEG组件生成图片水印效果
'
'
'##############################################################################################################
%>
<%
Sub TextOnPic(Filename,Color,Font,Content,X,Y,IsValid)
	dim jpeg,path
	set jpeg = server.createobject("persits.jpeg")															' 建立实例
	path = server.mappath("UploadFiles") & "\" & Filename													' 图片所在位置
	jpeg.open path																									' 打开

	jpeg.canvas.font.color = "&H" & Color																		' 添加文字水印
	jpeg.canvas.font.family = Font
	jpeg.canvas.font.bold = false
	'Jpeg.Canvas.Font.BkMode = "Opaque"
	jpeg.canvas.print X, Y, Content																				' "Copyright (c) digjoy.com"

	Jpeg.Canvas.Pen.Color = &H000000																			' 这里把Brush设置为true可以让整张图片显示Pen的Color
	Jpeg.Canvas.Pen.Width = 2
	Jpeg.Canvas.Brush.Solid = IsValid 
	Jpeg.Canvas.DrawBar 1, 1, Jpeg.Width, Jpeg.Height

	name1 = Filename																									' 得到大图片名称
	'imagename = split(path,"\")	
	'name1 = imagename(UBound(imagename))																											
	strTmp = Split(name1,".")																						' 得到小图片名称
	name2 = strTmp(0) & "_Text" & "." & strTmp(1)
	jpeg.save server.mappath("UploadFiles") & "\" & name2													' 保存文件
	set jpeg = nothing																								' 注销对象
End Sub
%>

<%
'分页
        Sub PageList(PageCount,Page,PageSize,PageNum)
		IF PageCount>0 then
		Dim PageText,MaxPage,Query,PageRoot,PageFoot,PageNext10
		Dim PagePrevious,PageNext,Action,Pagej,PageTemp
		PagePrevious=Cint(PageNum/2)-1
		PageNext=Cint(PageNum/2)
		PageNext10=10
		Action = Request.ServerVariables("SCRIPT_NAME")&"?"&request.ServerVariableS("QUERY_STRING")
		'Response.write action&"<br>"
		Temppage=GetContent(Action,"&page","",7)
		Action = replace(Action,Temppage,"")
		'Response.write Action
		'Query =QueryStringPage("page",False)
		'Query=Query&"/_page/"
		Query="page="
		'Response.write Query
		IF PageCount MOD PageSize = 0 THEN
			MaxPage = PageCount \ PageSize
		Else
			MaxPage = PageCount \ PageSize+1
		End IF
		'Response.Write PageRoot & "|" & MaxPage
		IF MaxPage>1 Then
			Page=Clng(Page)
			IF Page>MaxPage Then
				Page=MaxPage
			End IF
				PageText="<div class=""page"">"
				IF Page<PageNext then
					PageText=PageText & "<img src=""../images/First.gif"" border=""0"" alt=""总有记录:"& PageCount &"条,共"& MaxPage &"页"" />  "		
				Else		
					PageText=PageText & "<a href=""" & Action & "&"& Query &"1""><img src=""../images/First.gIF"" border=""0"" alt=""总有记录:"& PageCount &"条,共"& MaxPage &"页,点这里转到第一页"" /></a>  "
				End IF
				IF (MaxPage-Page)<PageNext Then
					PageRoot=(Page-PagePrevious)-(PageNext-(MaxPage-Page))
				Else
					PageRoot=Page-PagePrevious
				End IF
				IF PageRoot<1 Then
					PageRoot=1
				End IF
				IF Page<PageNext or Page="" then
					PageRoot=1
					PageFoot=PageNum
				Else
					PageFoot=Page+PageNext
				End IF
				IF PageFoot>MaxPage Then
					PageFoot=MaxPage
				End IF
				
				IF Page<PageNext then
					PageText=PageText &"<img src=""../images/Previous.gIF"" border=""0"" alt=""总有记录:"& PageCount &"条,共"& MaxPage &"页"" />  "
				Else
					PageText=PageText &"<a href=""" & Action & "&"& Query & (Page-10)&"""><img src=""../images/Previous.gIF"" border=""0"" alt=""总有记录:"& PageCount &"条,共"& MaxPage &"页,点这里转到上十页"" /></a>  "
				End IF
				For Pagej=PageRoot To PageFoot
					IF Page=Pagej then
						PageTemp="<font style=color:red;font-weight:bold;>"&Pagej&"</font>  "
					Else
						IF Pagej=1 then
							PageTemp="<a href=""" & Action & "&"& Query & Pagej &""">"&Pagej&"</a>  "
						Else
							PageTemp="<a href=""" & Action & "&"& Query & Pagej &""">"&Pagej&"</a>  "
						End IF
					End IF
					PageText=PageText & PageTemp
					IF Pagej=MaxPage then 
						Pagej=Pagej+1
						Exit for
					End IF
				Next
				IF MaxPage-Page<PageNum then
					PageNext10=MaxPage-Page
				End IF
				IF Pagej-1=MaxPage then
					PageText=PageText &"<img src=""../images/Next.gIF"" alt=""总有记录:"& PageCount &"条,共"& MaxPage &"页"" />  " & Chr(13)
				Else
					PageText=PageText &"<a href=""" & Action & "&"& Query & (Page+PageNext10)&"""><img src=""../images/Next.gIF"" border=""0"" alt=""总有记录:"& PageCount &"条,共"& MaxPage &"页,点这里转到下十页"" /></a>  " & Chr(13)
				End IF
				IF Page>=MaxPage then
					PageText=PageText & "<img src=""../images/Last.gIF"" alt=""总有记录:"& PageCount &"条,共"& MaxPage &"页"" />  "  & Chr(13)
				Else
					PageText=PageText & "<a href=""" & Action & "&"& Query & MaxPage&"""><img src=""../images/Last.gIF"" border=""0"" alt=""总有记录:"& PageCount &"条,共"& MaxPage &"页,点这里转到尾页"" /></a>  " & Chr(13)			
				End IF
				PageText=PageText & " "
				Response.Write(PageText&"</div>")
			Else
			End IF
		End IF		
	End Sub%>

<%'###############################################################################################################################################
'调用实例
'
'################################################################################################################################################%>
<%
'call CreatePic("1.jpg",200,100)
%>
<%
'call TextOnPic("1.jpg","ffffff","宋体","笨笨",250,250,false)
%>

<!--修改大小后图片：
img src = '../images/1_small.jpg'><br>
加入水印后图片：
<img src = '../images/1_text.jpg'-->
<%
Function GetContent(str,start,last,n)
	If Instr(lcase(str),lcase(start))>0 then
		select case n
		case 0	'左右都截取（都取前面）（去处关键字）
		GetContent=Right(str,Len(str)-Instr(lcase(str),lcase(start))-Len(start)+1)
		GetContent=Left(GetContent,Instr(lcase(GetContent),lcase(last))-1)
		case 1	'左右都截取（都取前面）（保留关键字）
		GetContent=Right(str,Len(str)-Instr(lcase(str),lcase(start))+1)
		GetContent=Left(GetContent,Instr(lcase(GetContent),lcase(last))+Len(last)-1)
		case 2	'只往右截取（取前面的）（去除关键字）
		GetContent=Right(str,Len(str)-Instr(lcase(str),lcase(start))-Len(start)+1)
		case 3	'只往右截取（取前面的）（包含关键字）
		GetContent=Right(str,Len(str)-Instr(lcase(str),lcase(start))+1)
		case 4	'只往左截取（取后面的）（包含关键字）
		GetContent=Left(str,InstrRev(lcase(str),lcase(start))+Len(start)-1)
		case 5	'只往左截取（取后面的）（去除关键字）
		GetContent=Left(str,InstrRev(lcase(str),lcase(start))-1)
		case 6	'只往左截取（取前面的）（包含关键字）
		GetContent=Left(str,Instr(lcase(str),lcase(start))+Len(start)-1)
		case 7	'只往右截取（取后面的）（包含关键字）
		GetContent=Right(str,Len(str)-InstrRev(lcase(str),lcase(start))+1)
		case 8	'只往左截取（取前面的）（去除关键字）
		GetContent=Left(str,Instr(lcase(str),lcase(start))-1)
		case 9	'只往右截取（取后面的）（包含关键字）
		GetContent=Right(str,Len(str)-InstrRev(lcase(str),lcase(start)))
		end select
	Else
		GetContent=""
	End if
End function
%>
