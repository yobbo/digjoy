﻿/*
copyright yuandejun@gmail.com.袁得钧.13980752339;
yuandejun@gmail.com
*/
import basicFunction;
class login extends MCname {
	public function login() {
		if(_root.MakerUserLogin==true){
			_root[ral[0]].gotoAndStop("loginSuccess");
		}
		
		var basicFunction_in:basicFunction = new basicFunction();
		var loginThis:login = this;
		_root[ral[0]][ral[3]].onRelease = function() {
			basicFunction_in.buttonMusic();
			loginThis.loginCheck();
		};
		_root[ral[0]][ral[4]].onRelease = function() {
			basicFunction_in.buttonMusic();
			loginThis.loginClear();
		};
		for (var i:Number = 13; i<15; i++) {
			_root[ral[0]][ral[i]].onRollOver = function() {
				basicFunction_in.buttonMusic();
				this.gotoAndStop(2);
			};
			_root[ral[0]][ral[i]].onRollOut = function() {
				basicFunction_in.buttonMusic();
				this.gotoAndStop(1);
			};
		}
		_root[ral[0]][ral[13]].onRelease = function() {
			this._parent.gotoAndStop("register");
		};
		_root[ral[0]][ral[14]].onRelease = function() {
		};
	}
	private function loginClear():Void {
		_root[ral[0]][ral[1]].text = "";
		_root[ral[0]][ral[2]].text = "";
	}
	//登陆时清除按钮动作；
	public function loginCheck():Void {
		var loginThis:login = this;
		var basicFunction_in:basicFunction = new basicFunction();
		var user:String = _root[ral[0]][ral[1]].text;
		//取的用户名；
		var pass:String = _root[ral[0]][ral[2]].text;
		//取的密码；
		if (_root[ral[0]][ral[1]].text == "") {
			basicFunction_in.alert("用户名不能为空");
		} else if (_root[ral[0]][ral[2]].text == "") {
			basicFunction_in.alert("密码不能为空");
		} else {
			_root[ral[0]].gotoAndStop("loading");
			var loginToWEB:LoadVars = new LoadVars();
			//取得数据量
			var loginMess:LoadVars = new LoadVars();
			//发送的数据
			loginMess.loginUserName = user;
			loginMess.loginUserPassword = pass;
			loginMess.sendAndLoad("http://127.0.0.1:8080/login.jsp", loginToWEB, "post");
			loginToWEB.onLoad = function(success:Boolean) {
				if (success) {
					//_root.tt.text += loginToWEB.login;
					delete loginMess.send;
					delete loginToWEB.load;
					var booleanL:String=loginToWEB.login;
					
					_root.MakerUserName=loginToWEB.userName;//登陆用户名；
					_root.MakerUserPassword=pass;//登陆用户密码；
					_root.MakerUserEmail=loginToWEB.userEmail;//登陆用户邮局地址；
					
					if (booleanL=="true") {
						_root.MakerUserLogin=true;
						_root[loginThis.ral[0]].gotoAndStop("loginSuccess");
					}else if(booleanL=="false"){
						basicFunction_in.alert("验证不通过，错误的用户名或密码");
						_root[loginThis.ral[0]].gotoAndStop("login");
					}
					//如果登陆成功则跳转到登陆成功；   
				} else {
					basicFunction_in.alert("未能与服务器取的联系");
					_root[loginThis.ral[0]].gotoAndStop("login");
				}
			};
		}
	}
	//登陆检查动作；
}
