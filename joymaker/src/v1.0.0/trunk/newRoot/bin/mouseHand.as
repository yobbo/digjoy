﻿/*
*	copyright yuandejun@gmail.com.袁得钧.13980752339
*	mouse is ok,now tools is begin;
*/
import basicScale;
import assistant;
import basicFunction;
import circleObject;

class mouseHand extends MCname {
	private var nAc:String;
	
	public function mouseHand(toolSend:String) {
		_root.toolState.nowTool= toolSend;//设置当前工具；
		nAc = toolSend;
		_root.createEmptyMovieClip("mouseMC",40000);
		defaultMouse();
	}
	
	private function defaultMouse() {
		var arraysIN:arrays=new arrays();
		var basicFunctionIN:basicFunction = new basicFunction();
		var newassistant:assistant = new assistant();
		var nowThis:mouseHand = this;
		newassistant.clearreopqs();
		_root.mouseMC.attachMovie("mouseStyle", "nowA", 0);//绑定鼠标状态；
		_root.mouseMC.nowA.gotoAndStop("none");//默认什么都不显示；
		var mouseListener:Object = new Object();
		
		mouseListener.onMouseMove = function():Void {
			if (nowThis.nAc == tools[0]) {
				/*移动到物体上面就为选择工具*/
				for (var i:Number = 0; i<arraysIN.getNowPageArr(); i++) {
					if (_root.objectInner["object"+i].hitTest(_root._xmouse,_root._ymouse,true)) {
						_root.mouseMC.nowA.gotoAndStop("select");
						break;
					}
				}
				_root.mouseMC.nowA._x = _root._xmouse;
				_root.mouseMC.nowA._y = _root._ymouse;
				/*移动到物体上面就为选择工具*/
				
			}else if(nowThis.nAc == tools[1]){
				/*移动到物体上面就为文字工具*/
				_root.mouseMC.nowA.gotoAndStop("text");
				_root.mouseMC.nowA._x = _root._xmouse;
				_root.mouseMC.nowA._y = _root._ymouse;
				/*移动到物体上面就为文字工具*/
				
			}else if(nowThis.nAc == tools[2]){
				/*移动到物体上面就为涂鸦工具*/
				_root.mouseMC.nowA.gotoAndStop("tuya");
				_root.mouseMC.nowA._x = _root._xmouse;
				_root.mouseMC.nowA._y = _root._ymouse;
				/*移动到物体上面就为涂鸦工具*/
				
			}else if(nowThis.nAc == tools[3]){
				/*移动到物体上面就为涂鸦工具*/
				_root.mouseMC.nowA.gotoAndStop("tuya");
				_root.mouseMC.nowA._x = _root._xmouse;
				_root.mouseMC.nowA._y = _root._ymouse;
				/*移动到物体上面就为涂鸦工具*/
				
			}else if(nowThis.nAc == tools[4]){
				/*移动到物体上面就为涂鸦工具*/
				_root.mouseMC.nowA.gotoAndStop("tuya");
				_root.mouseMC.nowA._x = _root._xmouse;
				_root.mouseMC.nowA._y = _root._ymouse;
				/*移动到物体上面就为涂鸦工具*/
			}

		}
		mouseListener.onMouseDown = function():Void {
			if (nowThis.nAc == tools[0]) {
				/*当时选择工具时，鼠标按下时的反应*/
				if (!(_root.toolBAR.hitTest(_root._xmouse,_root._ymouse,true) ||_root.pageBAR.hitTest(_root._xmouse,_root._ymouse,true) ||_root.propertyBAR.hitTest(_root._xmouse,_root._ymouse,true))) {
					var mc:MovieClip = null;
					for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
						if (_root.work["object"+i].hitTest(_root._xmouse,_root._ymouse,true)) {
							var d:Number;
							if (mc == null) {
								d = -1;
							} else {
								d = mc.getDepth();
							}
							if (_root.work["object"+i].getDepth()>d) {
								mc = _root.work["object"+i];
							}
						}
					}
					var basicScaleIN:basicScale = new basicScale(mc);
				}
				/*当时选择工具时，鼠标按下时的反应*/

			}else if(nowThis.nAc == tools[1]){
				/*当为文本工具时，鼠标按下时的反应*/
				if (!(_root.toolBAR.hitTest(_root._xmouse,_root._ymouse,true) ||_root.pageBAR.hitTest(_root._xmouse,_root._ymouse,true) ||_root.propertyBAR.hitTest(_root._xmouse,_root._ymouse,true))) {
					basicFunctionIN.startDRECT();
				}
				/*当为文本工具时，鼠标按下时的反应*/
			}else if(nowThis.nAc == tools[2]){
					
			}else if(nowThis.nAc == tools[3]){
				/*当为画矩形工具时，鼠标按下所具有的反应*/
				if (!(_root.toolBAR.hitTest(_root._xmouse,_root._ymouse,true) ||_root.pageBAR.hitTest(_root._xmouse,_root._ymouse,true) ||_root.propertyBAR.hitTest(_root._xmouse,_root._ymouse,true))) {
						basicFunctionIN.startDRECT();
				}
				/*当为画矩形工具时，鼠标按下所具有的反应*/
			}else if(nowThis.nAc == tools[4]){
				/*当为画圆形工具时，鼠标按下所具有的反应*/
				if (!(_root.toolBAR.hitTest(_root._xmouse,_root._ymouse,true) ||_root.pageBAR.hitTest(_root._xmouse,_root._ymouse,true) ||_root.propertyBAR.hitTest(_root._xmouse,_root._ymouse,true))) {
						basicFunctionIN.startDcircle();
				}
				/*当为画原形工具时，鼠标按下所具有的反应*/
			}

		}
		
		mouseListener.onMouseUp = function():Void {
			if (nowThis.nAc == tools[0]) {

			}else if(nowThis.nAc == tools[1]){

					var nowR:Array = basicFunctionIN.stopDRECT();
					_root.createEmptyMovieClip("textEditMC", 20000);
					_root.textEditMC.attachMovie("textEdit", "textEdits", 1);
					_root.textEditMC._x = 300;
					_root.textEditMC._y = 200;
					_root.textEditMC.r1 = nowR[0];
					_root.textEditMC.r2 = nowR[1];
					_root.textEditMC.r3 = nowR[2];
					_root.textEditMC.r4 = nowR[3];
					var shadowNow:flash.filters.DropShadowFilter = new flash.filters.DropShadowFilter(10, 90, 0x000000, 0.8, 16, 16, 1, 3, false, false, false);
					_root.textEditMC.filters = [shadowNow];
					_root.textEditMC.textEdits.succT.onRelease = function() {
						_root.textEditMC.removeMovieClip();
					};
					_root.textEditMC.textEdits.clearT.onRelease = function() {
						_root.textEditMC.textEdit.textTT.text = "";
					};
					_root.textEditMC.textEdits.offButton.onRelease = function() {
						_root.textEditMC.removeMovieClip();
					};

			}else if(nowThis.nAc == tools[2]){
					
			}else if(nowThis.nAc == tools[3]){
				var nowR:Array = basicFunctionIN.stopDRECT();
			}else if(nowThis.nAc == tools[4]){
				var nowR:Array = basicFunctionIN.stopDcircle();
				var circleObjectIN:circleObject=new circleObject();
				circleObjectIN.newCircleObject(nowR,arraysIN.getNowPageArr());
			}
		}
		Mouse.addListener(mouseListener);

	}
}
