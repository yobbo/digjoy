﻿/*
*	copyright yuandejun@gmail.com.袁得钧.13980752339
*	属性表；
*/

class objectDATA{
	private var o_nowid:Number;
	private var o_kinds:String;//0
	private var o_width:Number;//1
	private var o_height:Number;//2
	private var o_x:Number;//3
	private var o_y:Number;//4
	private var o_url:String;//5
	private var o_depth:Number;//6
	private var o_color:Array;//7
	private var o_drop:Array;//8
	private var o_blur:Array;//9
	private var o_rotation:Number;//10
	private var o_xscale:Number;//11
	private var o_yscale:Number;//12
	private var o_linecolor:String;//13
	private var o_linewidth:Number;//14
	private var o_fillcolor:String;//15
	private var o_circler:Number;//16
	private var o_link:String;//17
	private var o_filter:Array;//18
}