﻿/*
*	copyright yuandejun@gmail.com.袁得钧.13980752339
*	page Control;
*/
import arrays;
import listObject;
import workAreaPlay;
import assistant;
import basicFunction;
import exportXML;
import helpPanelList;

class pageControl extends MCname {
	public var maxPage:Number = 40;//确定最大的页码数，用户定义的页码数不能超过此页码数；
	public var maxPageCycle:Number;//定义总共的页码轮数；
	public var nowPageCycle:Number;//定义现在的页码轮数；
	public var maxPageNumber:Number;//定义现在的页码总数；
	public var nowPage:Number;//定义现在的页码数；
	private var pST:Number;//记录以前的duplicateMC开始数用于删除
	private var pEN:Number;//记录以前的duplicateMC开始数用于删除
	public function pageControl() {
		var nowThis:pageControl = this;
		var basicFunction_in:basicFunction = new basicFunction();
		var newhelpPanelList:helpPanelList = new helpPanelList();
		
		nowPageCycle = 0;
		nowPage = 0;
		
		calPage();//计算总共多少页码轮数，开始执行页码轮数转换；
		for (var i:Number = 0; i<3; i++) {
			_root.pageBAR[pageS[i]].arrayP = i;
			_root.pageBAR[pageS[i]].onRollOver = function() {
				newhelpPanelList.getHelp(nowThis.pageS[this.arrayP]);
				basicFunction_in.buttonMusic();
				this.gotoAndStop(2);
			};
			_root.pageBAR[pageS[i]].onRollOut = function() {
				newhelpPanelList.deleteTip();
				this.gotoAndStop(1);
			};
		}
		_root.pageBAR[pageS[0]].onRelease = function() {
			if (_root["maxPageArr"].length<nowThis.maxPage) {
				this.gotoAndStop(1);
				nowThis.addOnePage(_root.thisPage.nowPage);
			} else {
				basicFunction_in.alert("您进行的操作大于"+nowThis.maxPage+"页"+newline+"请妥善保存您的文件，以便于下次操作");
			}
		};
		_root.pageBAR[pageS[1]].onRelease = function() {
			if (_root.thisPage.nowPage == 0) {
				basicFunction_in.alert("不能删除封面"+newline+"请跳转到其它页码进行删除");
			} else if (_root.thisPage.nowPage == (_root["maxPageArr"].length)) {
				basicFunction_in.alert("不能删除封底"+newline+"请跳转到其它页码进行删除");
			} else {
				this.gotoAndStop(1);
				nowThis.deleteOnePage(_root.thisPage.nowPage);
			}
		};
	}
	public function getMaxPageNumber():Number {
		var newarrays:arrays = new arrays();
		maxPageNumber = newarrays.getMaxPageArr();
		return maxPageNumber;
	}
	
	public function calPage() {
		maxPageCycle = Math.floor(getMaxPageNumber()/12);//算出最大的页码轮；
		setPageNumButton();//页码轮动作开始；
		goPageCycle(nowPageCycle);//开始页码列表了；
		goPage(nowPage);//现在的页码开始跳转；
	}
	
	private function setPageNumButton():Void {
		var newhelpPanelList:helpPanelList = new helpPanelList();
		var basicFunction_in:basicFunction = new basicFunction();
		var nowThis:pageControl = this;
		
		for (var i:Number = 4; i<6; i++) {
			_root.pageBAR[pageS[3]][pageS[i]].arrayP = i;
			_root.pageBAR[pageS[3]][pageS[i]].onRollOver = function() {
				basicFunction_in.buttonMusic();
				newhelpPanelList.getHelp(nowThis.pageS[this.arrayP]);
				this.gotoAndStop(2);
			};
			_root.pageBAR[pageS[3]][pageS[i]].onRollOut = function() {
				newhelpPanelList.deleteTip();
				this.gotoAndStop(1);
			};
			_root.pageBAR[pageS[3]][pageS[i]].onReleaseOutside = function() {
				newhelpPanelList.deleteTip();
				this.gotoAndStop(1);
			};
		}
		_root.pageBAR[pageS[3]][pageS[4]].onRelease = function() {
			if (nowThis.nowPageCycle>0) {
				nowThis.nowPageCycle = nowThis.nowPageCycle-1;
				nowThis.goPageCycle(nowThis.nowPageCycle);
				//进入前一轮；
			}
		};
		_root.pageBAR[pageS[3]][pageS[5]].onRelease = function() {
			if (nowThis.nowPageCycle<nowThis.maxPageCycle) {
				nowThis.nowPageCycle = nowThis.nowPageCycle+1;
				nowThis.goPageCycle(nowThis.nowPageCycle);
				//进入后一轮；
			}
		};
	}
	public function addOnePage(getNowPage:Number):Void {
		var arrLength:Number = _root["maxPageArr"].length;
		_root["page"+arrLength] = new Array();
		_root["maxPageArr"].push(_root["page"+arrLength]);
		for (var i:Number = arrLength; i>getNowPage; i--) {
			for (var t:Number = _root["page"+(i-1)].length-1; t>=0; t--) {
				_root["page"+i][t] = _root["page"+(i-1)].pop();
			}
		}
		calPage();
	}
	public function deleteOnePage(getNowPage:Number):Void {
		var arrLength:Number = _root["maxPageArr"].length;
		
		var newlistObject:listObject = new listObject();
		newlistObject.listObjectClear();
		newlistObject.clearPageArray();
		
		for (var i:Number = getNowPage; i<arrLength-1; i++) {
			for (var t:Number = _root["page"+(i+1)].length-1; t>=0; t--) {
				_root["page"+i][t] = _root["page"+(i+1)].pop();
			}
		}
		trace(_root["maxPageArr"].pop());
		calPage();
	}
	private function goPageCycle(pagenu:Number) {
		
		var thisIDB:Number;//起始的数
		var thisIDE:Number;//结束的数
		var thisPageID:Number;//这页轮显示的数目
		var goPageNowPage:Number = pagenu;//将目前的页码数减去1就是该页的数了；
		thisIDB = goPageNowPage*12;
		thisIDE = goPageNowPage*12+12;
		if (thisIDE>getMaxPageNumber()) {
			thisIDE = goPageNowPage*12+getMaxPageNumber()%12;
		}
		getPage(thisIDB, thisIDE);
	}
	
	private function getPage(startIDB:Number, endIDB:Number):Void {
		clearPST();
		var nowThis:pageControl = this;
		var basicFunction_in:basicFunction = new basicFunction();
		var newhelpPanelList:helpPanelList = new helpPanelList();
		var pic_y:Number=0;
		for (var t:Number = startIDB; t<endIDB; t++) {
			_root.pageBAR[pageS[3]][pageS[6]].attachMovie([pageS[7]], "dpNM"+t, t);
			_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t]._y = pic_y;
			_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].realPageName = t;
			var newarrays = new arrays();
			if (t == 0) {
				_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].pageTextN.text = "封面";
			} else if (t == (newarrays.getMaxPageArr()-1)) {
				_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].pageTextN.text = "封底";
			} else {
				_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].pageTextN.text = t+"页";
			}
			
			_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].onRollOver = function() {
				basicFunction_in.buttonMusic();
				if (nowThis.nowPage != this.realPageName) {
					this.gotoAndStop(2);
				}
			};
			
			_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].onRollOut = function() {
				if (nowThis.nowPage != this.realPageName) {
					this.gotoAndStop(1);
				}
			};
			
			_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].onRelease = function() {
				if (nowThis.nowPage != this.realPageName) {
					_root.pageBAR[nowThis.pageS[3]][nowThis.pageS[6]]["dpNM"+nowThis.nowPage].gotoAndStop(1);
					nowThis.nowPage = this.realPageName;
					nowThis.goPage(this.realPageName);
					this.gotoAndStop(3);
				}
			};
			
			_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].onReleaseOutside = function() {
				if (nowThis.nowPage != this.realPageName) {
					this.gotoAndStop(1);
				}
			};
			
			if (nowThis.nowPage == _root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].realPageName) {
				_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+t].realPageName].gotoAndStop(3);
			}
			
			pic_y +=38;
		}
		getPS(startIDB, endIDB);
	}//开始列表了，以5排的方式一排2列的方式开始排列;
	
	private function getPS(startIB:Number, endIB:Number) {
		pST = startIB;
		pEN = endIB;
	}//取的先前的页中数目
	
	private function clearPST() {
		for (var i:Number = pST; i<pEN; i++) {
			_root.pageBAR[pageS[3]][pageS[6]]["dpNM"+i].removeMovieClip();
		}
	}//删除先前的页中数目
	
	private function goPage(page:Number) {
		var newlistObject:listObject = new listObject();
		newlistObject.listObjectClear();//先执行清除；

		_root.thisPage.nowPage = page;//设置主场景上的当前页为目前页；
		newlistObject.listObjectThis();//再执行建立；
		
		var newworkAreaPlay:workAreaPlay = new workAreaPlay();
		newworkAreaPlay.goWorkAreaPlay();//页面变换;
		
		var newassistant:assistant = new assistant();
		newassistant.clearreopqs();//删除物件包围的匡;
	}
}
