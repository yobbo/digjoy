﻿/*
*	copyright yuandejun@gmail.com.袁得钧.13980752339
*	滤镜功能；
*/
import flash.filters.BlurFilter;
import flash.geom.ColorTransform;
import flash.geom.Transform;
import flash.filters.DropShadowFilter;
class filterOBJECT {

	public function filterOBJECT() {
	}
	public function setFilter(object:MovieClip, colorTransFrom:Array, DropShadow:Array, pBlur:Array):Void {
		var filter_drop:DropShadowFilter = new DropShadowFilter(DropShadow[0], DropShadow[1], DropShadow[2], DropShadow[3], 16, 16, 1, 3, false, false, false);
		var filter_blur:BlurFilter = new BlurFilter(pBlur[0], pBlur[1], 3);
		var filterArray:Array = new Array();
		filterArray.push(filter_drop, filter_blur);
		object.filters = filterArray;
		var colorTrans:ColorTransform = new ColorTransform(1, 1, 1, colorTransFrom[0], colorTransFrom[1], colorTransFrom[2], colorTransFrom[3], 0);
		var trans:Transform = new Transform(object);
		trans.colorTransform = colorTrans;
	}
}