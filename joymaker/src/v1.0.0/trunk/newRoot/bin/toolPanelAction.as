﻿/*
*	copyright yuandejun@gmail.com.袁得钧.13980752339
*	tools is begin;
*/
import basicFunction;
import showRESOURCE;
import helpPanelList;
import assistant;
class toolPanelAction extends MCname {
	public var toolState:String;//目前所在的工具；
	public function toolPanelAction() {
		var nowThist:toolPanelAction = this;
		var basicFunction_in:basicFunction = new basicFunction();//实例化基本方法；
		var newhelpPanelList:helpPanelList = new helpPanelList();//实例化弹出提示；
		var newassistant:assistant = new assistant();//实例化清除工具类；
		
		for (var t:Number = 0; t<toolsI.length; t++) {
			_root.toolBAR[toolsI[t]].arrayP = t;
			_root.toolBAR[toolsI[t]].onRollOver = function() {
				basicFunction_in.buttonMusic();
				newhelpPanelList.getHelp(this.arrayP);
				this.gotoAndStop(2);
			};
			_root.toolBAR[toolsI[t]].onRollOut = function() {
				newhelpPanelList.deleteTip();
				this.gotoAndStop(1);
			};
			_root.toolBAR[toolsI[t]].onReleaseOutside = function() {
				this.gotoAndStop(1);
			};
			_root.toolBAR[toolsI[t]].onRelease = function() {
				this.gotoAndStop(1);
			};
		}
		_root.toolBAR[toolsI[0]].onPress = function() {
			this.gotoAndStop(3);
			fscommand("importPIC", "TRUE");
		};
		_root.toolBAR[toolsI[1]].onPress = function() {
			this.gotoAndStop(3);
			fscommand("importMUSIC", "TRUE");
		};
		_root.toolBAR[toolsI[2]].onPress = function() {
			this.gotoAndStop(3);
			fscommand("importSWF", "TRUE");
		};
		_root.toolBAR[toolsI[3]].onPress = function() {
			this.gotoAndStop(3);
			fscommand("importMOVIE", "TRUE");
		};
		_root.toolBAR[toolsI[4]].onPress = function() {
			this.gotoAndStop(3);
			var showRESOURCE_in:showRESOURCE = new showRESOURCE();
		};
		/*默认工具*/
		_root.toolBAR[tools[0]].gotoAndStop(3);
		toolState = tools[0];
		var newmouseHand = new mouseHand(toolState);
		/*默认工具*/
		for (var i:Number = 0; i<tools.length; i++) {
			_root.toolBAR[tools[i]].arrayP = i;
			_root.toolBAR[tools[i]].onRollOver = function() {
				newhelpPanelList.getHelp(nowThist.tools[this.arrayP]);
				if (nowThist.toolState != nowThist.tools[this.arrayP]) {
					basicFunction_in.buttonMusic();
					this.gotoAndStop(2);
				}
			};
			_root.toolBAR[tools[i]].onRollOut = function() {
				newhelpPanelList.deleteTip();
				if (nowThist.toolState != nowThist.tools[this.arrayP]) {
					this.gotoAndStop(1);
				}
			};
			_root.toolBAR[tools[i]].onPress = function() {
				newhelpPanelList.deleteTip();
				if (nowThist.toolState != nowThist.tools[this.arrayP]) {
					this.gotoAndStop(3);
				}
			};
			_root.toolBAR[tools[i]].onRelease = function() {
				newassistant.clearreopqs();
				if (nowThist.toolState != nowThist.tools[this.arrayP]) {
					this.gotoAndStop(3);
					_root.toolBAR[nowThist.toolState].gotoAndStop(1);//清除以前按钮状态;
					nowThist.toolState = nowThist.tools[this.arrayP];//确定目前工具面板状态;
					var newmouseHand = new mouseHand(nowThist.toolState);
					//执行不同的鼠标动作;
					//if (nowThist.tP[this.arrayP] != "toolText") {
					//_root.textEditMC.removeMovieClip();
					//}
				}
			};
			_root.toolBAR[tools[i]].onReleaseOutside = function() {
				if (nowThist.toolState != nowThist.tools[this.arrayP]) {
					this.gotoAndStop(1);
				}
			};
		}
	}
}
