﻿/*
copyright yuandejun@gmail.com.袁得钧.13980752339;
yuandejun@gmail.com
*/
import basicFunction;
class register extends MCname {
	public function register() {
		var basicFunction_in:basicFunction = new basicFunction();
		var loginThis:register = this;
		_root[ral[0]][ral[7]].onRelease = function() {
			basicFunction_in.buttonMusic();
			loginThis.registerCheck();
		};
		_root[ral[0]][ral[8]].onRelease = function() {
			basicFunction_in.buttonMusic();
			loginThis.registerClear();
		};
		_root[ral[0]][ral[20]].onRelease = function() {
			_root[loginThis.ral[0]].gotoAndStop("login");
			this.gotoAndStop(1);
		};
		_root[ral[0]][ral[20]].onRollOver = function() {
			basicFunction_in.buttonMusic();
			this.gotoAndStop(2);
		};
		_root[ral[0]][ral[20]].onRollOut = function() {
			basicFunction_in.buttonMusic();
			this.gotoAndStop(1);
		};
	}
	public function registerClear():Void {
		_root[ral[0]][ral[5]].text = "";
		_root[ral[0]][ral[6]].text = "";
		_root[ral[0]][ral[9]].text = "";
		_root[ral[0]][ral[10]].text = "";
	}
	public function registerCheck():Void {
		var loginThis:register = this;
		var basicFunction_in:basicFunction = new basicFunction();
		var user:String = _root[ral[0]][ral[9]].text;
		//取的用户名；
		var pass:String = _root[ral[0]][ral[10]].text;
		//取的密码；
		var passaga:String = _root[ral[0]][ral[5]].text;
		//取的重复密码；
		var email:String = _root[ral[0]][ral[6]].text;
		//取的邮件地址；
		if (_root[ral[0]][ral[5]].text == "") {
			basicFunction_in.alert("密码不能为空");
		} else if (_root[ral[0]][ral[6]].text == "") {
			basicFunction_in.alert("邮件地址不能为空");
		} else if (_root[ral[0]][ral[9]].text == "") {
			basicFunction_in.alert("用户名不能为空");
		} else if (_root[ral[0]][ral[10]].text == "") {
			basicFunction_in.alert("密码不能为空");
		} else if (_root[ral[0]][ral[5]].text != _root[ral[0]][ral[10]].text) {
			basicFunction_in.alert("密码不匹配");
		} else if (!basicFunction_in.checkEmail(_root[ral[0]][ral[6]].text)) {
			basicFunction_in.alert("邮件地址不合法");
		} else {
			_root[ral[0]].gotoAndStop("loading");
			//点击登陆跳转到loading帧；
			var loginToRegister:LoadVars = new LoadVars();
			//取得数据量
			var loginMessReg:LoadVars = new LoadVars();
			//发送的数据
			loginMessReg.UserName = user;
			loginMessReg.UserPassword = pass;
			loginMessReg.UserEmail = email;
			loginMessReg.sendAndLoad("http://127.0.0.1:8080/register.jsp", loginToRegister, "post");
			loginToRegister.onLoad = function(success:Boolean) {
				if (success) {
					delete loginMessReg.send;
					delete loginToRegister.load;
					var booleanL:String = loginToRegister.register;
					if (booleanL == "true") {
						basicFunction_in.alert("注册成功，现在返回到登陆窗口请在登陆窗口登陆");
						_root[loginThis.ral[0]].gotoAndStop("login");
					} else if (booleanL == "false") {
						basicFunction_in.alert("注册不通过");
						_root[loginThis.ral[0]].gotoAndStop("login");
					}
				} else {
					basicFunction_in.alert("未能与服务器取的联系");
					_root[loginThis.ral[0]].gotoAndStop("login");
				}
			};
		}
	}
}
