﻿/*
*	copyright yuandejun@gmail.com.袁得钧.13980752339
*	图片建立；
*/
import objectDATA;
import filterOBJECT;
class jpgObject extends objectDATA {
	public function jpgObject(){
	}
	
	public function newjpgObject(ni:Number,urlADD:String):Void {
		o_nowid = ni;
		o_kinds = "load";
		o_width = 0;
		o_height = 0;
		o_x = Stage.width/2;
		o_y = Stage.height/2;
		o_url = urlADD;
		o_depth = ni;
		o_color = [1,0,0,0];
		o_drop = [16,45,"0x000000",0];
		o_blur = [0,0];
		o_rotation = 0;
		o_xscale = 100;
		o_yscale = 100;
		o_linecolor = "0";
		o_linewidth = 0;
		o_fillcolor = "0";
		o_circler = 0;
		o_link = "";
		o_filter = ["None",0.5,0,"None"];
		
		_root.work.createEmptyMovieClip("object"+o_nowid,o_depth);
		_root.work["object"+o_nowid].o_nowid = o_nowid;
		_root.work["object"+o_nowid].o_kinds = o_kinds;
		_root.work["object"+o_nowid].o_width = o_width;
		_root.work["object"+o_nowid].o_height = o_height;
		_root.work["object"+o_nowid].o_url = o_url;
		_root.work["object"+o_nowid].o_color = o_color;
		_root.work["object"+o_nowid].o_drop = o_drop;
		_root.work["object"+o_nowid].o_blur = o_blur;
		_root.work["object"+o_nowid].o_linecolor = o_linecolor;
		_root.work["object"+o_nowid].o_linewidth = o_linewidth;
		_root.work["object"+o_nowid].o_fillcolor = o_fillcolor;
		_root.work["object"+o_nowid].o_circler = o_circler;
		_root.work["object"+o_nowid].o_link = o_link;
		_root.work["object"+o_nowid].o_filter = o_filter;
		
		_root.work["object"+o_nowid].createEmptyMovieClip("object", 0);
		_root.work["object"+o_nowid]["object"].loadMovie(o_url);
		
		_root.work["object"+o_nowid]._x = o_x;
		_root.work["object"+o_nowid]._y = o_y;
		_root.work["object"+o_nowid]._rotation = o_rotation;
		
		_root.work["object"+o_nowid]["object"]._xscale =o_xscale;
		_root.work["object"+o_nowid]["object"]._yscale =o_yscale;
		
		var nowMC:MovieClip=_root.work["object"+o_nowid];
		var filterOBJECTIN:filterOBJECT=new filterOBJECT();
		filterOBJECTIN.setFilter(nowMC,o_color,o_drop,o_blur);
		
		var getArray:Array=[nowMC.o_kinds,nowMC.o_width,nowMC.o_height,nowMC._x,nowMC._y,nowMC.o_url,nowMC.getDepth(),nowMC.o_color,nowMC.o_drop,nowMC.o_blur,nowMC._rotation,nowMC["object"]._xscale,nowMC["object"]._yscale,nowMC.o_linecolor,nowMC.o_linewidth,nowMC.o_fillcolor,nowMC.o_circler,nowMC.o_link,nowMC.o_filter];
		_root["page"+_root.thisPage.nowPage].push(getArray);
	}
	
	public function rejpgObject(hd:Array, ni:Number):Void{
		o_nowid = ni;
		o_kinds = hd[0];
		o_width = hd[1];
		o_height = hd[2];
		o_x = hd[3];
		o_y = hd[4];
		o_url = hd[5];
		o_depth = hd[6];
		o_color = hd[7];
		o_drop = hd[8];
		o_blur = hd[9];
		o_rotation = hd[10];
		o_xscale = hd[11];
		o_yscale = hd[12];
		o_linecolor = hd[13];
		o_linewidth = hd[14];
		o_fillcolor = hd[15];
		o_circler = hd[16];
		o_link = hd[17];
		o_filter = hd[18];
		//trace(o_kinds+" 宽度："+o_width+" 高度"+o_height+" x="+o_x+" y="+o_y+" url="+o_url);
		_root.work.createEmptyMovieClip("object"+o_nowid,o_depth);
		_root.work["object"+o_nowid].o_nowid = o_nowid;
		_root.work["object"+o_nowid].o_kinds = o_kinds;
		_root.work["object"+o_nowid].o_width = o_width;
		_root.work["object"+o_nowid].o_height = o_height;
		_root.work["object"+o_nowid].o_url = o_url;
		_root.work["object"+o_nowid].o_color = o_color;
		_root.work["object"+o_nowid].o_drop = o_drop;
		_root.work["object"+o_nowid].o_blur = o_blur;
		_root.work["object"+o_nowid].o_linecolor = o_linecolor;
		_root.work["object"+o_nowid].o_linewidth = o_linewidth;
		_root.work["object"+o_nowid].o_fillcolor = o_fillcolor;
		_root.work["object"+o_nowid].o_circler = o_circler;
		_root.work["object"+o_nowid].o_link = o_link;
		_root.work["object"+o_nowid].o_filter = o_filter;
		
		_root.work["object"+o_nowid].createEmptyMovieClip("object", 0);
		_root.work["object"+o_nowid]["object"].loadMovie(o_url);
		
		_root.work["object"+o_nowid]._x = o_x;
		_root.work["object"+o_nowid]._y = o_y;
		_root.work["object"+o_nowid]._rotation = o_rotation;
		
		_root.work["object"+o_nowid]["object"]._xscale =o_xscale;
		_root.work["object"+o_nowid]["object"]._yscale =o_yscale;
		
		var nowMC:MovieClip=_root.work["object"+o_nowid];
		var filterOBJECTIN:filterOBJECT=new filterOBJECT();
		filterOBJECTIN.setFilter(nowMC,o_color,o_drop,o_blur);
	}
}
