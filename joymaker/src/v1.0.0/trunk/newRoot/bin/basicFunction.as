﻿/*
* copyright yuandejun@gmail.com;袁得钧.13980752339;
* 基础方法；
*/

import MCname;
class basicFunction extends MCname {
	public function basicFunction(){
	}
	public function buttonMusic():Void {
		var buttonRollOver:Sound = new Sound();
		buttonRollOver.attachSound("rollover_mp3");
		buttonRollOver.start();
	}
	
	public function alert(newString:String):Void {
		_root.createEmptyMovieClip("alert", 10000);
		_root.alert.createEmptyMovieClip("bg", 0);//背景色；
		_root.alert.createEmptyMovieClip("texta", 1);//文字内容；
		_root.alert.createEmptyMovieClip("buttona", 2);//按钮；
		_root.alert._x = Stage.width/2;
		_root.alert._y = Stage.height/2;
		
		_root.alert.bg.beginFill(000000, 60);
		_root.alert.bg.lineStyle(1, 000000, 100, true, "none", "round", "round", 1);
		_root.alert.bg.moveTo(0, 0);
		_root.alert.bg.lineTo(Stage.width, 0);
		_root.alert.bg.lineTo(Stage.width, Stage.height);
		_root.alert.bg.lineTo(0, Stage.height);
		_root.alert.bg.lineTo(0, 0);
		_root.alert.bg.endFill();
		_root.alert.bg._x = -Stage.width/2;
		_root.alert.bg._y = -Stage.height/2;
		_root.alert.bg.onRollOver = function() {
		};
		
		_root.alert.texta._x = 0;
		_root.alert.texta._y = 0;
		_root.alert.texta.createTextField("mytext", 1, 0, 0, 450, 300);
		_root.alert.texta.mytext.multiline = true;
		_root.alert.texta.mytext.wordWrap = true;
		_root.alert.texta.mytext.background =true;
		_root.alert.texta.mytext.border = false;
		_root.alert.texta.mytext._x=-_root.alert.texta.mytext._width/2;
		_root.alert.texta.mytext._y=-_root.alert.texta.mytext._height/2;
		var myformat:TextFormat = new TextFormat();
		myformat.underline = false;
		myformat.color = 0x000000;
		myformat.bold=true;
		_root.alert.texta.mytext.text = newString;
		_root.alert.texta.mytext.setTextFormat(myformat);
		
		trace(newString);

		_root.alert.buttona.createClassObject(mx.controls.Button, "okb", 0);
		_root.alert.buttona.okb.label = "关闭";
		_root.alert.buttona._x = -_root.alert.buttona.okb.width/2;
		_root.alert.buttona._y = _root.alert.texta.mytext._height-100;
		
		_root.alert.buttona.okb.onRelease = function() {
			this._parent._parent.removeMovieClip();
		};
	}//弹出警告信息；
	
	public function checkEmail(checkEmail:String):Boolean {
		if (checkEmail.charAt(0) == "." || checkEmail.charAt(0) == "@" || checkEmail.indexOf("@", 0) == -1 || checkEmail.indexOf(".", 0) == -1 || checkEmail.indexOf(".@", 0) != -1 || checkEmail.indexOf("@.", 0) != -1 || checkEmail.indexOf("..", 0) != -1 || checkEmail.lastIndexOf("@") != checkEmail.indexOf("@") || checkEmail.lastIndexOf("@") == checkEmail.length-1 || checkEmail.lastIndexOf(".") == checkEmail.length-1) {
			return false;
		} else {
			return true;
		}
	}//检查邮件地址是否合法；
	
	public function startDRECT():Void {
		_root.work.createEmptyMovieClip("dragRect", 80000);
		var nx:Number = _root._xmouse;
		var ny:Number = _root._ymouse;
		_root.work["dragRect"].nxx = nx;//x;
		_root.work["dragRect"].nyy = ny;//y;
		
		_root.work["dragRect"].onEnterFrame = function() {
			
			this.createEmptyMovieClip("Rect", 0);
			this.Rect.beginFill(0x00CCFF, 0);
			this.Rect.lineStyle(1, 0x00CCFF, 100, true, "none", "round", "round", 1);
			this.Rect.moveTo(this.nxx, this.nyy);
			this.Rect.lineTo(_root._xmouse, this.nyy);
			this.Rect.lineTo(_root._xmouse, _root._ymouse);
			this.Rect.lineTo(this.nxx, _root._ymouse);
			this.Rect.lineTo(this.nxx, this.nyy);
			this.Rect.endFill();
			this.widthis = this.Rect._width;
			this.heights = this.Rect._height;
			
			var xT:String;
			if (_root._xmouse<this.nxx) {
				xT = "false";
			} else {
				xT = "True";
			}
			this.xxT = xT;
			
			var yT:String;
			if (_root._ymouse<this.nyy) {
				yT = "false";
			} else {
				yT = "True";
			}
			this.yyT = yT;
		};
	}//拖拉方形；
	
	public function stopDRECT(mc:MovieClip):Array {
		var stopDRECT_Array:Array;
		delete mc.onEnterFrame;
		if (mc.xxT == "false") {
			var xx:Number = mc.nxx-mc.widthis;
		} else {
			var xx:Number = mc.nxx;
		}
		if (mc.yyT == "false") {
			var yy:Number = mc.nyy-mc.heights;
		} else {
			var yy:Number = mc.nyy;
		}
		stopDRECT_Array = [xx, yy, mc.widthis, mc.heights];
		mc.removeMovieClip();
		return stopDRECT_Array;
	}//终止拖拉方形；
	
	public function startDcircle():Void {
		_root.work.createEmptyMovieClip("dragRect", 80000);
		var nx:Number = _root._xmouse;
		var ny:Number = _root._ymouse;
		_root.work["dragRect"].nxx = nx;
		_root.work["dragRect"].nyy = ny;
		_root.work["dragRect"].onEnterFrame = function() {
			this.createEmptyMovieClip("Rect", 0);
			this.Rect.beginFill(0x00CCFF, 0);
			this.Rect.lineStyle(1, 0x00CCFF, 0, true, "none", "round", "round", 1);
			this.Rect.moveTo(this.nxx, this.nyy);
			this.Rect.lineTo(_root._xmouse, this.nyy);
			this.Rect.lineTo(_root._xmouse, _root._ymouse);
			this.Rect.lineTo(this.nxx, _root._ymouse);
			this.Rect.lineTo(this.nxx, this.nyy);
			this.Rect.endFill();
			this.widthis = this.Rect._width;
			this.heights = this.Rect._height;
			/*开始画圆形*/
			this.createEmptyMovieClip("circle", 1);
			this["circle"].beginFill(0x00ff00, 0);
			this["circle"].lineStyle(1, 0x00CCFF, 100, true, "none", "round", "round", 1);
			this["circle"]._x = this.nxx+(_root._xmouse-this.nxx)/2;
			this["circle"]._y = this.nyy+(_root._ymouse-this.nyy)/2;
			this.xx = this["circle"]._x;
			this.yy = this["circle"]._y;
			var scale:Number = Math.cos(Math.PI/8);
			var halfR:Number;
			if (this.widthis>this.heights) {
				halfR = this.widthis/2;
			} else {
				halfR = this.heights/2;
			}
			this.halfRi = halfR;
			this["circle"].moveTo(halfR, 0);
			for (var i:Number = Math.PI/4; i<=Math.PI*2; i += Math.PI/4) {
				var cx = Math.cos(i-Math.PI/8)*halfR/scale;
				var cy = Math.sin(i-Math.PI/8)*halfR/scale;
				var ax = Math.cos(i)*halfR;
				var ay = Math.sin(i)*halfR;
				this["circle"].curveTo(cx, cy, ax, ay);
			}
			this["circle"]._width = this.widthis;
			this["circle"]._height = this.heights;
			if (this.widthis>this.heights) {
				var xxscale:Number = 100;
				var yyscale:Number = this.heights/this.widthis*100;
			} else if (this.widthis<this.heights) {
				var yyscale:Number = 100;
				var xxscale:Number = this.widthis/this.heights*100;
			} else if (this.widthis == this.heights) {
				var xxscale:Number = 100;
				var yyscale:Number = 100;
			}
			
			this.xxscalep = xxscale;
			this.yyscalep = yyscale;
			/*开始画圆形*/
		};
	}//开始拖拉圆形；
	public function stopDcircle(mc:MovieClip):Array {
		var stopDRECT_Array:Array;
		delete mc.onEnterFrame;
		stopDRECT_Array = [mc.xx,mc.yy,mc.widthis,mc.heights,mc.halfRi,mc.xxscalep,mc.yyscalep];
		mc.removeMovieClip();
		return stopDRECT_Array;
	}//终止拖拉圆形；
}
