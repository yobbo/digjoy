﻿/*
copyright yuandejun@gmail.com 袁得钧 13980752339;
create new project , set _root.parameter project_name;
*/
import MCname;
import basicFunction;
import flash.filters.DropShadowFilter;
class newProject extends MCname {
	var selectEd:Number;
	//现在选择的模板；
	public function newProject() {
		var basicFunction_in:basicFunction = new basicFunction();
		var newProject_this:newProject = this;
		//复制自己；
		for (var i:Number = 0; i<2; i++) {
			_root[seleP[i]].onRollOver = function() {
				basicFunction_in.buttonMusic();
				this.gotoAndStop(2);
			};
			_root[seleP[i]].onRollOut = function() {
				this.gotoAndStop(1);
			};
		}
		_root[seleP[0]].onRelease = function() {
			newProject_this.getNewProject();
		};//新建项目；
		_root[seleP[1]].onRelease = function() {
			fscommand("openProject", "TRUE");
		};//打开项目；
		
	}
	
	public function getNewProject() {
		var nowProject:String;//定义现在的项目名称;
		var basicFunction_in:basicFunction = new basicFunction();
		var newProject_this:newProject = this;//复制自己;
		_root.nP_a.attachMovie([newPA[0]], [newPA[0]], 0);//开始载入新的newProjectMC.
		var getNP:MovieClip = _root.nP_a[newPA[0]];//定义getNp就是新载入的 _root.nP_a.newProject;
		var filter:DropShadowFilter = new DropShadowFilter(10, 90, 0x000000, 0.8, 16, 16, 1, 3, false, false, false);
		getNP.filters = [filter];//定义getNP是新建立项目的弹出菜单。
		
		getNP[newPA[1]].onRollOver = function() {
			basicFunction_in.buttonMusic();
			this.gotoAndStop(2);
		};
		getNP[newPA[1]].onRollOut = function() {
			this.gotoAndStop(1);
		};
		getNP[newPA[1]].onRelease = function() {
			trace(this._parent);
			this._parent.removeMovieClip();
		};
		//关闭按钮动作;
		var projectArrayH:Number = 0;//高度为0；
		for (var i:Number = 0; i<_root.projectArray.length; i++) {
			getNP[newPA[2]].attachMovie([newPA[5]], "list"+i, i);
			getNP[newPA[2]]["list"+i]._x = 0;
			getNP[newPA[2]]["list"+i]._y = projectArrayH;
			projectArrayH += getNP[newPA[2]]["list"+i]._height+10;
			getNP[newPA[2]]["list"+i].parameter = i;
			getNP[newPA[2]]["list"+i].projectName = _root.projectArray[i];
			getNP[newPA[2]]["list"+i].project_name.text = _root.projectArray[i];
			getNP[newPA[2]]["list"+i].onRollOver = function() {
				basicFunction_in.buttonMusic();
				this.gotoAndStop(2);
			};
			getNP[newPA[2]]["list"+i].onRollOut = function() {
				this.gotoAndStop(1);
			};
			getNP[newPA[2]]["list"+i].onRelease = function() {
				this._parent._parent[newProject_this.newPA[4]]._lockroot = true;
				nowProject = this.projectName;
				
				var mclListener:Object = new Object();
				mclListener.onLoadInit = function(target_mc:MovieClip) {
					fscommand("fullscreen", false);
					
					target_mc.face_mc.gotoAndStop(2);
					target_mc.face_mc.stencil_mc.gotoAndStop(1);
					target_mc._width=500;
					target_mc._height=380;
				};
				var image_mcl:MovieClipLoader = new MovieClipLoader();
				image_mcl.addListener(mclListener);
				image_mcl.loadClip(nowProject, this._parent._parent[newProject_this.newPA[4]]);
			};
		}
		getNP[newPA[3]].onRelease=function(){
			if(nowProject!=null){
				_root.project_name=nowProject;
				_root.gotoAndStop("working");
				this._parent.removeMovieClip();
			}else{
				basicFunction_in.alert("还没有选择模板文件");
			}
		}
	}
}
