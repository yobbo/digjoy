﻿/*
*	copyright yuandejun@gmail.com.袁得钧.13980752339
*	help software;
*/
class helpPanelList extends MCname {
	public function helpPanelList() {
	}
	public function setHelpPanel() {
		var nowThis:helpPanelList = this;
		var idhelp = setInterval(function () {
			if (_root.toolState.nowTool == nowThis.tools[1]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = "选择工具，在工作区点击鼠标左键选择物件并可拖动物件";
			} else if (_root.toolState.nowTool == nowThis.tools[2]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = "缩放工具，在工作区点击鼠标左键选择物件可缩放、拖拉、旋转";
			} else if (_root.toolState.nowTool == nowThis.tools[3]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = "平移工具，移动工作区位置";
			} else if (_root.toolState.nowTool == nowThis.tools[4]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tools[5]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = "文字工具，在工作区点击建立新文字，并进行缩放与改变文字";
			} else if (_root.toolState.nowTool == nowThis.tools[6]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tools[7]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tools[8]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tools[9]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = " ";
			} else if (_root.toolState.nowTool == nowThis.tools[10]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = "画圆工具，在工作拖动画出圆形";
			} else if (_root.toolState.nowTool == nowThis.tools[11]) {
				_root[nowThis.tools[0]][nowThis.tools[1]].text = "画矩形工具，在工作拖动画出矩形";
			}
		}, 10);
	}
	public function getHelp(p:String) {
		var nowThis:helpPanelList = this;
		if (p == tools[0]) {
			showTip("选择与缩放工具");
		} else if (p == tools[1]) {
			showTip("文本工具");
		} else if (p == tools[2]) {
			showTip("涂鸦工具");
		} else if (p == tools[3]) {
			showTip("画圆工具");
		} else if (p == tools[4]) {
			showTip("画方形工具");
		} else if (p == pageS[1]) {
			showTip("添加页码到当前页之前");
		} else if (p == pageS[2]) {
			showTip("删除页码");
		} else if (p == pageS[3]) {
			showTip("翻到前10页");
		} else if (p == pageS[4]) {
			showTip("翻到后10页");
		} else if (p == pageS[5]) {
			showTip("关闭当前面板");
		} else if (p == pageS[6]) {
			showTip("拖动面板四周可移动面板");
		} else if (p == resourP[1]) {
			showTip("");
		} else if (p == resourP[2]) {
			showTip("图片类型素材");
		} else if (p == resourP[3]) {
			showTip("Flash类型素材");
		} else if (p == resourP[4]) {
			showTip("音乐类型素材");
		} else if (p == resourP[5]) {
			showTip("上一页素材");
		} else if (p == resourP[6]) {
			showTip("下一页素材");
		} else if (p == resourP[7]) {
			showTip("关闭此面板");
		} else if (p == resourP[8]) {
			showTip("拖动面板四周可移动面板");
		} else if (p == resourP[9]) {
			showTip("用户添加素材");
		}
	}
	public function showTip(tip:String) {
		var idsppbegin = setInterval(function () {
			var xx:Number = 0;
			var yy:Number = 0;
			_root.createEmptyMovieClip("showTip", 60000);
			_root.showTip.createEmptyMovieClip("textF", 1);
			_root.showTip.createEmptyMovieClip("bg", 0);
			_root.showTip.textF.createTextField("my_sb", 0, 5, 5, 500, 20);
			_root.showTip.textF.my_sb.text = tip;
			_root.showTip.textF.my_sb.selectable = false;
			_root.showTip.textF.my_sb._width = _root.showTip.textF.my_sb.textWidth+10;
			_root.showTip.bg.beginFill(0xFFFFE1, 100);
			_root.showTip.bg.lineStyle(1, 0x000000, 100, true, "none", "round", "round", 2);
			_root.showTip.bg.moveTo(0, 0);
			_root.showTip.bg.lineTo(_root.showTip.textF.my_sb.textWidth+10, 0);
			_root.showTip.bg.lineTo(_root.showTip.textF.my_sb.textWidth+10, _root.showTip.textF.my_sb.textHeight+10);
			_root.showTip.bg.lineTo(0, _root.showTip.textF.my_sb.textHeight+10);
			_root.showTip.bg.lineTo(0, 0);
			_root.showTip.bg.endFill();
			if (_root._xmouse+_root.showTip._width>Stage.width) {
				xx = _root._xmouse-_root.showTip._width;
			} else {
				xx = _root._xmouse;
			}
			if (_root._ymouse+_root.showTip._height>Stage.height) {
				yy = _root._ymouse-_root.showTip._height;
			} else {
				yy = _root._ymouse;
			}
			_root.showTip._x = xx;
			_root.showTip._y = yy;
			clearInterval(idsppbegin);
		}, 500);
	}
	public function deleteTip(tip:String) {
		var idsppbeginn = setInterval(function () {
			_root.showTip.removeMovieClip();
			clearInterval(idsppbeginn);
		}, 500);
	}
}
