﻿import toolPp;
import assistant;
import exportXML;
class basicScale {
	private var newMc:MovieClip;
	private var stopSet:String;
	public function basicScale(mc:MovieClip) {
		var nowThis:basicScale = this;
		if (mc == null) {
			var newassistant:assistant = new assistant();
			newassistant.clearreopqs();
		} else {
			if (mc == _root.toolState.nowObject) {
				newMc = mc;
				var newtoolPp:toolPp = new toolPp();
				newtoolPp.setToolPp();
			} else {
				_root.objectInner.opq.removeMovieClip();
				_root.toolState.nowObject = mc;
				newMc = mc;
				var newtoolPp:toolPp = new toolPp();
				newtoolPp.setToolPp();
				_root.objectInner.createEmptyMovieClip("reopq", 999990);
				_root.objectInner.reopq._x = newMc._x;
				_root.objectInner.reopq._y = newMc._y;
				_root.objectInner.reopq._rotation = newMc._rotation;
				if ((_root["page"+_root.thisPage.nowPage][mc.id][0] == "load") || (_root["page"+_root.thisPage.nowPage][mc.id][0] == "draw_circle") || (_root["page"+_root.thisPage.nowPage][mc.id][0] == "draw_rect")) {
					for (var t:Number = 0; t<12; t++) {
						_root.objectInner.reopq.createEmptyMovieClip("reopqs"+t, t);
						if (t<=7) {
							_root.objectInner.reopq["reopqs"+t].tt = t;
							_root.objectInner.reopq["reopqs"+t].beginFill(0xFFFFFF, 75);
							_root.objectInner.reopq["reopqs"+t].lineStyle(2, 0x00CCFF, 75, true, "none", "round", "round", 1);
							_root.objectInner.reopq["reopqs"+t].moveTo(-8, 8);
							_root.objectInner.reopq["reopqs"+t].lineTo(8, 8);
							_root.objectInner.reopq["reopqs"+t].lineTo(8, -8);
							_root.objectInner.reopq["reopqs"+t].lineTo(-8, -8);
							_root.objectInner.reopq["reopqs"+t].lineTo(-8, 8);
							_root.objectInner.reopq["reopqs"+t].endFill();
							if (t == 0 || t == 2 || t == 4 || t == 6) {
								_root.objectInner.reopq["reopqs"+t].onPress = function() {
									nowThis.beginXYScale(nowThis.newMc, this._x, this._y);
								};
								_root.objectInner.reopq["reopqs"+t].onRelease = function() {
									nowThis.stopXYScale(nowThis.newMc);
								};
								_root.objectInner.reopq["reopqs"+t].onReleaseOutside = function() {
									nowThis.stopXYScale(nowThis.newMc);
								};
							}
							if (t == 7 || t == 3) {
								_root.objectInner.reopq["reopqs"+t].onPress = function() {
									nowThis.beginXScale(nowThis.newMc, this._x, this._y);
								};
								_root.objectInner.reopq["reopqs"+t].onRelease = function() {
									nowThis.stopXYScale(nowThis.newMc);
								};
								_root.objectInner.reopq["reopqs"+t].onReleaseOutside = function() {
									nowThis.stopXYScale(nowThis.newMc);
								};
							}
							if (t == 1 || t == 5) {
								_root.objectInner.reopq["reopqs"+t].onPress = function() {
									nowThis.beginYScale(nowThis.newMc, this._x, this._y);
								};
								_root.objectInner.reopq["reopqs"+t].onRelease = function() {
									nowThis.stopXYScale(nowThis.newMc);
								};
								_root.objectInner.reopq["reopqs"+t].onReleaseOutside = function() {
									nowThis.stopXYScale(nowThis.newMc);
								};
							}
						} else {
							_root.objectInner.reopq["reopqs"+t].beginFill(0xFFFFFF, 100);
							_root.objectInner.reopq["reopqs"+t].lineStyle(2, 0x00CCFF, 100, true, "none", "round", "round", 1);
							_root.objectInner.reopq["reopqs"+t].moveTo(0+6.5, 0);
							_root.objectInner.reopq["reopqs"+t].curveTo(6.5+0, Math.tan(Math.PI/8)*6.5+0, Math.sin(Math.PI/4)*6.5+0, Math.sin(Math.PI/4)*6.5+0);
							_root.objectInner.reopq["reopqs"+t].curveTo(Math.tan(Math.PI/8)*6.5+0, 6.5+0, 0, 6.5+0);
							_root.objectInner.reopq["reopqs"+t].curveTo(-Math.tan(Math.PI/8)*6.5+0, 6.5+0, -Math.sin(Math.PI/4)*6.5+0, Math.sin(Math.PI/4)*6.5+0);
							_root.objectInner.reopq["reopqs"+t].curveTo(-6.5+0, Math.tan(Math.PI/8)*6.5+0, -6.5+0, 0);
							_root.objectInner.reopq["reopqs"+t].curveTo(-6.5+0, -Math.tan(Math.PI/8)*6.5+0, -Math.sin(Math.PI/4)*6.5+0, -Math.sin(Math.PI/4)*6.5+0);
							_root.objectInner.reopq["reopqs"+t].curveTo(-Math.tan(Math.PI/8)*6.5+0, -6.5+0, 0, -6.5+0);
							_root.objectInner.reopq["reopqs"+t].curveTo(Math.tan(Math.PI/8)*6.5+0, -6.5+0, Math.sin(Math.PI/4)*6.5+0, -Math.sin(Math.PI/4)*6.5+0);
							_root.objectInner.reopq["reopqs"+t].curveTo(6.5+0, -Math.tan(Math.PI/8)*6.5+0, 6.5+0, 0);
							_root.objectInner.reopq["reopqs"+t].endFill();
							_root.objectInner.reopq["reopqs"+t].onPress = function() {
								nowThis.rotation(nowThis.newMc);
							};
							_root.objectInner.reopq["reopqs"+t].onRelease = function() {
								nowThis.stopRotation(nowThis.newMc);
							};
							_root.objectInner.reopq["reopqs"+t].onReleaseOutside = function() {
								nowThis.stopRotation(nowThis.newMc);
							};
						}
					}
				}
				_root.objectInner.createEmptyMovieClip("opq", 999900);
				_root.objectInner.opq.createEmptyMovieClip("object", 0);
				_root.objectInner.opq.object.beginFill(0xFF0000, 0);
				_root.objectInner.opq.object.lineStyle(1, 0x00CCFF, 75, true, "none", "round", "round", 1);
				_root.objectInner.opq.object.moveTo(0, 0);
				_root.objectInner.opq.object.lineTo(newMc.object._width, 0);
				_root.objectInner.opq.object.lineTo(newMc.object._width, newMc.object._height);
				_root.objectInner.opq.object.lineTo(0, newMc.object._height);
				_root.objectInner.opq.object.lineTo(0, 0);
				_root.objectInner.opq.object.endFill();
				if (_root["page"+_root.thisPage.nowPage][newMc.id][0] != "txt") {
					_root.objectInner.opq.object._x = -_root.objectInner.opq.object._width/2;
					_root.objectInner.opq.object._y = -_root.objectInner.opq.object._height/2;
				}
				_root.objectInner.opq._x = newMc._x;
				_root.objectInner.opq._y = newMc._y;
				_root.objectInner.opq._rotation = newMc._rotation;
				_root.objectInner.opq.onPress = function() {
					nowThis.stopSet = "true";
					this.startDrag();
					nowThis.thisMCXY(nowThis.newMc);
				};
				_root.objectInner.opq.onRelease = function() {
					nowThis.stopSet = "false";
					this.stopDrag();
					nowThis.thisMCXX(nowThis.newMc);
				};
				_root.objectInner.opq.onReleaseOutside = function() {
					this.stopDrag();
					nowThis.stopSet = "false";
					nowThis.thisMCXX(nowThis.newMc);
				};
				var idss = setInterval(function () {
					if ((_root.toolState.nowObject == null) || (_root.toolState.nowObject != nowThis.newMc)) {
						clearInterval(idss);
					}
					if (Key.isDown(Key.LEFT)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						nowThis.newMc._x = nowThis.newMc._x-1;
						_root.objectInner.opq._x = _root.objectInner.opq._x-1;
						_root.objectInner.reopq._x = _root.objectInner.reopq._x-1;
						_root["page"+_root.thisPage.nowPage][nowThis.newMc.id][3] = nowThis.newMc._x;
					} else if (Key.isDown(Key.DELETEKEY)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						delete _root["page"+_root.thisPage.nowPage][nowThis.newMc.id];
						nowThis.newMc.removeMovieClip();
						var newassistant:assistant = new assistant();
						newassistant.clearreopqs();
					} else if (Key.isDown(Key.UP)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						nowThis.newMc._y = nowThis.newMc._y-1;
						_root.objectInner.opq._y = _root.objectInner.opq._y-1;
						_root.objectInner.reopq._y = _root.objectInner.reopq._y-1;
						_root["page"+_root.thisPage.nowPage][nowThis.newMc.id][4] = nowThis.newMc._y;
					} else if (Key.isDown(Key.RIGHT)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						nowThis.newMc._x = nowThis.newMc._x+1;
						_root.objectInner.opq._x = _root.objectInner.opq._x+1;
						_root.objectInner.reopq._x = _root.objectInner.reopq._x+1;
						_root["page"+_root.thisPage.nowPage][nowThis.newMc.id][3] = nowThis.newMc._x;
					} else if (Key.isDown(Key.DOWN)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						nowThis.newMc._y = nowThis.newMc._y+1;
						_root.objectInner.opq._y = _root.objectInner.opq._y+1;
						_root.objectInner.reopq._y = _root.objectInner.reopq._y+1;
						_root["page"+_root.thisPage.nowPage][nowThis.newMc.id][4] = nowThis.newMc._y;
					} else if (Key.isDown(189)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						var setMC:MovieClip = nowThis.newMc;
						var newMC:MovieClip = _root.objectInner.getInstanceAtDepth(setMC.getDepth()-1);
						setMC.swapDepths(newMC);
						_root["page"+_root.thisPage.nowPage][newMC.id][6] = newMC.getDepth();
						_root["page"+_root.thisPage.nowPage][setMC.id][6] = setMC.getDepth();
					} else if (Key.isDown(187)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						var setMC:MovieClip = nowThis.newMc;
						var newMC:MovieClip = _root.objectInner.getInstanceAtDepth(setMC.getDepth()+1);
						setMC.swapDepths(newMC);
						_root["page"+_root.thisPage.nowPage][newMC.id][6] = newMC.getDepth();
						_root["page"+_root.thisPage.nowPage][setMC.id][6] = setMC.getDepth();
					}
				}, 50);
				if ((_root["page"+_root.thisPage.nowPage][mc.id][0] == "load") || (_root["page"+_root.thisPage.nowPage][mc.id][0] == "draw_circle") || (_root["page"+_root.thisPage.nowPage][mc.id][0] == "draw_rect")) {
					_root.objectInner.reopq["reopqs"+0].onEnterFrame = function() {
						this._x = -_root.objectInner.opq.object._width/2;
						this._y = -_root.objectInner.opq.object._height/2;
					};
					_root.objectInner.reopq["reopqs"+1].onEnterFrame = function() {
						this._x = 0;
						this._y = -_root.objectInner.opq.object._height/2;
					};
					_root.objectInner.reopq["reopqs"+2].onEnterFrame = function() {
						this._x = _root.objectInner.opq.object._width/2;
						this._y = -_root.objectInner.opq.object._height/2;
					};
					_root.objectInner.reopq["reopqs"+3].onEnterFrame = function() {
						this._x = _root.objectInner.opq.object._width/2;
						this._y = 0;
					};
					_root.objectInner.reopq["reopqs"+4].onEnterFrame = function() {
						this._x = _root.objectInner.opq.object._width/2;
						this._y = _root.objectInner.opq.object._height/2;
					};
					_root.objectInner.reopq["reopqs"+5].onEnterFrame = function() {
						this._x = 0;
						this._y = _root.objectInner.opq.object._height/2;
					};
					_root.objectInner.reopq["reopqs"+6].onEnterFrame = function() {
						this._x = -_root.objectInner.opq.object._width/2;
						this._y = +_root.objectInner.opq.object._height/2;
					};
					_root.objectInner.reopq["reopqs"+7].onEnterFrame = function() {
						this._x = -_root.objectInner.opq.object._width/2;
						this._y = 0;
					};
					_root.objectInner.reopq["reopqs"+8].onEnterFrame = function() {
						this._x = _root.objectInner.reopq["reopqs"+0]._x-_root.objectInner.reopq["reopqs"+0]._width;
						this._y = _root.objectInner.reopq["reopqs"+0]._y-_root.objectInner.reopq["reopqs"+0]._height;
					};
					_root.objectInner.reopq["reopqs"+9].onEnterFrame = function() {
						this._x = _root.objectInner.reopq["reopqs"+2]._x+_root.objectInner.reopq["reopqs"+0]._width;
						this._y = _root.objectInner.reopq["reopqs"+2]._y-_root.objectInner.reopq["reopqs"+0]._height;
					};
					_root.objectInner.reopq["reopqs"+10].onEnterFrame = function() {
						this._x = _root.objectInner.reopq["reopqs"+4]._x+_root.objectInner.reopq["reopqs"+0]._width;
						this._y = _root.objectInner.reopq["reopqs"+4]._y+_root.objectInner.reopq["reopqs"+0]._height;
					};
					_root.objectInner.reopq["reopqs"+11].onEnterFrame = function() {
						this._x = _root.objectInner.reopq["reopqs"+6]._x-_root.objectInner.reopq["reopqs"+0]._width;
						this._y = _root.objectInner.reopq["reopqs"+6]._y+_root.objectInner.reopq["reopqs"+0]._height;
					};
				}
			}
		}
	}
	private function thisMCXY(getMC:MovieClip) {
		var nowThis:basicScale = this;
		var selectp = setInterval(function () {
			if ((_root.toolState.nowObject == null) || (_root.toolState.nowObject != getMC) || (nowThis.stopSet == "false")) {
				clearInterval(selectp);
			}
			getMC._x = _root.objectInner.opq._x;
			getMC._y = _root.objectInner.opq._y;
			_root.objectInner.reopq._x = _root.objectInner.opq._x;
			_root.objectInner.reopq._y = _root.objectInner.opq._y;
		}, 1);
	}
	private function thisMCXX(getMC:MovieClip) {
		_root.CHANGED = "TRUE";
		delete getMC.onEnterFrame;
		_root["page"+_root.thisPage.nowPage][getMC.id][3] = getMC._x;
		_root["page"+_root.thisPage.nowPage][getMC.id][4] = getMC._y;
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
	}
	private function beginXYScale(getMC:MovieClip, rx:Number, ry:Number) {
		_root.CHANGED = "TRUE";
		var nowThis:basicScale = this;
		getMC.nowX = _root._xmouse;
		getMC.nowY = _root._ymouse;
		getMC.nowSxscale = getMC.object._xscale;
		getMC.nowSyscale = getMC.object._yscale;
		getMC.rxx = rx;
		getMC.ryy = ry;
		getMC.onEnterFrame = function() {
			if (Key.isDown(Key.SHIFT)) {
				this.object._yscale = this.object._xscale=_root._xmouse-this.nowX+this.nowSxscale;
			} else {
				if (getMC.rxx<0) {
					if (this.nowSxscale>0) {
						this.object._xscale = -(_root._xmouse-this.nowX)+this.nowSxscale;
					} else if (this.nowSxscale<0) {
						this.object._xscale = _root._xmouse-this.nowX+this.nowSxscale;
					}
				} else {
					if (this.nowSxscale>0) {
						this.object._xscale = _root._xmouse-this.nowX+this.nowSxscale;
					} else if (this.nowSxscale<0) {
						this.object._xscale = -(_root._xmouse-this.nowX)+this.nowSxscale;
					}
				}
				if (getMC.ryy<0) {
					if (this.nowSyscale>0) {
						this.object._yscale = -(_root._ymouse-this.nowY)+this.nowSyscale;
					} else if (this.nowSyscale<0) {
						this.object._yscale = _root._ymouse-this.nowY+this.nowSyscale;
					}
				} else {
					if (this.nowSyscale>0) {
						this.object._yscale = _root._ymouse-this.nowY+this.nowSyscale;
					} else if (this.nowSyscale<0) {
						this.object._yscale = -(_root._ymouse-this.nowY)+this.nowSyscale;
					}
				}
			}
			if (this.object._xscale>0) {
				if (_root["page"+_root.thisPage.nowPage][this.id][0] != "draw_circle") {
					this.object._x = -this.object._width/2;
				}
			} else if (this.object._xscale<0) {
				if (_root["page"+_root.thisPage.nowPage][this.id][0] != "draw_circle") {
					this.object._x = this.object._width/2;
				}
			}
			if (this.object._yscale>0) {
				if (_root["page"+_root.thisPage.nowPage][this.id][0] != "draw_circle") {
					this.object._y = -this.object._height/2;
				}
			} else if (this.object._yscale<0) {
				if (_root["page"+_root.thisPage.nowPage][this.id][0] != "draw_circle") {
					this.object._y = this.object._height/2;
				}
			}
			_root.objectInner.opq.object._width = this.object._width;
			_root.objectInner.opq.object._height = this.object._height;
			_root.objectInner.opq.object._x = -_root.objectInner.opq.object._width/2;
			_root.objectInner.opq.object._y = -_root.objectInner.opq.object._height/2;
		};
	}
	private function beginXScale(getMC:MovieClip, rx:Number, ry:Number) {
		_root.CHANGED = "TRUE";
		var nowThis:basicScale = this;
		getMC.nowX = _root._xmouse;
		getMC.nowY = _root._ymouse;
		getMC.nowSxscale = getMC.object._xscale;
		getMC.nowSyscale = getMC.object._yscale;
		getMC.rxx = rx;
		getMC.ryy = ry;
		getMC.onEnterFrame = function() {
			if (getMC.rxx<0) {
				if (this.nowSxscale>0) {
					this.object._xscale = -(_root._xmouse-this.nowX)+this.nowSxscale;
				} else if (this.nowSxscale<0) {
					this.object._xscale = _root._xmouse-this.nowX+this.nowSxscale;
				}
			} else {
				if (this.nowSxscale>0) {
					this.object._xscale = _root._xmouse-this.nowX+this.nowSxscale;
				} else if (this.nowSxscale<0) {
					this.object._xscale = -(_root._xmouse-this.nowX)+this.nowSxscale;
				}
			}
			if (this.object._xscale>0) {
				if (_root["page"+_root.thisPage.nowPage][this.id][0] != "draw_circle") {
					this.object._x = -this.object._width/2;
				}
			} else if (this.object._xscale<0) {
				if (_root["page"+_root.thisPage.nowPage][this.id][0] != "draw_circle") {
					this.object._x = this.object._width/2;
				}
			}
			_root.objectInner.opq.object._width = this.object._width;
			_root.objectInner.opq.object._height = this.object._height;
			_root.objectInner.opq.object._x = -_root.objectInner.opq.object._width/2;
			_root.objectInner.opq.object._y = -_root.objectInner.opq.object._height/2;
		};
	}
	private function beginYScale(getMC:MovieClip, rx:Number, ry:Number) {
		_root.CHANGED = "TRUE";
		var nowThis:basicScale = this;
		getMC.nowX = _root._xmouse;
		getMC.nowY = _root._ymouse;
		getMC.nowSxscale = getMC.object._xscale;
		getMC.nowSyscale = getMC.object._yscale;
		getMC.rxx = rx;
		getMC.ryy = ry;
		getMC.onEnterFrame = function() {
			if (getMC.ryy<0) {
				if (this.nowSyscale>0) {
					this.object._yscale = -(_root._ymouse-this.nowY)+this.nowSyscale;
				} else if (this.nowSyscale<0) {
					this.object._yscale = _root._ymouse-this.nowY+this.nowSyscale;
				}
			} else {
				if (this.nowSyscale>0) {
					this.object._yscale = _root._ymouse-this.nowY+this.nowSyscale;
				} else if (this.nowSyscale<0) {
					this.object._yscale = -(_root._ymouse-this.nowY)+this.nowSyscale;
				}
			}
			if (this.object._yscale>0) {
				if (_root["page"+_root.thisPage.nowPage][this.id][0] != "draw_circle") {
					this.object._y = -this.object._height/2;
				}
			} else if (this.object._yscale<0) {
				if (_root["page"+_root.thisPage.nowPage][this.id][0] != "draw_circle") {
					this.object._y = this.object._height/2;
				}
			}
			_root.objectInner.opq.object._width = this.object._width;
			_root.objectInner.opq.object._height = this.object._height;
			_root.objectInner.opq.object._x = -_root.objectInner.opq.object._width/2;
			_root.objectInner.opq.object._y = -_root.objectInner.opq.object._height/2;
		};
	}
	private function stopXYScale(getMC:MovieClip) {
		delete getMC.onEnterFrame;
		_root["page"+_root.thisPage.nowPage][getMC.id][9] = getMC.object._xscale;
		_root["page"+_root.thisPage.nowPage][getMC.id][10] = getMC.object._yscale;
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
	}
	private function rotation(getMC:MovieClip) {
		getMC.r = getMC._rotation;
		getMC.x = _root._xmouse;
		getMC.y = _root._ymouse;
		var thisogj:basicScale = this;
		getMC.onEnterFrame = function() {
			var line:MovieClip = getMC;
			var dy:Number = line.y-line._y;
			var dx:Number = line.x-line._x;
			var ang:Number = Math.atan2(dy, dx);
			var a1:Number = ang*180/Math.PI;
			//
			dy = _root._ymouse-line._y;
			dx = _root._xmouse-line._x;
			ang = Math.atan2(dy, dx);
			var a2:Number = ang*180/Math.PI;
			line._rotation += a2-a1;
			_root.objectInner.opq._rotation += a2-a1;
			_root.objectInner.reopq._rotation += a2-a1;
			line.r = line._rotation;
			line.x = _root._xmouse;
			line.y = _root._ymouse;
		};
	}
	private function stopRotation(getMC:MovieClip) {
		_root.CHANGED = "TRUE";
		delete getMC.onEnterFrame;
		_root["page"+_root.thisPage.nowPage][getMC.id][7] = getMC._rotation;
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
	}
}
