﻿/*
copyright yuandejun@gmail.com.袁得钧.13980752339;
yuandejun@gmail.com
*/
import basicFunction;
class loginSuccess extends MCname {
	public function loginSuccess() {
		
		if (_root.MakerUserLogin == false) {
			_root[ral[0]].gotoAndStop("login");
		}
		
		var basicFunction_in:basicFunction = new basicFunction();
		var loginThis:loginSuccess = this;
		
		_root[ral[0]][ral[15]].onRelease = function() {
			basicFunction_in.buttonMusic();
			_root.MakerUserLogin = false;
			this._parent.gotoAndStop("login");
		};
		
		_root[ral[0]][ral[16]].text = _root.MakerUserName;
		_root[ral[0]][ral[17]].text = _root.MakerUserEmail;
	}
}
