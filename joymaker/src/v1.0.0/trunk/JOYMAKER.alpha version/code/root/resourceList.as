﻿import resourceGetLocal;
import resourceGetSelf;
import resourceThumb;
import createNewAndAddArr;
import helpPanelList;
class resourceList extends rmcName {
	private var nowActionS:String;
	private var selectProjectNow:String;
	private var swfResourceT:Array;
	private var picResourceT:Array;
	private var musicResourceT:Array;
	private var nowResourceArray:Array;
	private var nowPage:Number = 1;
	private var totalPage:Number;
	private var clearStart:Number;
	private var clearEnd:Number;
	public function resourceList(newArr:Array) {
		var nowThis:resourceList = this;
		nowActionS = newArr[0];
		if (nowActionS == "defaultR") {
			var newresourceGetLocal:resourceGetLocal = new resourceGetLocal();
			newresourceGetLocal.getpicResource();
			newresourceGetLocal.getswfResource();
			newresourceGetLocal.getmusicResource();
			swfResourceT = newresourceGetLocal.swfResource;
			picResourceT = newresourceGetLocal.picResource;
			musicResourceT = newresourceGetLocal.musicResource;
		} else if (nowActionS == "personalR") {
			var newresourceGetSelf:resourceGetSelf = new resourceGetSelf();
			newresourceGetSelf.getpicResource();
			newresourceGetSelf.getswfResource();
			newresourceGetSelf.getmusicResource();
			swfResourceT = newresourceGetSelf.swfResource;
			picResourceT = newresourceGetSelf.picResource;
			musicResourceT = newresourceGetSelf.musicResource;
		}
		_root[rP[0]].nowResource.attachMovie("resourceList", "resourceListP", 100);
		selectPP();
		selectProjectNow = rP[2];
		_root[rP[0]].nowResource.resourceListP[rP[2]].gotoAndStop(3);
		beginResource(selectProjectNow);
		less();
	}
	private function less():Void {
		var nowThis:resourceList = this;
		var reRes = setInterval(function () {
			if (nowThis.nowActionS != _root.resource.kinds) {
				clearInterval(reRes);
			}
			if (nowThis.nowActionS == "defaultR") {
				var newresourceGetLocal:resourceGetLocal = new resourceGetLocal();
				newresourceGetLocal.getpicResource();
				newresourceGetLocal.getswfResource();
				newresourceGetLocal.getmusicResource();
				if ((nowThis.swfResourceT.length == newresourceGetLocal.swfResource.length) && (nowThis.picResourceT.length == newresourceGetLocal.picResource.length) && (nowThis.musicResourceT.length == newresourceGetLocal.musicResource.length)) {
				} else {
					nowThis.swfResourceT = newresourceGetLocal.swfResource;
					nowThis.picResourceT = newresourceGetLocal.picResource;
					nowThis.musicResourceT = newresourceGetLocal.musicResource;
					nowThis.beginResource(nowThis.selectProjectNow);
				}
			} else if (nowThis.nowActionS == "personalR") {
				var newresourceGetSelf:resourceGetSelf = new resourceGetSelf();
				newresourceGetSelf.getpicResource();
				newresourceGetSelf.getswfResource();
				newresourceGetSelf.getmusicResource();
				if ((nowThis.swfResourceT.length == newresourceGetSelf.swfResource.length) && (nowThis.picResourceT.length == newresourceGetSelf.picResource.length) && (nowThis.musicResourceT.length == newresourceGetSelf.musicResource.length)) {
				} else {
					nowThis.swfResourceT = newresourceGetSelf.swfResource;
					nowThis.picResourceT = newresourceGetSelf.picResource;
					nowThis.musicResourceT = newresourceGetSelf.musicResource;
					nowThis.beginResource(nowThis.selectProjectNow);
				}
			}
		}, 1000);
	}
	private function beginResource(nowdefaultSP:String):Void {
		if (nowdefaultSP == "rImportP") {
			listA(picResourceT);
		} else if (nowdefaultSP == "rImportF") {
			listA(swfResourceT);
		} else if (nowdefaultSP == "rImportM") {
			listA(musicResourceT);
		}
	}
	//分辨出选择的到底是哪类，是mp3是swf还是pic?
	public function listA(nowArraT:Array):Void {
		nowResourceArray = nowArraT;
		if ((selectProjectNow == "rImportF") && (nowActionS == "personalR")) {
			totalPage = Math.floor(nowArraT.length/10)+1;
		} else if (selectProjectNow == "rImportM") {
			totalPage = Math.floor(nowArraT.length/10)+1;
		} else {
			totalPage = Math.floor(nowArraT.length/6)+1;
		}
		//判断总共页数，这里根据主面板选择与里面的素材种类来确定；
		nextButtonStart();
		goPage(nowPage);
	}
	//根据选择的哪类而收到数组，开始分页，并且执行页面跳转1；
	public function goPage(nPage:Number):Void {
		var nowThis:resourceList = this;
		_root[rP[0]].nowResource.resourceListP.thisPageN.text = nPage;
		if (selectProjectNow == "rImportP") {
			var newH:Number = -80;
			var startN:Number = (nPage-1)*6;
			var endN:Number = (nPage-1)*6+6;
			if (endN>nowResourceArray.length) {
				endN = (nPage-1)*6+nowResourceArray.length%6;
			}
			//算出每个循环开始的数与结束的数，这里是指出为 rImportP 类型时为6个；                       
		} else if ((selectProjectNow == "rImportF") && (nowActionS == "personalR")) {
			var newH:Number = -50;
			//开始排序的高度是-50;
			var startN:Number = (nPage-1)*10;
			var endN:Number = (nPage-1)*10+10;
			if (endN>nowResourceArray.length) {
				endN = (nPage-1)*10+nowResourceArray.length%10;
			}
			//算出每个循环开始的数与结束的数，这里是指出为 personalR 与 personalR 时为10个；                       
		} else if (selectProjectNow == "rImportM") {
			var newH:Number = -50;
			//开始排序的高度是-50;
			var startN:Number = (nPage-1)*10;
			var endN:Number = (nPage-1)*10+10;
			if (endN>nowResourceArray.length) {
				endN = (nPage-1)*10+nowResourceArray.length%10;
			}
			//算出每个循环开始的数与结束的数，这里是指出为 rImportM 时为10个；                       
		} else if ((selectProjectNow == "rImportF") && (nowActionS == "defaultR")) {
			var newH:Number = -80;
			//开始排序的高度是-80;
			var startN:Number = (nPage-1)*6;
			var endN:Number = (nPage-1)*6+6;
			if (endN>nowResourceArray.length) {
				endN = (nPage-1)*6+nowResourceArray.length%6;
			}
			//算出每个循环开始的数与结束的数，这里是指出为 rImportF 与 defaultR 类型时为6个；                       
		}
		clearM(clearStart, clearEnd, startN, endN);
		for (var i:Number = startN; i<endN; i++) {
			//**第一类**//
			if ((selectProjectNow == "rImportF") && (nowActionS == "defaultR")) {
				_root[rP[0]].nowResource.resourceListP.objectUrl.attachMovie("resourceListMC", "listMC"+i, i);
				newH = newH+72;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i]._y = newH;
				var mclListener:Object = new Object();
				mclListener.onLoadInit = function(target_mc:MovieClip) {
					var w:Number = target_mc._width;
					var h:Number = target_mc._height;
					if (w>h) {
						if (w != 68) {
							var bilv:Number = 68/w;
							target_mc._width = 68;
							target_mc._height = bilv*h;
						}
					} else if (h>w) {
						if (h != 56) {
							var bilv:Number = 56/w;
							target_mc._height = 56;
							target_mc._width = bilv*w;
						}
					} else if (h>68) {
						var bilv:Number = 56/w;
						target_mc._height = 56;
						target_mc._width = bilv*w;
					}
				};
				var image_mcl:MovieClipLoader = new MovieClipLoader();
				image_mcl.addListener(mclListener);
				image_mcl.loadClip(nowResourceArray[i], _root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].resourceUrl);
				//这段代码判断是否是否图片加载开始否，如果加载开始，就要判断图片的高宽之比；
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].opp = i;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].onRollOver = function() {
					this.duplicateMovieClip("justDr", 10000);
					//新建立一个一个名为 justDr 的MovieClip;
					this._parent.justDr.opp = this.opp;
					//将i数分配给 justDr.
					this._parent.justDr.onRollOver = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.startThumb();
					};
					this._parent.justDr.onPress = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endThumbs();
						var mclListener:Object = new Object();
						mclListener.onLoadInit = function(target_mc:MovieClip) {
							var w:Number = target_mc._width;
							var h:Number = target_mc._height;
							if (w>h) {
								if (w != 68) {
									var bilv:Number = 68/w;
									target_mc._width = 68;
									target_mc._height = bilv*h;
								}
							} else if (h>w) {
								if (h != 56) {
									var bilv:Number = 56/w;
									target_mc._height = 56;
									target_mc._width = bilv*w;
								}
							} else if (h>68) {
								var bilv:Number = 56/w;
								target_mc._height = 56;
								target_mc._width = bilv*w;
							}
						};
						var image_mcl:MovieClipLoader = new MovieClipLoader();
						image_mcl.addListener(mclListener);
						image_mcl.loadClip(nowThis.nowResourceArray[this.opp], this.resourceUrl);
						this.startDrag();
					};
					this._parent.justDr.onRelease = function() {
						this.stopDrag();
						var getChart:String;
						var startChart:Number;
						startChart = nowThis.nowResourceArray[this.opp].lastIndexOf("\\");
						getChart = nowThis.nowResourceArray[this.opp].slice(startChart+1, nowThis.nowResourceArray[this.opp].length);
						startChart = getChart.lastIndexOf(".");
						getChart = getChart.slice(0, startChart);
						_root.testText1.text += newline+_root[nowThis.rP[0]]+" "+this.hitTest(_root[nowThis.rP[0]]);
						var newcreateNewAndAddArr:createNewAndAddArr = new createNewAndAddArr();
						newcreateNewAndAddArr.createObjectsI("resource\\swfs\\movieClip\\"+getChart, _root._xmouse, _root._ymouse);
						//开始将这个值显示在工作区中；       
						this.removeMovieClip();
					};
					this._parent.justDr.onRollOut = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endThumbs();
						this.stopDrag();
						this.removeMovieClip();
					};
					this._parent.justDr.onReleaseOutside = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endThumbs();
						this.stopDrag();
						this.removeMovieClip();
					};
				};
			}
			//**第一类**//                       
			//**第二类**//
			if ((selectProjectNow == "rImportF") && (nowActionS == "personalR")) {
				_root[rP[0]].nowResource.resourceListP.objectUrl.attachMovie("resourceListMCSwf", "listMC"+i, i);
				newH = newH+41;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i]._y = newH;
				//开始根据数组进行列表了，目前是图片显示，每页为10个；
				var getChart:String;
				var startChart:Number;
				startChart = nowResourceArray[i].lastIndexOf("\\");
				getChart = nowResourceArray[i].slice(startChart+1, nowResourceArray[i].length);
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].getMpFlName.text = getChart;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].opp = i;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].onRollOver = function() {
					this.duplicateMovieClip("justDr", 10000);
					//新建立一个一个名为 justDr 的MovieClip;
					this._parent.justDr.opp = this.opp;
					//将i数分配给 justDr.
					this._parent.justDr.onRollOver = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.startThumb();
					};
					this._parent.justDr.onPress = function() {
						this.getMpFlName.text = "动画素材";
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endThumbs();
						var mclListener:Object = new Object();
						mclListener.onLoadInit = function(target_mc:MovieClip) {
							var w:Number = target_mc._width;
							var h:Number = target_mc._height;
							if (w>h) {
								if (w != 68) {
									var bilv:Number = 68/w;
									target_mc._width = 68;
									target_mc._height = bilv*h;
								}
							} else if (h>w) {
								if (h != 56) {
									var bilv:Number = 56/w;
									target_mc._height = 56;
									target_mc._width = bilv*w;
								}
							}
						};
						var image_mcl:MovieClipLoader = new MovieClipLoader();
						image_mcl.addListener(mclListener);
						image_mcl.loadClip(nowThis.nowResourceArray[this.opp], this.resourceUrl);
						this.startDrag();
					};
					this._parent.justDr.onRelease = function() {
						this.stopDrag();
						var newcreateNewAndAddArr:createNewAndAddArr = new createNewAndAddArr();
						newcreateNewAndAddArr.createObjectsI(nowThis.nowResourceArray[this.opp], _root._xmouse, _root._ymouse);
						//开始将这个值显示在工作区中；
						this.removeMovieClip();
					};
					this._parent.justDr.onRollOut = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endThumbs();
						this.stopDrag();
						this.removeMovieClip();
					};
					this._parent.justDr.onReleaseOutside = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endThumbs();
						this.stopDrag();
						this.removeMovieClip();
					};
				};
			}
			//**第二类**//                       
			//**第三类**//
			if (selectProjectNow == "rImportM") {
				_root[rP[0]].nowResource.resourceListP.objectUrl.attachMovie("resourceListMCSwf", "listMC"+i, i);
				newH = newH+41;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i]._y = newH;
				//开始根据数组进行列表了，目前是图片显示，每页为10个；
				var getChart:String;
				var startChart:Number;
				startChart = nowResourceArray[i].lastIndexOf("\\");
				getChart = nowResourceArray[i].slice(startChart+1, nowResourceArray[i].length);
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].getMpFlName.text = getChart;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].opp = i;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].onRollOver = function() {
					this.duplicateMovieClip("justDr", 10000);
					//新建立一个一个名为 justDr 的MovieClip;
					this._parent.justDr.opp = this.opp;
					//将i数分配给 justDr.
					this._parent.justDr.onRollOver = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.beginShow();
					};
					this._parent.justDr.onPress = function() {
						this.getMpFlName.text = "声音素材";
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endMusic();
						this.startDrag();
					};
					this._parent.justDr.onRelease = function() {
						this.stopDrag();
						var newcreateNewAndAddArr:createNewAndAddArr = new createNewAndAddArr();
						newcreateNewAndAddArr.createMusicsI(nowThis.nowResourceArray[this.opp], _root._xmouse, _root._ymouse);
						//开始将这个值显示在工作区中；
						this.removeMovieClip();
					};
					this._parent.justDr.onRollOut = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endMusic();
						this.stopDrag();
						this.removeMovieClip();
					};
					this._parent.justDr.onReleaseOutside = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endMusic();
						this.stopDrag();
						this.removeMovieClip();
					};
				};
			}
			//**第三类**//                       
			//**第四类**//
			if (selectProjectNow == "rImportP") {
				_root[rP[0]].nowResource.resourceListP.objectUrl.attachMovie("resourceListMC", "listMC"+i, i);
				newH = newH+72;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i]._y = newH;
				//开始根据数组进行列表了，目前是图片显示，每页为6个；
				var mclListener:Object = new Object();
				mclListener.onLoadInit = function(target_mc:MovieClip) {
					var w:Number = target_mc._width;
					var h:Number = target_mc._height;
					if (w>h) {
						if (w != 68) {
							var bilv:Number = 68/w;
							target_mc._width = 68;
							target_mc._height = bilv*h;
						}
					} else if (h>w) {
						if (h != 56) {
							var bilv:Number = 56/w;
							target_mc._height = 56;
							target_mc._width = bilv*w;
						}
					}
				};
				var image_mcl:MovieClipLoader = new MovieClipLoader();
				image_mcl.addListener(mclListener);
				image_mcl.loadClip(nowResourceArray[i], _root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].resourceUrl);
				//这段代码判断是否是否图片加载开始否，如果加载开始，就要判断图片的高宽之比；
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].opp = i;
				_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+i].onRollOver = function() {
					this.duplicateMovieClip("justDr", 10000);
					//新建立一个一个名为 justDr 的MovieClip;
					this._parent.justDr.opp = this.opp;
					//将i数分配给 justDr.
					this._parent.justDr.onRollOver = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.startThumb();
					};
					this._parent.justDr.onPress = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endThumbs();
						var mclListener:Object = new Object();
						mclListener.onLoadInit = function(target_mc:MovieClip) {
							var w:Number = target_mc._width;
							var h:Number = target_mc._height;
							if (w>h) {
								if (w != 68) {
									var bilv:Number = 68/w;
									target_mc._width = 68;
									target_mc._height = bilv*h;
								}
							} else if (h>w) {
								if (h != 56) {
									var bilv:Number = 56/w;
									target_mc._height = 56;
									target_mc._width = bilv*w;
								}
							}
						};
						var image_mcl:MovieClipLoader = new MovieClipLoader();
						image_mcl.addListener(mclListener);
						image_mcl.loadClip(nowThis.nowResourceArray[this.opp], this.resourceUrl);
						this.startDrag();
					};
					this._parent.justDr.onRelease = function() {
						this.stopDrag();
						var newcreateNewAndAddArr:createNewAndAddArr = new createNewAndAddArr();
						newcreateNewAndAddArr.createObjectsI(nowThis.nowResourceArray[this.opp], _root._xmouse, _root._ymouse);
						//开始将这个值显示在工作区中；
						this.removeMovieClip();
					};
					this._parent.justDr.onRollOut = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endThumbs();
						this.stopDrag();
						this.removeMovieClip();
					};
					this._parent.justDr.onReleaseOutside = function() {
						var newresourceThumb:resourceThumb = new resourceThumb(this._y, nowThis.selectProjectNow, nowThis.nowResourceArray[this.opp]);
						newresourceThumb.endThumbs();
						this.stopDrag();
						this.removeMovieClip();
					};
				};
			}
			//**第四类**//                       
		}
		getM(startN, endN);
	}
	//*不管是swf或pic或mp3都是通用的*//
	private function clearM(psT:Number, psN:Number, psTT:Number, psNN:Number):Void {
		for (var ii:Number = psT; ii<psN; ii++) {
			_root[rP[0]].nowResource.resourceListP.objectUrl["listMC"+ii].removeMovieClip();
		}
	}
	private function getM(sta:Number, enda:Number):Void {
		clearStart = sta;
		clearEnd = enda;
	}
	//*不管是swf或pic或mp3都是通用的*//
	public function nextButtonStart() {
		var nowThis:resourceList = this;
		for (var i:Number = 5; i<7; i++) {
			_root[rP[0]].nowResource.resourceListP[rP[i]].onRollOver = function() {
				this.gotoAndStop(2);
			};
			_root[rP[0]].nowResource.resourceListP[rP[i]].onRollOut = function() {
				this.gotoAndStop(1);
			};
			_root[rP[0]].nowResource.resourceListP[rP[i]].onReleaseOutside = function() {
				this.gotoAndStop(1);
			};
		}
		_root[rP[0]].nowResource.resourceListP[rP[5]].onRelease = function() {
			this.gotoAndStop(2);
			if (nowThis.nowPage>1) {
				nowThis.nowPage = nowThis.nowPage-1;
				nowThis.goPage(nowThis.nowPage);
			}
		};
		_root[rP[0]].nowResource.resourceListP[rP[6]].onRelease = function() {
			this.gotoAndStop(2);
			if (nowThis.nowPage<nowThis.totalPage) {
				nowThis.nowPage = nowThis.nowPage+1;
				nowThis.goPage(nowThis.nowPage);
			}
		};
	}
	//翻页动作开始运行；
	public function selectPP() {
		var nowThist:resourceList = this;
		for (var i:Number = 2; i<5; i++) {
			_root[rP[0]].nowResource.resourceListP[rP[i]].arrayP = i;
			_root[rP[0]].nowResource.resourceListP[rP[i]].onRollOver = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.getHelp(nowThist.rP[this.arrayP]);
				if (nowThist.selectProjectNow != nowThist.rP[this.arrayP]) {
					this.gotoAndStop(2);
				}
			};
			_root[rP[0]].nowResource.resourceListP[rP[i]].onRollOut = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.deleteTip();
				if (nowThist.selectProjectNow != nowThist.rP[this.arrayP]) {
					this.gotoAndStop(1);
				}
			};
			_root[rP[0]].nowResource.resourceListP[rP[i]].onPress = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.deleteTip();
			};
			_root[rP[0]].nowResource.resourceListP[rP[i]].onRelease = function() {
				if (nowThist.selectProjectNow != nowThist.rP[this.arrayP]) {
					nowThist.nowPage = 1;
					_root[nowThist.rP[0]].nowResource.resourceListP[nowThist.selectProjectNow].gotoAndStop(1);
					this.gotoAndStop(3);
					nowThist.selectProjectNow = nowThist.rP[this.arrayP];
					nowThist.beginResource(nowThist.selectProjectNow);
				}
			};
			_root[rP[0]].nowResource.resourceListP[rP[i]].onReleaseOutside = function() {
				if (nowThist.selectProjectNow != nowThist.rP[this.arrayP]) {
					this.gotoAndStop(1);
				}
			};
		}
	}
}
