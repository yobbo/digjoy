﻿import setToolState;
import mouseHand;
import assistant;
import basicFunction;
import helpPanelList;
class toolPanelAction extends rmcName {
	var initializeX:Number;
	var initializeY:Number;
	var panelX:Number;
	var panelY:Number;
	public var toolState:String;
	public function toolPanelAction() {
		var nowThist:toolPanelAction = this;
		_root[tP[0]][tP[1]].gotoAndStop(3);
		toolState = tP[1];
		var newSetToolState:setToolState = new setToolState(toolState);
		var newmouseHand:mouseHand = new mouseHand(toolState);
		for (var i:Number = 1; i<tP.length; i++) {
			_root[tP[0]][tP[i]].arrayP = i;
			_root[tP[0]][tP[i]].onRollOver = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.getHelp(nowThist.tP[this.arrayP]);
				if (nowThist.toolState != nowThist.tP[this.arrayP]) {
					this.gotoAndStop(2);
				}
			};
			_root[tP[0]][tP[i]].onRollOut = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.deleteTip();
				if (nowThist.toolState != nowThist.tP[this.arrayP]) {
					this.gotoAndStop(1);
				}
			};
			_root[tP[0]][tP[i]].onPress = function() {
				var newhelpPanelList:helpPanelList = new helpPanelList();
				newhelpPanelList.deleteTip();
				if (nowThist.toolState != nowThist.tP[this.arrayP]) {
					this.gotoAndStop(3);
				}
				//如果鼠标移动上去的按钮与按下去的按钮是同一个就不产生动作；                      
			};
			_root[tP[0]][tP[i]].onRelease = function() {
				var newassistant:assistant = new assistant();
				newassistant.clearreopqs();
				if (nowThist.toolState != nowThist.tP[this.arrayP]) {
					this.gotoAndStop(3);
					if ((nowThist.tP[this.arrayP] != "toolPanelOff") && (nowThist.tP[this.arrayP] != "logo")) {
						_root[nowThist.tP[0]][nowThist.toolState].gotoAndStop(1);
						//清除以前按钮状态;
						nowThist.toolState = nowThist.tP[this.arrayP];
						//确定目前工具面板状态;
						var newSetToolState:setToolState = new setToolState(nowThist.toolState);
						//设置当前动作这个变量;
						var newmouseHand:mouseHand = new mouseHand(nowThist.toolState);
						//执行不同的鼠标动作;
						if (nowThist.tP[this.arrayP] != "toolText") {
							_root.textEditMC.removeMovieClip();
						}
					} else if (nowThist.tP[this.arrayP] == "toolPanelOff") {
						_root[nowThist.tP[0]]._x = -500;
						_root[nowThist.tP[0]]._y = -500;
					} else if (nowThist.tP[this.arrayP] == "logo") {
						var newbasicFunction:basicFunction = new basicFunction();
						newbasicFunction.alert("感谢您使用聚合传媒Joy Maker工具"+newline+"聚合传媒Joy Maker软件产品为共享软件。本软件产品版权属于『聚合传媒』所有，受著作权法、国际版权公约及其它知识产权之法律及条约所保护，因此未经许可您不得利用任何方法取得或使用本软件的程序代码、文字资料、图片、影像、音乐和音效等相关数据或文档。"+newline+"1.限制"+newline+"（1）请您保留所有本软件产品拷贝上的著作权标示，"+newline+"（2）未经许可，您不得销售本软件产品。"+newline+"（3）您必须遵守所有软件产品的相关法律法规。 并且未经许可，您不得对本软件产品进行反向工程(Reverse engineer)、反向编译(Decompile)或反汇编 (Disassemble)。（注：若有相关适用法律法规禁止上述限制，则限制自动无效）"+newline+"2.终止"+newline+"（1）若您未能遵守本许可协议之条款或条件，则『聚合传媒』有权在不妨碍其他权力的情况下，终止本『许可协议』，届时您必须销毁本软件产品之所有拷贝。"+newline+"（2）若在网上或报刊杂志上发现了您公布本软件原代码，『聚合传媒』有权在未通知您的时候，永久取消您再次使用本软件的权力。"+newline+"3.不为瑕疵担保"+newline+"您因使用本『软件产品』所造成之任何损失和风险将由您独自承担。在相关法律所允许之最大范围内，『聚合传媒』及其供应商不承担任何瑕疵担保责任与条件，不伦其为明示或默示者。"+newline+"Joy Maker小组敬上");
					}
				}
				//如果鼠标移动上去的按钮与按下去的按钮是同一个就不产生动作                    
			};
			_root[tP[0]][tP[i]].onReleaseOutside = function() {
				if (nowThist.toolState != nowThist.tP[this.arrayP]) {
					this.gotoAndStop(1);
				}
				//如果鼠标移动上去的按钮与按下去的按钮是同一个就不产生动作                     
			};
		}
		//定义按钮动作；
		dragPanel();
		//开始启动拖动面板函数
	}
	private function dragPanel():Void {
		var nowThist:toolPanelAction = this;
		_root[tP[0]][tP[9]].onPress = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			this._parent.startDrag();
		};
		_root[tP[0]][tP[9]].onRelease = function() {
			this._parent.stopDrag();
		};
		_root[tP[0]][tP[9]].onReleaseOutside = function() {
			this._parent.stopDrag();
		};
	}
	private function createpxpCC():Void {
		panelX = _root[tP[0]]._x;
		panelY = _root[tP[0]]._y;
		var nowThist:toolPanelAction = this;
		_root.createEmptyMovieClip("pxpCC", 45000);
		_root.pxpCC._x = panelX;
		_root.pxpCC._y = panelY;
		_root.pxpCC.createEmptyMovieClip("containerXp", 1);
		//新建立一个空的movieClip；
		_root.pxpCC._alpha = 0;
		_root.pxpCC.containerXp.beginFill(0xFF0000, 0);
		_root.pxpCC.containerXp.lineStyle(1, 0xFF0000, 100, true, "none", "round", "round", 1);
		_root.pxpCC.containerXp.moveTo(0, 0);
		_root.pxpCC.containerXp.lineTo(_root[tP[0]]._width, 0);
		_root.pxpCC.containerXp.lineTo(_root[tP[0]]._width, _root[tP[0]]._height);
		_root.pxpCC.containerXp.lineTo(0, _root[tP[0]]._height);
		_root.pxpCC.containerXp.lineTo(0, 0);
		_root.pxpCC.containerXp.endFill();
		_root.pxpCC.onRollOut = function() {
			nowThist.deletepxpCC();
		};
		_root.pxpCC.onPress = function() {
			this._alpha = 100;
			this.startDrag();
		};
		_root.pxpCC.onRelease = function() {
			nowThist.stopDragGetXY();
		};
		_root.pxpCC.onReleaseOutside = function() {
			nowThist.stopDragGetXY();
		};
	}
	private function deletepxpCC():Void {
		_root.pxpCC.removeMovieClip();
	}
	private function stopDragGetXY():Void {
		initializeX = _root.pxpCC._x;
		initializeY = _root.pxpCC._y;
		_root.pxpCC.stopDrag();
		_root.pxpCC._visible = false;
		_root[tP[0]]._x = initializeX;
		_root[tP[0]]._y = initializeY;
	}
}
//工具面板动作产生
