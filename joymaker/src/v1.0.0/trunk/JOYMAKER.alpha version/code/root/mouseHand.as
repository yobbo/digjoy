﻿import basicSelect;
import basicScale;
import pan;
import circleR;
import createNewAndAddArr;
import assistant;
import basicFunction;
class mouseHand extends rmcName {
	private var nAc:String;
	public function mouseHand(toolSend:String) {
		_root.createEmptyMovieClip("mouseMC", 99988);
		_root.createEmptyMovieClip("SHOWmouseMC", 100000);
		nAc = toolSend;
		defaultMouse();
	}
	private function defaultMouse() {
		var nowThis:mouseHand = this;
		var newbasicFunction:basicFunction = new basicFunction();
		//*这段代码是当tool状态等于toolSelect时执行*//
		if (nAc == tP[1]) {
			var newassistant:assistant = new assistant();
			newassistant.clearreopqs();
			Mouse.hide();
			_root.SHOWmouseMC.attachMovie("SHOWmouse", "showM", 0);
			_root.mouseMC.attachMovie("mouseNoHittest", "nowA", 0);
			_root.SHOWmouseMC.showM.onMouseMove = function() {
				mx.behaviors.DepthControl.bringToFront(this);
				//放在最前面；
				if ((this.hitTest(_root.textEditMC)) || (this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]]))) {
					Mouse.show();
					_root.mouseMC.nowA._alpha = 0;
				} else {
					Mouse.hide();
					_root.mouseMC.nowA._alpha = 100;
				}
				for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
					if (this.hitTest(_root.objectInner["object"+i]["object"])) {
						_root.mouseMC.nowA.gotoAndStop(2);
						break;
					} else {
						_root.mouseMC.nowA.gotoAndStop(1);
					}
				}
				this._x = _root._xmouse;
				this._y = _root._ymouse;
				_root.mouseMC.nowA._x = _root._xmouse;
				_root.mouseMC.nowA._y = _root._ymouse;
				updateAfterEvent();
			};
			_root.SHOWmouseMC.showM.onMouseDown = function() {
				if (!((this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]])))) {
					var mc:MovieClip = null;
					for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
						if (this.hitTest(_root.objectInner["object"+i])) {
							var d:Number;
							if (mc == null) {
								d = -1;
							} else {
								d = mc.getDepth();
							}
							if (_root.objectInner["object"+i].getDepth()>d) {
								mc = _root.objectInner["object"+i];
							}
						}
					}
					var newbasicSelect = new basicSelect(mc);
				}
			};
			//*这段代码是当tool状态等于toolSelect时执行*//
		} else if (nAc == tP[2]) {
			//*这段代码是当tool状态等于toolScale时执行*//
			var newassistant:assistant = new assistant();
			newassistant.clearreopqs();
			Mouse.hide();
			_root.mouseMC.attachMovie("mouseNoHittest", "nowA", 0);
			_root.SHOWmouseMC.attachMovie("SHOWmouse", "showM", 0);
			//*鼠标变化外观*//
			_root.SHOWmouseMC.showM.onMouseMove = function() {
				mx.behaviors.DepthControl.bringToFront(this);
				if ((this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]]))) {
					Mouse.show();
					_root.mouseMC.nowA._alpha = 0;
				} else {
					Mouse.hide();
					_root.mouseMC.nowA._alpha = 100;
				}
				for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
					if (this.hitTest(_root.objectInner["object"+i]["object"])) {
						_root.mouseMC.nowA.gotoAndStop(2);
						break;
					} else {
						_root.mouseMC.nowA.gotoAndStop(1);
					}
				}
				for (var xp:Number = 0; xp<8; xp++) {
					if (this.hitTest(_root.objectInner.reopq["reopqs"+xp])) {
						_root.mouseMC.nowA.gotoAndStop(3);
						break;
					} else {
						for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
							if (this.hitTest(_root.objectInner["object"+i]["object"])) {
								_root.mouseMC.nowA.gotoAndStop(2);
								break;
							} else {
								_root.mouseMC.nowA.gotoAndStop(1);
							}
						}
					}
				}
				for (var xps:Number = 8; xps<12; xps++) {
					if (this.hitTest(_root.objectInner.reopq["reopqs"+xps])) {
						_root.mouseMC.nowA.gotoAndStop(4);
						break;
					} else {
						for (var xp:Number = 0; xp<8; xp++) {
							if (this.hitTest(_root.objectInner.reopq["reopqs"+xp])) {
								_root.mouseMC.nowA.gotoAndStop(3);
								break;
							} else {
								for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
									if (this.hitTest(_root.objectInner["object"+i]["object"])) {
										_root.mouseMC.nowA.gotoAndStop(2);
										break;
									} else {
										_root.mouseMC.nowA.gotoAndStop(1);
									}
								}
							}
						}
					}
				}
				this._x = _root._xmouse;
				this._y = _root._ymouse;
				_root.mouseMC.nowA._x = _root._xmouse;
				_root.mouseMC.nowA._y = _root._ymouse;
				updateAfterEvent();
			};
			_root.SHOWmouseMC.showM.onMouseDown = function() {
				if (!((this.hitTest(_root.objectInner.opq)) || (this.hitTest(_root.objectInner.reopq)) || (this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]])))) {
					var mc:MovieClip = null;
					for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
						if (this.hitTest(_root.objectInner["object"+i])) {
							var d:Number;
							if (mc == null) {
								d = -1;
							} else {
								d = mc.getDepth();
							}
							if (_root.objectInner["object"+i].getDepth()>d) {
								mc = _root.objectInner["object"+i];
							}
						}
					}
					var newbasicScale = new basicScale(mc);
				}
			};
			//*这段代码是当tool状态等于toolScale时执行*//
		} else if (nAc == tP[3]) {
			var newassistant:assistant = new assistant();
			newassistant.clearreopqs();
			Mouse.hide();
			_root.mouseMC.attachMovie("pan", "nowA", 0);
			_root.mouseMC.nowA.onMouseMove = function() {
				mx.behaviors.DepthControl.bringToFront(this);
				if ((this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]]))) {
					Mouse.show();
					this._alpha = 0;
				} else {
					Mouse.hide();
					this._alpha = 100;
				}
				this._x = _root._xmouse;
				this._y = _root._ymouse;
				updateAfterEvent();
			};
			_root.objectInner.opq.removeMovieClip();
			_root.mouseMC.nowA.onMouseDown = function() {
				var newpan:pan = new pan();
				newpan.startD();
			};
			_root.mouseMC.nowA.onMouseUp = function() {
				var newpan:pan = new pan();
				newpan.stopD();
			};
		} else if (nAc == tP[4]) {
		} else if (nAc == tP[5]) {
			//*选择字体的变化*//
			var newassistant:assistant = new assistant();
			newassistant.clearreopqs();
			Mouse.hide();
			_root.mouseMC.attachMovie("mouseText", "nowA", 0);
			_root.mouseMC.nowA.onMouseMove = function() {
				mx.behaviors.DepthControl.bringToFront(this);
				if ((this.hitTest(_root.textEditMC)) || (this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]]))) {
					Mouse.show();
					this._alpha = 0;
				} else {
					Mouse.hide();
					this._alpha = 100;
				}
				for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
					if (this.hitTest(_root.objectInner["object"+i]["object"])) {
						this.gotoAndStop(2);
						break;
					} else {
						this.gotoAndStop(1);
					}
				}
				this._x = _root._xmouse;
				this._y = _root._ymouse;
				updateAfterEvent();
			};
			_root.mouseMC.nowA.onMouseDown = function() {
				if (!((this.hitTest(_root.textEditMC)) || (this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]])))) {
					newbasicFunction.startDRECT();
				}
			};
			_root.mouseMC.nowA.onMouseUp = function() {
				if (!((this.hitTest(_root.textEditMC)) || (this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]])))) {
					var nowR:Array = newbasicFunction.stopDRECT();
					_root.createEmptyMovieClip("textEditMC", 20000);
					_root.textEditMC.attachMovie("textEdit", "textEdits", 1);
					_root.textEditMC._x = 300;
					_root.textEditMC._y = 200;
					_root.textEditMC.r1 = nowR[0];
					_root.textEditMC.r2 = nowR[1];
					_root.textEditMC.r3 = nowR[2];
					_root.textEditMC.r4 = nowR[3];
					var shadowNow:flash.filters.DropShadowFilter = new flash.filters.DropShadowFilter(10, 90, 0x000000, 0.8, 16, 16, 1, 3, false, false, false);
					_root.textEditMC.filters = [shadowNow];
					_root.textEditMC.textEdits.succT.onRelease = function() {
						var newcreateNewAndAddArr:createNewAndAddArr = new createNewAndAddArr();
						newcreateNewAndAddArr.createTextI(this._parent._parent.r1, this._parent._parent.r2, this._parent._parent.r3, this._parent._parent.r4, _root.textEditMC.textEdits.textTT.text);
						_root.textEditMC.removeMovieClip();
					};
					_root.textEditMC.textEdits.clearT.onRelease = function() {
						_root.textEditMC.textEdit.textTT.text = "";
					};
					_root.textEditMC.textEdits.offButton.onRelease = function() {
						_root.textEditMC.removeMovieClip();
					};
				}
			};
			//*选择字体的变化*//
		} else if (nAc == tP[6]) {
		} else if (nAc == tP[7]) {
		} else if (nAc == tP[8]) {
		} else if (nAc == tP[9]) {
		} else if (nAc == tP[10]) {
			//*这段代码是当tool状态等于circleDraw时执行*//
			var newcircleR:circleR = new circleR();
			newcircleR.setToolPp();
			Mouse.hide();
			_root.mouseMC.attachMovie("draw", "nowA", 0);
			_root.mouseMC.nowA.onMouseMove = function() {
				mx.behaviors.DepthControl.bringToFront(this);
				if ((this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]]))) {
					Mouse.show();
					this._alpha = 0;
				} else {
					Mouse.hide();
					this._alpha = 100;
				}
				this._x = _root._xmouse;
				this._y = _root._ymouse;
				updateAfterEvent();
			};
			_root.mouseMC.nowA.onMouseDown = function() {
				if (!((this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]])))) {
					if (_root.draws.begindraw == "ok") {
						newbasicFunction.startDcircle();
					}
				}
			};
			_root.mouseMC.nowA.onMouseUp = function() {
				var nowR:Array = newbasicFunction.stopDcircle();
				var newcreateNewAndAddArr:createNewAndAddArr = new createNewAndAddArr();
				if ((nowR[2]>1) && (nowR[3]>1)) {
					newcreateNewAndAddArr.createDrawCircleI(_root.draws.linewidth, _root.draws.lineColor, _root.draws.innerColor, nowR[0], nowR[1], nowR[2], nowR[3], nowR[4], nowR[5], nowR[6]);
				}
			};
			//*这段代码是当tool状态等于circleDraw时执行*//
		} else if (nAc == tP[11]) {
			//*这段代码是当tool状态等于rectangle时执行*//
			var newcircleR:circleR = new circleR();
			newcircleR.setToolPp();
			Mouse.hide();
			_root.mouseMC.attachMovie("draw", "nowA", 0);
			_root.mouseMC.nowA.onMouseMove = function() {
				mx.behaviors.DepthControl.bringToFront(this);
				if ((this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]]))) {
					Mouse.show();
					this._alpha = 0;
				} else {
					Mouse.hide();
					this._alpha = 100;
				}
				this._x = _root._xmouse;
				this._y = _root._ymouse;
				updateAfterEvent();
			};
			_root.mouseMC.nowA.onMouseDown = function() {
				if (!((this.hitTest(_root[nowThis.pP[0]])) || (this.hitTest(_root[nowThis.tP[0]])) || (this.hitTest(_root[nowThis.nP[0]])) || (this.hitTest(_root[nowThis.rP[0]])))) {
					if (_root.draws.begindraw == "ok") {
						newbasicFunction.startDRECT();
					}
				}
			};
			_root.mouseMC.nowA.onMouseUp = function() {
				var nowR:Array = newbasicFunction.stopDRECT();
				var newcreateNewAndAddArr:createNewAndAddArr = new createNewAndAddArr();
				if ((nowR[2]>1) && (nowR[3]>1)) {
					newcreateNewAndAddArr.createDrawRectI(_root.draws.linewidth, _root.draws.lineColor, _root.draws.innerColor, nowR[0], nowR[1], nowR[2], nowR[3]);
				}
			};
			//*这段代码是当tool状态等于rectangle时执行*//
		} else if (nAc == tP[12]) {
		} else if (nAc == tP[13]) {
		}
	}
	//默认鼠标动作;
}
