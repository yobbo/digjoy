﻿import toolPp;
import assistant;
import exportXML;
class basicSelect {
	private var newMc:MovieClip;
	private var stopSet:String;
	public function basicSelect(mc:MovieClip) {
		var nowThis:basicSelect = this;
		if (mc == null) {
			var newassistant:assistant = new assistant();
			newassistant.clearreopqs();
		} else {
			if (mc == _root.toolState.nowObject) {
				newMc = mc;
				_root.toolState.nowObject = mc;
				var newtoolPp:toolPp = new toolPp();
				newtoolPp.setToolPp();
			} else {
				_root.objectInner.opq.removeMovieClip();
				_root.toolState.nowObject = mc;
				newMc = mc;
				var newtoolPp:toolPp = new toolPp();
				newtoolPp.setToolPp();
				_root.objectInner.createEmptyMovieClip("opq",999999);
				_root.objectInner.opq.createEmptyMovieClip("object", 0);
				_root.objectInner.opq.object.beginFill(0xFF0000, 0);
				_root.objectInner.opq.object.lineStyle(1, 0x00CCFF, 100, true, "none", "round", "round", 1);
				_root.objectInner.opq.object.moveTo(0, 0);
				_root.objectInner.opq.object.lineTo(newMc.object._width, 0);
				_root.objectInner.opq.object.lineTo(newMc.object._width, newMc.object._height);
				_root.objectInner.opq.object.lineTo(0, newMc.object._height);
				_root.objectInner.opq.object.lineTo(0, 0);
				_root.objectInner.opq.object.endFill();
				if (_root["page"+_root.thisPage.nowPage][newMc.id][0] != "txt") {
					_root.objectInner.opq.object._x = -_root.objectInner.opq.object._width/2;
					_root.objectInner.opq.object._y = -_root.objectInner.opq.object._height/2;
				}
				_root.objectInner.opq._x = newMc._x
				_root.objectInner.opq._y = newMc._y
				_root.objectInner.opq._rotation = newMc._rotation;
				_root.objectInner.opq.onPress = function() {
					nowThis.stopSet = "true";
					this.startDrag();
					nowThis.thisMCXY(nowThis.newMc);
				};
				_root.objectInner.opq.onRelease = function() {
					nowThis.stopSet = "false";
					this.stopDrag();
					nowThis.thisMCXX(nowThis.newMc);
				};
				_root.objectInner.opq.onReleaseOutside = function() {
					this.stopDrag();
					nowThis.stopSet = "false";
					nowThis.thisMCXX(nowThis.newMc);
				};
				var idss = setInterval(function () {
					if ((_root.toolState.nowObject == null) || (_root.toolState.nowObject != nowThis.newMc)) {
						clearInterval(idss);
					}
					if (Key.isDown(Key.LEFT)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						nowThis.newMc._x = nowThis.newMc._x-1;
						_root.objectInner.opq._x = _root.objectInner.opq._x-1;
						_root["page"+_root.thisPage.nowPage][nowThis.newMc.id][3] = nowThis.newMc._x;
					} else if (Key.isDown(Key.DELETEKEY)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						delete _root["page"+_root.thisPage.nowPage][nowThis.newMc.id];
						nowThis.newMc.removeMovieClip();
						var newassistant:assistant = new assistant();
						newassistant.clearreopqs();
					} else if (Key.isDown(Key.UP)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						nowThis.newMc._y = nowThis.newMc._y-1;
						_root.objectInner.opq._y = _root.objectInner.opq._y-1;
						_root["page"+_root.thisPage.nowPage][nowThis.newMc.id][4] = nowThis.newMc._y;
					} else if (Key.isDown(Key.RIGHT)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						nowThis.newMc._x = nowThis.newMc._x+1;
						_root.objectInner.opq._x = _root.objectInner.opq._x+1;
						_root["page"+_root.thisPage.nowPage][nowThis.newMc.id][3] = nowThis.newMc._x;
					} else if (Key.isDown(Key.DOWN)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						nowThis.newMc._y = nowThis.newMc._y+1;
						_root.objectInner.opq._y = _root.objectInner.opq._y+1;
						_root["page"+_root.thisPage.nowPage][nowThis.newMc.id][4] = nowThis.newMc._y;
					} else if (Key.isDown(189)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						var setMC:MovieClip = nowThis.newMc;
						var newMC:MovieClip = _root.objectInner.getInstanceAtDepth(setMC.getDepth()-1);
						setMC.swapDepths(newMC);
						_root["page"+_root.thisPage.nowPage][newMC.id][6] = newMC.getDepth();
						_root["page"+_root.thisPage.nowPage][setMC.id][6] = setMC.getDepth();
					} else if (Key.isDown(187)) {
						nowThis.newMc = null;
						nowThis.newMc = _root.toolState.nowObject;
						var setMC:MovieClip = nowThis.newMc;
						var newMC:MovieClip = _root.objectInner.getInstanceAtDepth(setMC.getDepth()+1);
						setMC.swapDepths(newMC);
						_root["page"+_root.thisPage.nowPage][newMC.id][6] = newMC.getDepth();
						_root["page"+_root.thisPage.nowPage][setMC.id][6] = setMC.getDepth();
					}
				}, 50);
			}
		}
	}
	private function thisMCXY(getMC:MovieClip) {
		var nowThis:basicSelect = this;
		var selectp = setInterval(function () {
			if ((_root.toolState.nowObject == null) || (_root.toolState.nowObject != getMC) || (nowThis.stopSet == "false")) {
				clearInterval(selectp);
			}
			getMC._x = _root.objectInner.opq._x
			getMC._y = _root.objectInner.opq._y
		}, 1);
	}
	private function thisMCXX(getMC:MovieClip) {
		_root.CHANGED = "TRUE";
		_root["page"+_root.thisPage.nowPage][getMC.id][3] = getMC._x;
		_root["page"+_root.thisPage.nowPage][getMC.id][4] = getMC._y;
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
	}
}
