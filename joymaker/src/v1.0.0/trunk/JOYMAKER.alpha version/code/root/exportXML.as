﻿import arrays;
class exportXML {
	private var max:Array;
	public function exportXML() {
		var newarrays:arrays = new arrays();
		max = newarrays.getMaxPageArr();
	}
	public function exportFileListXML():String {
		var fileListXML:String;
		var newFileList:String;
		newFileList = _root.loadStencil[0]+",";
		if (_root.resource.backgroundUrl != "") {
			newFileList += _root.resource.backgroundUrl+",";
		}
		for (var i:Number = 0; i<max.length; i++) {
			for (var t:Number = 0; t<max[i].length; t++) {
				if ((max[i][t][0] == "load") || (max[i][t][0] == "music")) {
					var url:String = max[i][t][5];
					if (fileListXML != null) {
						fileListXML = fileListXML+url+",";
					} else if (fileListXML == null) {
						fileListXML = url+",";
					}
				}
			}
		}
		if (fileListXML != null) {
			var tempFileListArray:Array = fileListXML.split(",");
			tempFileListArray.pop();
			for (var firstN:Number = 0; firstN<tempFileListArray.length; firstN++) {
				var nowString:String = tempFileListArray[firstN];
				for (var secondN:Number = firstN+1; secondN<tempFileListArray.length; secondN++) {
					var secString:String = tempFileListArray[secondN];
					if (nowString == secString) {
						delete tempFileListArray[secondN];
					}
				}
			}
			for (var ppp:Number = 0; ppp<tempFileListArray.length; ppp++) {
				if (tempFileListArray[ppp] != null) {
					newFileList += tempFileListArray[ppp]+",";
				}
			}
		}
		fileListXML = newFileList;
		return fileListXML;
	}
	public function exportRunXML():String {
		var runXML:String;
		runXML = "<?xml version=\"1.0\" encoding=\"gb2312\"?>";
		var nowDate:Date = new Date();
		runXML = runXML+"<content>";
		if (_root.resource.backgroundUrl != "") {
			var getCharts:String;
			var startCharts:Number;
			startCharts = _root.resource.backgroundUrl.lastIndexOf("\\");
			getCharts = _root.resource.backgroundUrl.slice(startCharts+1, _root.resource.backgroundUrl.length);
		}
		runXML = runXML+"<global bg=\""+getCharts+"\" time=\""+nowDate.getFullYear()+"-"+(nowDate.getMonth()+1)+"-"+nowDate.getDate()+"\" project=\""+_root.loadStencil[0]+"\"/>";
		for (var i:Number = 0; i<max.length; i++) {
			runXML = runXML+"<page>";
			for (var t:Number = 0; t<max[i].length; t++) {
				if ((max[i][t][0] == "load") || (max[i][t][0] == "music") || (max[i][t][0] == "draw_circle") || (max[i][t][0] == "draw_rect")) {
					runXML = runXML+"<obj type=\""+max[i][t][0]+"\" ";
					runXML = runXML+"width=\""+max[i][t][1]+"\" ";
					runXML = runXML+"height=\""+max[i][t][2]+"\" ";
					runXML = runXML+"x=\""+(max[i][t][3]-_root.workArea._x+85)+"\" ";
					runXML = runXML+"y=\""+(max[i][t][4]-_root.workArea._y+5)+"\" ";
					var getChart:String;
					var startChart:Number;
					startChart = max[i][t][5].lastIndexOf("\\");
					getChart = max[i][t][5].slice(startChart+1, max[i][t][5].length);
					runXML = runXML+"url=\""+getChart+"\" ";
					runXML = runXML+"depth=\""+max[i][t][6]+"\" ";
					runXML = runXML+"rotation=\""+max[i][t][7]+"\" ";
					runXML = runXML+"alpha=\""+max[i][t][8]+"\" ";
					runXML = runXML+"xscale=\""+max[i][t][9]+"\" ";
					runXML = runXML+"yscale=\""+max[i][t][10]+"\" ";
					runXML = runXML+"linecolor=\""+max[i][t][11]+"\" ";
					runXML = runXML+"linecu=\""+max[i][t][12]+"\" ";
					runXML = runXML+"fillcolor=\""+max[i][t][13]+"\" ";
					runXML = runXML+"circle_r=\""+max[i][t][14]+"\"/>";
				}
				if (max[i][t][0] == "txt") {
					runXML = runXML+"<obj type=\""+max[i][t][0]+"\" ";
					runXML = runXML+"width=\""+max[i][t][1]+"\" ";
					runXML = runXML+"height=\""+max[i][t][2]+"\" ";
					runXML = runXML+"x=\""+(max[i][t][3]-_root.workArea._x)+"\" ";
					runXML = runXML+"y=\""+(max[i][t][4]-_root.workArea._y)+"\" ";
					runXML = runXML+"url=\""+0+"\" ";
					runXML = runXML+"depth=\""+max[i][t][6]+"\" ";
					runXML = runXML+"rotation=\""+max[i][t][7]+"\" ";
					runXML = runXML+"alpha=\""+max[i][t][8]+"\" ";
					runXML = runXML+"xscale=\""+max[i][t][9]+"\" ";
					runXML = runXML+"yscale=\""+max[i][t][10]+"\" ";
					runXML = runXML+"linecolor=\""+max[i][t][11]+"\" ";
					runXML = runXML+"linecu=\""+max[i][t][12]+"\" ";
					runXML = runXML+"fillcolor=\""+max[i][t][13]+"\" ";
					runXML = runXML+"circle_r=\""+max[i][t][14]+"\">";
					runXML = runXML+"<![CDATA["+max[i][t][5]+"]]></obj>";
				}
			}
			runXML = runXML+"</page>";
		}
		runXML = runXML+"</content>";
		return runXML;
	}
	public function exportProjectRunXML():String {
		var runXMLproject:String;
		runXMLproject = "<?xml version=\"1.0\" encoding=\"gb2312\"?>";
		var nowDate:Date = new Date();
		runXMLproject = runXMLproject+"<content>";
		runXMLproject = runXMLproject+"<global projectkinds=\""+_root.sStencil.select+"\" bg=\""+_root.resource.backgroundUrl+"\" time=\""+nowDate.getFullYear()+"-"+(nowDate.getMonth()+1)+"-"+nowDate.getDate()+"\" project=\""+_root.loadStencil[0]+"\"/>";
		for (var i:Number = 0; i<max.length; i++) {
			runXMLproject = runXMLproject+"<page>";
			for (var t:Number = 0; t<max[i].length; t++) {
				if ((max[i][t][0] == "load") || (max[i][t][0] == "music") || (max[i][t][0] == "draw_circle") || (max[i][t][0] == "draw_rect")) {
					runXMLproject = runXMLproject+"<obj type=\""+max[i][t][0]+"\" ";
					runXMLproject = runXMLproject+"width=\""+max[i][t][1]+"\" ";
					runXMLproject = runXMLproject+"height=\""+max[i][t][2]+"\" ";
					runXMLproject = runXMLproject+"x=\""+max[i][t][3]+"\" ";
					runXMLproject = runXMLproject+"y=\""+max[i][t][4]+"\" ";
					runXMLproject = runXMLproject+"url=\""+max[i][t][5]+"\" ";
					runXMLproject = runXMLproject+"depth=\""+max[i][t][6]+"\" ";
					runXMLproject = runXMLproject+"rotation=\""+max[i][t][7]+"\" ";
					runXMLproject = runXMLproject+"alpha=\""+max[i][t][8]+"\" ";
					runXMLproject = runXMLproject+"xscale=\""+max[i][t][9]+"\" ";
					runXMLproject = runXMLproject+"yscale=\""+max[i][t][10]+"\" ";
					runXMLproject = runXMLproject+"linecolor=\""+max[i][t][11]+"\" ";
					runXMLproject = runXMLproject+"linecu=\""+max[i][t][12]+"\" ";
					runXMLproject = runXMLproject+"fillcolor=\""+max[i][t][13]+"\" ";
					runXMLproject = runXMLproject+"circle_r=\""+max[i][t][14]+"\"/>";
				}
				if (max[i][t][0] == "txt") {
					runXMLproject = runXMLproject+"<obj type=\""+max[i][t][0]+"\" ";
					runXMLproject = runXMLproject+"width=\""+max[i][t][1]+"\" ";
					runXMLproject = runXMLproject+"height=\""+max[i][t][2]+"\" ";
					runXMLproject = runXMLproject+"x=\""+max[i][t][3]+"\" ";
					runXMLproject = runXMLproject+"y=\""+max[i][t][4]+"\" ";
					runXMLproject = runXMLproject+"url=\""+0+"\" ";
					runXMLproject = runXMLproject+"depth=\""+max[i][t][6]+"\" ";
					runXMLproject = runXMLproject+"rotation=\""+max[i][t][7]+"\" ";
					runXMLproject = runXMLproject+"alpha=\""+max[i][t][8]+"\" ";
					runXMLproject = runXMLproject+"xscale=\""+max[i][t][9]+"\" ";
					runXMLproject = runXMLproject+"yscale=\""+max[i][t][10]+"\" ";
					runXMLproject = runXMLproject+"linecolor=\""+max[i][t][11]+"\" ";
					runXMLproject = runXMLproject+"linecu=\""+max[i][t][12]+"\" ";
					runXMLproject = runXMLproject+"fillcolor=\""+max[i][t][13]+"\" ";
					runXMLproject = runXMLproject+"circle_r=\""+max[i][t][14]+"\">";
					runXMLproject = runXMLproject+"<![CDATA["+max[i][t][5]+"]]></obj>";
				}
			}
			runXMLproject = runXMLproject+"</page>";
		}
		runXMLproject = runXMLproject+"</content>";
		return runXMLproject;
	}
}
