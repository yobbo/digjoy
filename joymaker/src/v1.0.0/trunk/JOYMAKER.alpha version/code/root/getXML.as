﻿class getXML {
	public function getXML() {
		if (_root.OPENFILE == "TRUE") {
			splitXML();
		}
	}
	private function splitXML() {
		_root.OPENFILE="FALSE";
		var openXML:XML = new XML(_root.OPENPROJECT_STRING);
		_root.aa1.text += _root.OPENPROJECT_STRING;
		var golbal = "/content/global";
		var globalNodes:Array = mx.xpath.XPathAPI.selectNodeList(openXML.firstChild, golbal);
		for (var ii:Number = 0; ii<globalNodes.length; ii++) {
			_root.sStencil.select = globalNodes[ii].attributes["projectkinds"];
			_root.resource.backgroundUrl = globalNodes[ii].attributes["bg"];
			_root.loadStencil[0] = globalNodes[ii].attributes["project"];
		}
		var stylePath = "/content/page";
		var styleNodes:Array = mx.xpath.XPathAPI.selectNodeList(openXML.firstChild, stylePath);
		for (var i:Number = 0; i<styleNodes.length; i++) {
			_root["page"+i] = new Array();
			for (var t:Number = 0; t<styleNodes[i].childNodes.length; t++) {
				var type:String = styleNodes[i].childNodes[t].attributes["type"];
				var widths:Number = styleNodes[i].childNodes[t].attributes["width"];
				var heights:Number = styleNodes[i].childNodes[t].attributes["height"];
				var xs:Number = styleNodes[i].childNodes[t].attributes["x"];
				var ys:Number = styleNodes[i].childNodes[t].attributes["y"];
				if (type != "txt") {
					var urls:String = styleNodes[i].childNodes[t].attributes["url"];
				} else {
					var urls:String = styleNodes[i].childNodes[t].firstChild.nodeValue;
				}
				var depths:Number = styleNodes[i].childNodes[t].attributes["depth"];
				var rotations:Number = styleNodes[i].childNodes[t].attributes["rotation"];
				var alphas:Number = styleNodes[i].childNodes[t].attributes["alpha"];
				var xscales:Number = styleNodes[i].childNodes[t].attributes["xscale"];
				var yscales:Number = styleNodes[i].childNodes[t].attributes["yscale"];
				var linecolors:Number = styleNodes[i].childNodes[t].attributes["linecolor"];
				var linecus:Number = styleNodes[i].childNodes[t].attributes["linecu"];
				var fillcolors:Number = styleNodes[i].childNodes[t].attributes["fillcolor"];
				var circle_rs:Number = styleNodes[i].childNodes[t].attributes["circle_r"];
				_root["page"+[i]][t] = [type, widths, heights, xs, ys, urls, depths, rotations, alphas, xscales, yscales, linecolors, linecus, fillcolors, circle_rs];
			}
			_root["maxPageArr"].push(_root["page"+i]);
		}
		_root.aa2.text = _root["maxPageArr"].length;
	}
}
