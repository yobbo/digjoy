﻿import arrays;
import basicFunction;
import resourceThumb;
import exportXML;
class listObject {
	private var testtext:String;
	private var nowid:Number;
	private var kinds:String;
	private var xwidth:Number;
	private var xheight:Number;
	private var xstation:Number;
	private var ystation:Number;
	private var url:String;
	private var depth:Number;
	private var rotation:Number;
	private var alpha:Number;
	private var xscale:Number;
	private var yscale:Number;
	private var linecolor:String;
	private var linecu:Number;
	private var fillcolor:String;
	private var circleR:Number;
	public function listObject() {
	}
	public function listObjectClear():Void {
		for (var i:Number = 0; i<1000; i++) {
			_root.objectInner["object"+i].removeMovieClip();
		}
	}
	public function clearPageArray():Void {
		for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
			delete _root["page"+_root.thisPage.nowPage][i];
		}
	}
	public function listObjectThis():Void {
		var nowThis:listObject = this;
		for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
			nowid = i;
			kinds = _root["page"+_root.thisPage.nowPage][i][0];
			xwidth = _root["page"+_root.thisPage.nowPage][i][1];
			xheight = _root["page"+_root.thisPage.nowPage][i][2];
			xstation = _root["page"+_root.thisPage.nowPage][i][3];
			ystation = _root["page"+_root.thisPage.nowPage][i][4];
			url = _root["page"+_root.thisPage.nowPage][i][5];
			depth = _root["page"+_root.thisPage.nowPage][i][6];
			rotation = _root["page"+_root.thisPage.nowPage][i][7];
			alpha = _root["page"+_root.thisPage.nowPage][i][8];
			xscale = _root["page"+_root.thisPage.nowPage][i][9];
			yscale = _root["page"+_root.thisPage.nowPage][i][10];
			linecolor = _root["page"+_root.thisPage.nowPage][i][11];
			linecu = _root["page"+_root.thisPage.nowPage][i][12];
			fillcolor = _root["page"+_root.thisPage.nowPage][i][13];
			circleR = _root["page"+_root.thisPage.nowPage][i][14];
			//*music类型重建*//
			if (kinds == "music") {
				_root.objectInner.createEmptyMovieClip("object"+i, depth);
				_root.objectInner["object"+i].id = i;
				_root.objectInner["object"+i].createEmptyMovieClip("object", 0);
				_root.objectInner["object"+i].createEmptyMovieClip("playb", 1);
				_root.objectInner["object"+i].createEmptyMovieClip("stopb", 2);
				_root.objectInner["object"+i].musicUrl = url;
				_root.objectInner["object"+i]["object"].attachMovie("musicLogo", "musicLogo", 0);
				_root.objectInner["object"+i]["playb"].attachMovie("buttonPlay", "mplay", 0);
				_root.objectInner["object"+i]["stopb"].attachMovie("buttonStop", "mstop", 0);
				_root.objectInner["object"+i]._x = xstation;
				_root.objectInner["object"+i]._y = ystation;
				_root.objectInner["object"+i]["object"]._x = -_root.objectInner["object"+i]["object"]._width/2;
				_root.objectInner["object"+i]["object"]._y = -_root.objectInner["object"+i]["object"]._height/2;
				_root.objectInner["object"+i]["playb"]._x = -_root.objectInner["object"+i]["object"]._width/2+_root.objectInner["object"+i]["playb"]._width;
				_root.objectInner["object"+i]["playb"]._y = -_root.objectInner["object"+i]["object"]._height/2+_root.objectInner["object"+i]["object"]._height;
				_root.objectInner["object"+i]["playb"]._rotation = 90;
				_root.objectInner["object"+i]["playb"].onRollOver = function() {
					var newbasicFunction:basicFunction = new basicFunction();
					newbasicFunction.setMouseD();
					this.mplay.gotoAndStop(2);
				};
				_root.objectInner["object"+i]["playb"].onRollOut = function() {
					var newbasicFunction:basicFunction = new basicFunction();
					newbasicFunction.getMouseD();
					this.mplay.gotoAndStop(1);
				};
				_root.objectInner["object"+i]["playb"].onRelease = function() {
					var newresourceThumb:resourceThumb = new resourceThumb(0, "rImportM", this._parent.musicUrl);
					newresourceThumb.beginShow();
					this.mplay.gotoAndStop(1);
				};
				_root.objectInner["object"+i]["playb"].onReleaseOutside = function() {
					var newbasicFunction:basicFunction = new basicFunction();
					newbasicFunction.getMouseD();
					this.mplay.gotoAndStop(1);
				};
				_root.objectInner["object"+i]["stopb"]._x = _root.objectInner["object"+i]["playb"]._x;
				_root.objectInner["object"+i]["stopb"]._y = _root.objectInner["object"+i]["playb"]._y;
				_root.objectInner["object"+i]["stopb"].onRollOver = function() {
					var newbasicFunction:basicFunction = new basicFunction();
					newbasicFunction.setMouseD();
					this.mstop.gotoAndStop(2);
				};
				_root.objectInner["object"+i]["stopb"].onRollOut = function() {
					var newbasicFunction:basicFunction = new basicFunction();
					newbasicFunction.getMouseD();
					this.mstop.gotoAndStop(1);
				};
				_root.objectInner["object"+i]["stopb"].onRelease = function() {
					var newresourceThumb:resourceThumb = new resourceThumb(0, "rImportM", this._parent.musicUrl);
					newresourceThumb.endMusic();
					this.mstop.gotoAndStop(1);
				};
				_root.objectInner["object"+i]["stopb"].onReleaseOutside = function() {
					var newbasicFunction:basicFunction = new basicFunction();
					newbasicFunction.getMouseD();
					this.mstop.gotoAndStop(1);
				};
				var getChart:String;
				var startChart:Number;
				startChart = url.lastIndexOf("\\");
				getChart = url.slice(startChart+1, url.length);
				_root.objectInner["object"+i]["object"]["musicLogo"].musicName.text = getChart;
			}
			//*music类型重建*//                    
			//*draw_rectangle类型重建*//
			if (kinds == "draw_rect") {
				_root.objectInner.createEmptyMovieClip("object"+i, depth);
				_root.objectInner["object"+i].id = i;
				_root.objectInner["object"+i].createEmptyMovieClip("object", 0);
				_root.objectInner["object"+i]._x = xstation;
				_root.objectInner["object"+i]._y = ystation;
				_root.objectInner["object"+i]._rotation = rotation;
				_root.objectInner["object"+i]["object"].beginFill(fillcolor, 100);
				_root.objectInner["object"+i]["object"].lineStyle(linecu, linecolor, 100, true, "none", "round", "round", 1);
				_root.objectInner["object"+i]["object"].moveTo(0, 0);
				_root.objectInner["object"+i]["object"].lineTo(xwidth, 0);
				_root.objectInner["object"+i]["object"].lineTo(xwidth, xheight);
				_root.objectInner["object"+i]["object"].lineTo(0, xheight);
				_root.objectInner["object"+i]["object"].lineTo(0, 0);
				_root.objectInner["object"+i]["object"].endFill();
				_root.objectInner["object"+i]["object"]._xscale = xscale;
				_root.objectInner["object"+i]["object"]._yscale = yscale;
				if (xscale>0) {
					_root.objectInner["object"+i]["object"]._x = -_root.objectInner["object"+i]["object"]._width/2;
				} else if (xscale<0) {
					_root.objectInner["object"+i]["object"]._x = _root.objectInner["object"+i]["object"]._width/2;
				}
				if (yscale>0) {
					_root.objectInner["object"+i]["object"]._y = -_root.objectInner["object"+i]["object"]._height/2;
				} else if (yscale<0) {
					_root.objectInner["object"+i]["object"]._y = _root.objectInner["object"+i]["object"]._height/2;
				}
				_root.objectInner["object"+i]._alpha = alpha;
			}
			//*draw_rectangle类型重建*//                        
			//*draw_circle类型重建*//
			if (kinds == "draw_circle") {
				_root.objectInner.createEmptyMovieClip("object"+i, depth);
				_root.objectInner["object"+i].id = i;
				_root.objectInner["object"+i].createEmptyMovieClip("object", 0);
				var scale:Number = Math.cos(Math.PI/8);
				_root.objectInner["object"+i]["object"].beginFill(fillcolor, 100);
				_root.objectInner["object"+i]["object"].lineStyle(linecu, linecolor, 100, true, "none", "round", "round", 1);
				_root.objectInner["object"+i]["object"].moveTo(circleR, 0);
				for (var ii:Number = Math.PI/4; ii<=Math.PI*2; ii += Math.PI/4) {
					var cx = Math.cos(ii-Math.PI/8)*circleR/scale;
					var cy = Math.sin(ii-Math.PI/8)*circleR/scale;
					var ax = Math.cos(ii)*circleR;
					var ay = Math.sin(ii)*circleR;
					_root.objectInner["object"+i]["object"].curveTo(cx, cy, ax, ay);
				}
				_root.objectInner["object"+nowid]["object"].endFill();
				_root.objectInner["object"+nowid]["object"]._xscale = xscale;
				_root.objectInner["object"+nowid]["object"]._yscale = yscale;
				_root.objectInner["object"+i]._x = xstation;
				_root.objectInner["object"+i]._y = ystation;
				_root.objectInner["object"+i]._alpha = alpha;
				_root.objectInner["object"+i]._rotation = rotation;
			}
			//*draw_circle类型重建*//                      
			//*load类型重建*//
			if (kinds == "load") {
				_root.objectInner.createEmptyMovieClip("object"+i, depth);
				_root.objectInner["object"+i].id = i;
				_root.objectInner["object"+i].sxscale = xscale;
				_root.objectInner["object"+i].syscale = yscale;
				_root.objectInner["object"+i].createEmptyMovieClip("object", 0);
				_root.objectInner["object"+i]["object"].loadMovie(url);
				_root.objectInner["object"+i]._x = xstation;
				_root.objectInner["object"+i]._y = ystation;
				_root.objectInner["object"+i]._rotation = rotation;
				_root.objectInner["object"+i].onEnterFrame = function() {
					this["object"]._xscale = this.sxscale;
					this["object"]._yscale = this.syscale;
					if (this.sxscale>0) {
						this["object"]._x = -this["object"]._width/2;
					} else if (this.sxscale<0) {
						this["object"]._x = this["object"]._width/2;
					}
					if (this.syscale>0) {
						this["object"]._y = -this["object"]._height/2;
					} else if (this.syscale<0) {
						this["object"]._y = this["object"]._height/2;
					}
				};
				_root.objectInner["object"+i]._alpha = alpha;
			}
			//*load类型重建*//                          
			//*text类型重建*//
			if (kinds == "txt") {
				_root.objectInner.createEmptyMovieClip("object"+i, depth);
				_root.objectInner["object"+i].id = i;
				//给每个对象加个标签；
				_root.objectInner["object"+i].createEmptyMovieClip("object", 0);
				_root.objectInner["object"+i].createEmptyMovieClip("scrollb", 1);
				_root.objectInner["object"+i].createEmptyMovieClip("bg", 2);
				_root.objectInner["object"+i].createEmptyMovieClip("scale", 3);
				_root.objectInner["object"+i].createEmptyMovieClip("textedit", 4);
				_root.objectInner["object"+i].texts = url;
				_root.objectInner["object"+i]._x = xstation;
				_root.objectInner["object"+i]._y = ystation;
				_root.objectInner["object"+i]["object"].createTextField("my_sb", 0, 0, 0, xwidth, xheight);
				_root.objectInner["object"+i]["object"].my_sb.html = true;
				_root.objectInner["object"+i]["object"].my_sb.multiline = true;
				_root.objectInner["object"+i]["object"].my_sb.wordWrap = true;
				_root.objectInner["object"+i]["object"].my_sb.htmlText = url;
				_root.objectInner["object"+i]["scrollb"].createClassObject(mx.controls.UIScrollBar, "my_sc", 0);
				_root.objectInner["object"+i]["scrollb"].my_sc.setScrollTarget(_root.objectInner["object"+i]["object"].my_sb);
				_root.objectInner["object"+i]["scrollb"].my_sc.setSize(16, _root.objectInner["object"+i]["object"].my_sb._height);
				_root.objectInner["object"+i]["scrollb"].my_sc.move(_root.objectInner["object"+i]["object"].my_sb._x+_root.objectInner["object"+i]["object"].my_sb._width, _root.objectInner["object"+i]["object"].my_sb._y);
				if (_root.objectInner["object"+i]["object"].my_sb.textHeight>_root.objectInner["object"+i]["object"].my_sb._height) {
					_root.objectInner["object"+i]["scrollb"].my_sc._alpha = 100;
				} else {
					_root.objectInner["object"+i]["scrollb"].my_sc._alpha = 0;
				}
				_root.objectInner["object"+i]["bg"].beginFill(0xFF0000, 0);
				_root.objectInner["object"+i]["bg"].lineStyle(1, 0x00CCFF, 100, true, "none", "round", "round", 1);
				_root.objectInner["object"+i]["bg"].moveTo(0, 0);
				_root.objectInner["object"+i]["bg"].lineTo(_root.objectInner["object"+i]["object"].my_sb.width, 0);
				_root.objectInner["object"+i]["bg"].lineTo(_root.objectInner["object"+i]["object"].my_sb.width, _root.objectInner["object"+i]["object"].my_sb.height);
				_root.objectInner["object"+i]["bg"].lineTo(0, _root.objectInner["object"+i]["object"].my_sb.height);
				_root.objectInner["object"+i]["bg"].lineTo(0, 0);
				_root.objectInner["object"+i]["bg"].endFill();
				_root.objectInner["object"+i]["scale"].attachMovie("toolScale", "toolScaleT", 0);
				_root.objectInner["object"+i]["scale"]._x = 0;
				_root.objectInner["object"+i]["scale"]._y = _root.objectInner["object"+i]["object"].my_sb.height;
				_root.objectInner["object"+i]["scale"].onRollOver = function() {
					if (_root.toolState.nowTool == "toolSelect") {
						var newbasicFunction:basicFunction = new basicFunction();
						newbasicFunction.setMouseD();
					}
				};
				_root.objectInner["object"+i]["scale"].onRollOut = function() {
					if (_root.toolState.nowTool == "toolSelect") {
						var newbasicFunction:basicFunction = new basicFunction();
						newbasicFunction.getMouseD();
					}
				};
				_root.objectInner["object"+i]["scale"].onPress = function() {
					if (_root.toolState.nowTool == "toolSelect") {
						_root.objectInner["object"+this._parent.id]["bg"].nowX = _root._xmouse;
						_root.objectInner["object"+this._parent.id]["bg"].nowY = _root._ymouse;
						_root.objectInner["object"+this._parent.id]["bg"].nowSxscale = _root.objectInner["object"+this._parent.id]["bg"]._xscale;
						_root.objectInner["object"+this._parent.id]["bg"].nowSyscale = _root.objectInner["object"+this._parent.id]["bg"]._yscale;
						_root.objectInner["object"+this._parent.id]["bg"].onEnterFrame = function() {
							this._xscale = _root._xmouse-this.nowX+this.nowSxscale;
							this._yscale = _root._ymouse-this.nowY+this.nowSyscale;
							_root.objectInner.opq.object._width = this._width;
							_root.objectInner.opq.object._height = this._height;
							_root.objectInner["object"+this._parent.id]["object"].my_sb._width = this._width;
							_root.objectInner["object"+this._parent.id]["object"].my_sb._height = this._height;
							_root.objectInner["object"+this._parent.id]["scrollb"].my_sc.setSize(16, _root.objectInner["object"+this._parent.id]["object"].my_sb._height);
							_root.objectInner["object"+this._parent.id]["scrollb"].my_sc.move(this._x+this._width, this._y);
							if (_root.objectInner["object"+this._parent.id]["object"].my_sb.textHeight>_root.objectInner["object"+this._parent.id]["object"].my_sb._height) {
								_root.objectInner["object"+this._parent.id]["scrollb"].my_sc._alpha = 100;
							} else {
								_root.objectInner["object"+this._parent.id]["scrollb"].my_sc._alpha = 0;
							}
							_root.objectInner["object"+this._parent.id]["scale"]._x = 0;
							_root.objectInner["object"+this._parent.id]["scale"]._y = _root.objectInner["object"+this._parent.id]["object"].my_sb.height;
							_root.objectInner["object"+this._parent.id]["textedit"]._x = _root.objectInner["object"+this._parent.id]["scale"]._width;
							_root.objectInner["object"+this._parent.id]["textedit"]._y = _root.objectInner["object"+this._parent.id]["scale"]._y;
							_root["page"+_root.thisPage.nowPage][this._parent.id][1] = this._width;
							_root["page"+_root.thisPage.nowPage][this._parent.id][2] = this._height;
						};
					}
				};
				_root.objectInner["object"+i]["scale"].onRelease = function() {
					delete _root.objectInner["object"+this._parent.id]["bg"].onEnterFrame;
					var newbasicFunction:basicFunction = new basicFunction();
					newbasicFunction.getMouseD();
				};
				_root.objectInner["object"+i]["scale"].onReleaseOutside = function() {
					delete _root.objectInner["object"+this._parent.id]["bg"].onEnterFrame;
					var newbasicFunction:basicFunction = new basicFunction();
					newbasicFunction.getMouseD();
				};
				_root.objectInner["object"+i]["textedit"].attachMovie("toolInputText", "toolInputText", 0);
				_root.objectInner["object"+i]["textedit"]._x = _root.objectInner["object"+i]["scale"]._width;
				_root.objectInner["object"+i]["textedit"]._y = _root.objectInner["object"+i]["scale"]._y;
				_root.objectInner["object"+i]["textedit"].onRollOver = function() {
					if (_root.toolState.nowTool == "toolSelect") {
						var newbasicFunction:basicFunction = new basicFunction();
						newbasicFunction.setMouseD();
					}
				};
				_root.objectInner["object"+i]["textedit"].onRollOut = function() {
					if (_root.toolState.nowTool == "toolSelect") {
						var newbasicFunction:basicFunction = new basicFunction();
						newbasicFunction.getMouseD();
					}
				};
				_root.objectInner["object"+i]["textedit"].onRelease = function() {
					if (_root.toolState.nowTool == "toolSelect") {
						var newbasicFunction:basicFunction = new basicFunction();
						newbasicFunction.getMouseD();
						_root.createEmptyMovieClip("textEditMC", 20000);
						_root.textEditMC.attachMovie("textEdit", "textEdits", 1);
						_root.textEditMC._x = 300;
						_root.textEditMC._y = 200;
						var shadowNow:flash.filters.DropShadowFilter = new flash.filters.DropShadowFilter(10, 90, 0x000000, 0.8, 16, 16, 1, 3, false, false, false);
						_root.textEditMC.filters = [shadowNow];
						nowThis.testtext = _root["page"+_root.thisPage.nowPage][nowThis.nowid][5];
						var id = setInterval(function () {
							_root.textEditMC.textEdits.textTT.text = nowThis.testtext;
							clearInterval(id);
						}, 1000);
						//建立textEditMC.textEdit这样的文字编辑窗口;
						_root.textEditMC.textEdits.succT.onRelease = function() {
							_root.objectInner["object"+nowThis.nowid]["object"].my_sb.htmlText = _root.textEditMC.textEdits.textTT.text;
							_root["page"+_root.thisPage.nowPage][nowThis.nowid][5] = _root.textEditMC.textEdits.textTT.text;
							_root.textEditMC.removeMovieClip();
							_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc.setSize(16, _root.objectInner["object"+nowThis.nowid]["object"].my_sb._height);
							if (_root.objectInner["object"+nowThis.nowid]["object"].my_sb.textHeight>_root.objectInner["object"+nowThis.nowid]["object"].my_sb._height) {
								_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc._alpha = 100;
							} else {
								_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc._alpha = 0;
							}
						};
						_root.textEditMC.textEdits.clearT.onRelease = function() {
							_root.textEditMC.textEdit.textTT.text = "";
						};
						_root.textEditMC.textEdits.offButton.onRelease = function() {
							_root.textEditMC.removeMovieClip();
						};
					}
				};
			}
			//*text类型重建*//                 
		}
	}
}
