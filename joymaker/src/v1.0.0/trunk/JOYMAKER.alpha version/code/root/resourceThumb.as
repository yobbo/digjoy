﻿class resourceThumb extends rmcName {
	private var thY:Number;//高度；
	private var kin:String;//类型；
	private var thisUrl:String;//链接地址；
	public function resourceThumb(thumbY:Number, kinds:String, tUrl:String) {
		thY = thumbY;
		kin = kinds;
		thisUrl = tUrl;
	}
	//*图片显示缩略图*//
	public function startThumb() {
		_root.createEmptyMovieClip("resoT",20000);
		var shadowNow:flash.filters.DropShadowFilter = new flash.filters.DropShadowFilter(10, 90, 0x000000, 0.8, 16, 16, 1, 3, false, false, false);
		_root.resoT.attachMovie("resourceThumb", "resourceThumbT", _root.getNextHighestDepth());
		_root.resoT.resourceThumbT.filters = [shadowNow];
		//初始化显示面板；
		_root.resoT.resourceThumbT._y = thY+70+_root[rP[0]]._y;
		if (_root[rP[0]]._x>250) {
			_root.resoT._x = _root[rP[0]]._x-_root.resoT._width;
		} else {
			_root.resoT._x = _root[rP[0]]._x+_root[rP[0]]._width;
		}
		beginShow();
	}
	public function endThumbs() {
		_root.resoT.removeMovieClip();
	}
	//*图片显示缩略图*//
	public function beginShow() {
		if (kin == "rImportP") {
			picAndSwfShow();
		} else if (kin == "rImportF") {
			picAndSwfShow();
		} else if (kin == "rImportM") {
			musicPlay();
		}
	}
	//*图片与swf显示；*//
	private function picAndSwfShow() {
		var mclListener:Object = new Object();
		mclListener.onLoadInit = function(target_mc:MovieClip) {
			var w:Number = target_mc._width;
			var h:Number = target_mc._height;
			if (w>h) {
				if (w != 226) {
					var bilv:Number = 226/w;
					target_mc._width = 226;
					target_mc._height = bilv*h;
				}
			} else if (h>w) {
				if (h != 186) {
					var bilv:Number = 186/w;
					target_mc._height = 186;
					target_mc._width = bilv*w;
				}
			}
		};
		var image_mcl:MovieClipLoader = new MovieClipLoader();
		image_mcl.addListener(mclListener);
		image_mcl.loadClip(thisUrl,_root.resoT.resourceThumbT.picLocation);
	}
	//*图片与swf显示；*//
	//*音乐播放*//
	public function musicPlay() {
		var my_sound:Sound = new Sound();
		my_sound.onLoad = function(success:Boolean) {
			if (success) {
				my_sound.start();
			}
		};
		my_sound.loadSound(thisUrl, true);
	}
	public function endMusic() {
		var my_sound:Sound = new Sound();
		my_sound.stop();
	}
	//*音乐播放*//
}
