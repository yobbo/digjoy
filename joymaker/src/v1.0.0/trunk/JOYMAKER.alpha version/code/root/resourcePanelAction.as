﻿//import beginResourceAction;
import resourceList;
import helpPanelList;
class resourcePanelAction extends rmcName {
	private var initializeX:Number;
	private var initializeY:Number;
	private var panelX:Number;
	private var panelY:Number;
	private var defaultPanel:Array;
	private var rS:Array;
	public function resourcePanelAction() {
		rS = new Array(new Array("defaultR", "默认素材"), new Array("personalR", "个人素材"));
		var nowThis:resourcePanelAction = this;
		dragPanel();
		defaultPanel = rS[0];
		var newresourceList:resourceList = new resourceList(defaultPanel);
		refurbishPanel();
		_root[rP[0]][rP[7]].arrayP = 7;
		_root[rP[0]][rP[7]].onRollOver = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.getHelp(nowThis.rP[this.arrayP]);
			this.gotoAndStop(2);
		};
		_root[rP[0]][rP[7]].onRollOut = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			this.gotoAndStop(1);
		};
		_root[rP[0]][rP[7]].onPress = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			this.gotoAndStop(3);
		};
		_root[rP[0]][rP[7]].onRelease = function() {
			_root[nowThis.rP[0]]._x = -500;
			_root[nowThis.rP[0]]._y = -500;
		};
		_root[rP[0]][rP[7]].onReleaseOutside = function() {
			this.gotoAndStop(1);
		};
		_root[rP[0]][rP[9]].arrayP = 9;
		_root[rP[0]][rP[9]].onRollOver = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.getHelp(nowThis.rP[this.arrayP]);
			this.gotoAndStop(2);
		};
		_root[rP[0]][rP[9]].onRollOut = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			this.gotoAndStop(1);
		};
		_root[rP[0]][rP[9]].onPress = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			this.gotoAndStop(2);
		};
		_root[rP[0]][rP[9]].onRelease = function() {
			fscommand("UPFILES", "TRUE");
		};
		_root[rP[0]][rP[9]].onReleaseOutside = function() {
			this.gotoAndStop(1);
		};
	}
	private function refurbishPanel():Void {
		var thisT:resourcePanelAction = this;
		var rightMenuX:Number =-22.7;
		var rightMenuY:Number = 10;
		var nowP:Number = 0;
		for (var i:Number = 0; i<rS.length; i++) {
			_root["rightMenuArray"+i] = new Array();
			_root["rightMenuArray"+i] = rS[i];
			_root[rP[0]].rightMenu.attachMovie("otherRs", "otherRss"+i, i);
			if (defaultPanel[0] != rS[i][0]) {
				_root[rP[0]].rightMenu["otherRss"+i]._x = rightMenuX-94.5;
				_root[rP[0]].rightMenu["otherRss"+i]._y += rightMenuY+i*95;
			} else {
				_root[rP[0]].rightMenu["otherRss"+i]._x = rightMenuX-94.5;
				_root[rP[0]].rightMenu["otherRss"+i]._y += rightMenuY+i*95;
			}
			_root[rP[0]].rightMenu["otherRss"+i].menuNameText.text = _root["rightMenuArray"+i][1];
			_root[rP[0]].rightMenu["otherRss"+i].op = _root["rightMenuArray"+i];
			_root[rP[0]].rightMenu["otherRss"+i].onRelease = function() {
				thisT.setToolAction(this.op);
				_root.resource.kinds = this.op[0];
				thisT.clearRightMenu();
				thisT.refurbishPanel();
			};
			//加载右边菜单；   
		}
		_root[rP[0]].nowT.text = defaultPanel[1];
	}
	public function clearRightMenu():Void {
		for (var i:Number = 0; i<rS.length; i++) {
			_root[rP[0]].rightMenu["otherRss"+i].removeMovieClip();
		}
	}
	//清除右边菜单；
	private function setToolAction(newArray:Array):Void {
		defaultPanel = newArray;
		var newresourceList:resourceList = new resourceList(defaultPanel);
	}
	//设置动作方法名称；
	private function dragPanel():Void {
		var nowThist:resourcePanelAction = this;
		_root[rP[0]][rP[8]].arrayP = 8;
		_root[rP[0]][rP[8]].onRollOver = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.getHelp(nowThist.rP[this.arrayP]);
		};
		_root[rP[0]][rP[8]].onRollOut = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
		};
		_root[rP[0]][rP[8]].onPress = function() {
			var newhelpPanelList:helpPanelList = new helpPanelList();
			newhelpPanelList.deleteTip();
			this._parent.startDrag();
		};
		_root[rP[0]][rP[8]].onRelease = function() {
			this._parent.stopDrag();
		};
		_root[rP[0]][rP[8]].onReleaseOutside = function() {
			this._parent.stopDrag();
		};
	}
	private function createpxpCC():Void {
		panelX = _root[rP[0]]._x;
		panelY = _root[rP[0]]._y;
		var nowThist:resourcePanelAction = this;
		_root.createEmptyMovieClip("pxpCC", _root.getNextHighestDepth());
		_root.pxpCC._x = panelX;
		_root.pxpCC._y = panelY;
		_root.pxpCC.createEmptyMovieClip("containerXp", 1);
		//新建立一个空的movieClip；
		_root.pxpCC._alpha = 0;
		_root.pxpCC.containerXp.beginFill(0xFF0000, 0);
		_root.pxpCC.containerXp.lineStyle(1, 0xFF0000, 100, true, "none", "round", "round", 1);
		_root.pxpCC.containerXp.moveTo(0, 0);
		_root.pxpCC.containerXp.lineTo(_root[rP[0]]._width, 0);
		_root.pxpCC.containerXp.lineTo(_root[rP[0]]._width, _root[rP[0]]._height);
		_root.pxpCC.containerXp.lineTo(0, _root[rP[0]]._height);
		_root.pxpCC.containerXp.lineTo(0, 0);
		_root.pxpCC.containerXp.endFill();
		_root.pxpCC.onRollOut = function() {
			nowThist.deletepxpCC();
		};
		_root.pxpCC.onPress = function() {
			this._alpha = 100;
			this.startDrag();
		};
		_root.pxpCC.onRelease = function() {
			nowThist.stopDragGetXY();
		};
		_root.pxpCC.onReleaseOutside = function() {
			nowThist.stopDragGetXY();
		};
	}
	private function deletepxpCC():Void {
		_root.pxpCC._visible = false;
	}
	private function stopDragGetXY():Void {
		initializeX = _root.pxpCC._x;
		initializeY = _root.pxpCC._y;
		_root.pxpCC.stopDrag();
		_root.pxpCC._visible = false;
		_root[rP[0]]._x = initializeX;
		_root[rP[0]]._y = initializeY;
	}
}
