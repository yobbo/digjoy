﻿import arrays;
import resourceThumb;
import basicFunction;
import exportXML;
import mx.controls.UIScrollBar;
class createNewAndAddArr implements common {
	private var testtext:String;
	private var nowid:Number;
	private var max:Array;
	private var txstation:Number;
	private var tystation:Number;
	private var arrayt:Array;
	public function createNewAndAddArr() {
		var newarrays:arrays = new arrays();
		max = newarrays.getMaxPageArr();
	}
	public function createObjects(getArray:Array):Void {
		_root.CHANGED = "TRUE";
		_root["page"+_root.thisPage.nowPage].push(getArray);
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
	}
	public function createObjectsI(urlAddress:String, xstation:Number, ystation:Number):Void {
		var thisNew:Array = new Array();
		var nowThis:createNewAndAddArr = this;
		var newarrays:arrays = new arrays();
		_root.objectInner.createEmptyMovieClip("object"+newarrays.getNowPageArr().length, newarrays.getNowPageArr().length);
		_root.objectInner["object"+newarrays.getNowPageArr().length].id = newarrays.getNowPageArr().length;
		_root.objectInner["object"+newarrays.getNowPageArr().length].createEmptyMovieClip("object", 0);
		var mclListener:Object = new Object();
		mclListener.onLoadInit = function(target_mc:MovieClip) {
			target_mc._x = -target_mc._width/2;
			target_mc._y = -target_mc._height/2;
		};
		var image_mcl:MovieClipLoader = new MovieClipLoader();
		image_mcl.addListener(mclListener);
		image_mcl.loadClip(urlAddress, _root.objectInner["object"+newarrays.getNowPageArr().length]["object"]);
		_root.objectInner["object"+newarrays.getNowPageArr().length]._x = xstation;
		_root.objectInner["object"+newarrays.getNowPageArr().length]._y = ystation;
		thisNew = ["load", 0, 0, _root.objectInner["object"+newarrays.getNowPageArr().length]._x, _root.objectInner["object"+newarrays.getNowPageArr().length]._y, urlAddress, newarrays.getNowPageArr().length, 0, 100, 100, 100, 0, 0, 0, 0];
		createObjects(thisNew);
	}
	public function createMusics(getArray:Array):Void {
		_root["page"+_root.thisPage.nowPage].push(getArray);
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
	}
	public function createMusicsI(urlAddress:String, xstation:Number, ystation:Number):Void {
		_root.CHANGED = "TRUE";
		var haveMusic:Boolean = false;
		for (var i:Number = 0; i<_root["page"+_root.thisPage.nowPage].length; i++) {
			if (_root["page"+_root.thisPage.nowPage][i][0] == "music") {
				haveMusic = true;
			}
		}
		if (haveMusic == false) {
			var thisNew:Array = new Array();
			var nowThis:createNewAndAddArr = this;
			var newarrays:arrays = new arrays();
			_root.objectInner.createEmptyMovieClip("object"+newarrays.getNowPageArr().length, newarrays.getNowPageArr().length);
			_root.objectInner["object"+newarrays.getNowPageArr().length].id = newarrays.getNowPageArr().length;
			_root.objectInner["object"+newarrays.getNowPageArr().length].createEmptyMovieClip("object", 0);
			_root.objectInner["object"+newarrays.getNowPageArr().length].createEmptyMovieClip("playb", 1);
			_root.objectInner["object"+newarrays.getNowPageArr().length].createEmptyMovieClip("stopb", 2);
			_root.objectInner["object"+newarrays.getNowPageArr().length].musicUrl = urlAddress;
			_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].attachMovie("musicLogo", "musicLogo", newarrays.getNowPageArr().length);
			_root.objectInner["object"+newarrays.getNowPageArr().length]["playb"].attachMovie("buttonPlay", "mplay", newarrays.getNowPageArr().length);
			_root.objectInner["object"+newarrays.getNowPageArr().length]["stopb"].attachMovie("buttonStop", "mstop", newarrays.getNowPageArr().length);
			_root.objectInner["object"+newarrays.getNowPageArr().length]._x = xstation;
			_root.objectInner["object"+newarrays.getNowPageArr().length]._y = ystation;
			_root.objectInner["object"+newarrays.getNowPageArr().length]["object"]._x = -_root.objectInner["object"+newarrays.getNowPageArr().length]["object"]._width/2;
			_root.objectInner["object"+newarrays.getNowPageArr().length]["object"]._y = -_root.objectInner["object"+newarrays.getNowPageArr().length]["object"]._height/2;
			_root.objectInner["object"+newarrays.getNowPageArr().length]["playb"]._x = -_root.objectInner["object"+newarrays.getNowPageArr().length]["object"]._width/2+_root.objectInner["object"+newarrays.getNowPageArr().length]["playb"]._width;
			_root.objectInner["object"+newarrays.getNowPageArr().length]["playb"]._y = -_root.objectInner["object"+newarrays.getNowPageArr().length]["object"]._height/2+_root.objectInner["object"+newarrays.getNowPageArr().length]["object"]._height;
			_root.objectInner["object"+newarrays.getNowPageArr().length]["playb"]._rotation = 90;
			_root.objectInner["object"+newarrays.getNowPageArr().length]["playb"].onRollOver = function() {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.setMouseD();
				this.mplay.gotoAndStop(2);
			};
			_root.objectInner["object"+newarrays.getNowPageArr().length]["playb"].onRollOut = function() {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.getMouseD();
				this.mplay.gotoAndStop(1);
			};
			_root.objectInner["object"+newarrays.getNowPageArr().length]["playb"].onRelease = function() {
				var newresourceThumb:resourceThumb = new resourceThumb(0, "rImportM", this._parent.musicUrl);
				newresourceThumb.beginShow();
				this.mplay.gotoAndStop(1);
			};
			_root.objectInner["object"+newarrays.getNowPageArr().length]["playb"].onReleaseOutside = function() {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.getMouseD();
				this.mplay.gotoAndStop(1);
			};
			_root.objectInner["object"+newarrays.getNowPageArr().length]["stopb"]._x = _root.objectInner["object"+newarrays.getNowPageArr().length]["playb"]._x;
			_root.objectInner["object"+newarrays.getNowPageArr().length]["stopb"]._y = _root.objectInner["object"+newarrays.getNowPageArr().length]["playb"]._y;
			_root.objectInner["object"+newarrays.getNowPageArr().length]["stopb"].onRollOver = function() {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.setMouseD();
				this.mstop.gotoAndStop(2);
			};
			_root.objectInner["object"+newarrays.getNowPageArr().length]["stopb"].onRollOut = function() {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.getMouseD();
				this.mstop.gotoAndStop(1);
			};
			_root.objectInner["object"+newarrays.getNowPageArr().length]["stopb"].onRelease = function() {
				var newresourceThumb:resourceThumb = new resourceThumb(0, "rImportM", this._parent.musicUrl);
				newresourceThumb.endMusic();
				this.mstop.gotoAndStop(1);
			};
			_root.objectInner["object"+newarrays.getNowPageArr().length]["stopb"].onReleaseOutside = function() {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.getMouseD();
				this.mstop.gotoAndStop(1);
			};
			var getChart:String;
			var startChart:Number;
			startChart = urlAddress.lastIndexOf("\\");
			getChart = urlAddress.slice(startChart+1, urlAddress.length);
			_root.objectInner["object"+newarrays.getNowPageArr().length]["object"]["musicLogo"].musicName.text = getChart;
			thisNew = ["music", 0, 0, _root.objectInner["object"+newarrays.getNowPageArr().length]._x, _root.objectInner["object"+newarrays.getNowPageArr().length]._y, urlAddress, newarrays.getNowPageArr().length, 0, 100, 100, 100, 0, 0, 0, 0];
			createMusics(thisNew);
		} else {
			var newbasicFunction:basicFunction = new basicFunction();
			newbasicFunction.alert("每个页面中只允许出现一首音乐"+newline+"如果你要更新本页的音乐，请先删除本页音乐");
		}
	}
	public function createDrawCircle(getArray:Array):Void {

		_root["page"+_root.thisPage.nowPage].push(getArray);
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
	}
	public function createDrawCircleI(lineW:Number, lineColor:String, innerColor:String, xstation:Number, ystation:Number, xwidth:Number, yheight:Number, rr:Number,xsc:Number,ysc:Number):Void {
		_root.CHANGED = "TRUE";
		var nowThis:createNewAndAddArr = this;
		var newarrays:arrays = new arrays();
		nowid = newarrays.getNowPageArr().length;
		_root.objectInner.createEmptyMovieClip("object"+nowid, nowid);
		_root.objectInner["object"+nowid].id = nowid;
		_root.objectInner["object"+nowid].createEmptyMovieClip("object", 0);
		_root.objectInner["object"+nowid]["object"].beginFill(innerColor, 100);
		_root.objectInner["object"+nowid]["object"].lineStyle(lineW, lineColor, 100, true, "none", "round", "round", 1);
		var scale:Number = Math.cos(Math.PI/8);
		_root.objectInner["object"+nowid]["object"].moveTo(rr, 0);
		for (var i:Number = Math.PI/4; i<=Math.PI*2; i += Math.PI/4) {
			var cx = Math.cos(i-Math.PI/8)*rr/scale;
			var cy = Math.sin(i-Math.PI/8)*rr/scale;
			var ax = Math.cos(i)*rr;
			var ay = Math.sin(i)*rr;
			_root.objectInner["object"+nowid]["object"].curveTo(cx, cy, ax, ay);
		}
		_root.objectInner["object"+nowid]["object"].endFill();

		_root.objectInner["object"+nowid].linew = lineW;
		_root.objectInner["object"+nowid].linec = lineColor;
		_root.objectInner["object"+nowid].iinec = innerColor;
		_root.objectInner["object"+nowid]["object"]._xscale = xsc;
		_root.objectInner["object"+nowid]["object"]._yscale = ysc;
		trace(_root.objectInner["object"+nowid]["object"]._xscale);
		trace(_root.objectInner["object"+nowid]["object"]._yscale)
		_root.objectInner["object"+nowid]._x = xstation;
		_root.objectInner["object"+nowid]._y = ystation;
		arrayt = ["draw_circle", 0, 0, _root.objectInner["object"+nowThis.nowid]._x, _root.objectInner["object"+nowThis.nowid]._y, 0, nowThis.nowid, 0, 100, xsc, ysc, lineColor, lineW, innerColor, rr];
		createDrawCircle(arrayt);
	}
	public function createDrawRect(getArray:Array):Void {
		_root["page"+_root.thisPage.nowPage].push(getArray);
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
	}
	public function createDrawRectI(lineW:Number, lineColor:String, innerColor:String, xstation:Number, ystation:Number, xwidth:Number, yheight:Number):Void {
		_root.CHANGED = "TRUE";
		var nowThis:createNewAndAddArr = this;
		var newarrays:arrays = new arrays();
		nowid = newarrays.getNowPageArr().length;
		_root.objectInner.createEmptyMovieClip("object"+nowid, nowid);
		_root.objectInner["object"+nowid].id = nowid;
		_root.objectInner["object"+nowid]._x = xstation;
		_root.objectInner["object"+nowid]._y = ystation;
		_root.objectInner["object"+nowid].createEmptyMovieClip("object", 0);
		_root.objectInner["object"+nowid]["object"].beginFill(innerColor, 100);
		_root.objectInner["object"+nowid]["object"].lineStyle(lineW, lineColor, 100, true, "none", "round", "round", 1);
		_root.objectInner["object"+nowid]["object"].moveTo(0, 0);
		_root.objectInner["object"+nowid]["object"].lineTo(xwidth, 0);
		_root.objectInner["object"+nowid]["object"].lineTo(xwidth, yheight);
		_root.objectInner["object"+nowid]["object"].lineTo(0, yheight);
		_root.objectInner["object"+nowid]["object"].lineTo(0, 0);
		_root.objectInner["object"+nowid]["object"].endFill();
		_root.objectInner["object"+nowid]["object"].linew = lineW;
		_root.objectInner["object"+nowid]["object"].linec = lineColor;
		_root.objectInner["object"+nowid]["object"].iinec = innerColor;
		_root.objectInner["object"+nowid]["object"]._x = -_root.objectInner["object"+nowid]["object"]._width/2;
		_root.objectInner["object"+nowid]["object"]._y = -_root.objectInner["object"+nowid]["object"]._height/2;
		arrayt = ["draw_rect", _root.objectInner["object"+nowThis.nowid]["object"]._width, _root.objectInner["object"+nowThis.nowid]["object"]._height, _root.objectInner["object"+nowThis.nowid]._x, _root.objectInner["object"+nowThis.nowid]._y, 0, nowThis.nowid, 0, 100, 100, 100, lineColor, lineW, innerColor, 0];
		createDrawRect(arrayt);
	}
	public function createText(getArray:Array):Void {
		_root["page"+_root.thisPage.nowPage].push(getArray);
		var newexportXML:exportXML = new exportXML();
		_root.XMLSTRING = newexportXML.exportRunXML();
		_root.FILELIST = newexportXML.exportFileListXML();
		_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
	}
	public function createTextI(xstation:Number, ystation:Number, xwidth:Number, yheight:Number, textC:String):Void {
		_root.CHANGED = "TRUE";
		var nowThis:createNewAndAddArr = this;
		var thisNew:Array = new Array();
		var newarrays:arrays = new arrays();
		nowid = newarrays.getNowPageArr().length;
		_root.objectInner.createEmptyMovieClip("object"+newarrays.getNowPageArr().length, newarrays.getNowPageArr().length);
		_root.objectInner["object"+newarrays.getNowPageArr().length].id = newarrays.getNowPageArr().length;
		_root.objectInner["object"+newarrays.getNowPageArr().length].createEmptyMovieClip("object", 0);
		_root.objectInner["object"+newarrays.getNowPageArr().length].createEmptyMovieClip("scrollb", 1);
		_root.objectInner["object"+newarrays.getNowPageArr().length].createEmptyMovieClip("bg", 2);
		_root.objectInner["object"+newarrays.getNowPageArr().length].createEmptyMovieClip("scale", 3);
		_root.objectInner["object"+newarrays.getNowPageArr().length].createEmptyMovieClip("textedit", 4);
		_root.objectInner["object"+newarrays.getNowPageArr().length].texts = textC;
		_root.objectInner["object"+newarrays.getNowPageArr().length]._x = xstation-xwidth/2;
		_root.objectInner["object"+newarrays.getNowPageArr().length]._y = ystation-yheight/2;
		_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].createTextField("my_sb", 0, 0, 0, xwidth, yheight);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.html = true;
		_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.multiline = true;
		_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.wordWrap = true;
		_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.htmlText = textC;
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scrollb"].createClassObject(mx.controls.UIScrollBar, "my_sc", 0);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scrollb"].my_sc.setScrollTarget(_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scrollb"].my_sc.setSize(16, _root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb._height);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scrollb"].my_sc.move(_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb._x+_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb._width, _root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb._y);
		if (_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.textHeight>_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb._height) {
			_root.objectInner["object"+newarrays.getNowPageArr().length]["scrollb"].my_sc._alpha = 100;
		} else {
			_root.objectInner["object"+newarrays.getNowPageArr().length]["scrollb"].my_sc._alpha = 0;
		}
		_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"].beginFill(0xFF0000, 0);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"].lineStyle(1, 0x00CCFF, 100, true, "none", "round", "round", 1);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"].moveTo(0, 0);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"].lineTo(_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.width, 0);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"].lineTo(_root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.width, _root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.height);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"].lineTo(0, _root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.height);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"].lineTo(0, 0);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"].endFill();
		//_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"]._x = -(_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"]._width/2);
		//_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"]._y = -(_root.objectInner["object"+newarrays.getNowPageArr().length]["bg"]._height/2);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scale"].attachMovie("toolScale", "toolScaleT", 0);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scale"]._x = 0;
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scale"]._y = _root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb.height;
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scale"].onRollOver = function() {
			if (_root.toolState.nowTool == "toolSelect") {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.setMouseD();
			}
		};
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scale"].onRollOut = function() {
			if (_root.toolState.nowTool == "toolSelect") {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.getMouseD();
			}
		};
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scale"].onPress = function() {
			if (_root.toolState.nowTool == "toolSelect") {
				_root.objectInner["object"+nowThis.nowid]["bg"].nowX = _root._xmouse;
				_root.objectInner["object"+nowThis.nowid]["bg"].nowY = _root._ymouse;
				_root.objectInner["object"+nowThis.nowid]["bg"].nowSxscale = _root.objectInner["object"+nowThis.nowid]["bg"]._xscale;
				_root.objectInner["object"+nowThis.nowid]["bg"].nowSyscale = _root.objectInner["object"+nowThis.nowid]["bg"]._yscale;
				_root.objectInner["object"+nowThis.nowid]["bg"].onEnterFrame = function() {
					this._xscale = _root._xmouse-this.nowX+this.nowSxscale;
					this._yscale = _root._ymouse-this.nowY+this.nowSyscale;
					_root.objectInner.opq.object._width = this._width;
					_root.objectInner.opq.object._height = this._height;
					_root.objectInner["object"+nowThis.nowid]["object"].my_sb._width = this._width;
					_root.objectInner["object"+nowThis.nowid]["object"].my_sb._height = this._height;
					_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc.setSize(16, _root.objectInner["object"+nowThis.nowid]["object"].my_sb._height);
					_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc.move(this._x+this._width, this._y);
					if (_root.objectInner["object"+nowThis.nowid]["object"].my_sb.textHeight>_root.objectInner["object"+nowThis.nowid]["object"].my_sb._height) {
						_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc._alpha = 100;
					} else {
						_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc._alpha = 0;
					}
					_root.objectInner["object"+nowThis.nowid]["scale"]._x = 0;
					_root.objectInner["object"+nowThis.nowid]["scale"]._y = _root.objectInner["object"+nowThis.nowid]["object"].my_sb.height;
					_root.objectInner["object"+nowThis.nowid]["textedit"]._x =_root.objectInner["object"+nowThis.nowid]["scale"]._width;
					_root.objectInner["object"+nowThis.nowid]["textedit"]._y =_root.objectInner["object"+nowThis.nowid]["scale"]._y;
					_root["page"+_root.thisPage.nowPage][nowThis.nowid][1] = this._width;
					_root["page"+_root.thisPage.nowPage][nowThis.nowid][2] = this._height;
					var newexportXML:exportXML = new exportXML();
					_root.XMLSTRING = newexportXML.exportRunXML();
					_root.FILELIST = newexportXML.exportFileListXML();
					_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
				};
			}
		};
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scale"].onRelease = function() {
			delete _root.objectInner["object"+nowThis.nowid]["bg"].onEnterFrame;
			var newbasicFunction:basicFunction = new basicFunction();
			newbasicFunction.getMouseD();
		};
		_root.objectInner["object"+newarrays.getNowPageArr().length]["scale"].onReleaseOutside = function() {
			delete _root.objectInner["object"+nowThis.nowid]["bg"].onEnterFrame;
			var newbasicFunction:basicFunction = new basicFunction();
			newbasicFunction.getMouseD();
		};
		_root.objectInner["object"+newarrays.getNowPageArr().length]["textedit"].attachMovie("toolInputText", "toolInputText", 0);
		_root.objectInner["object"+newarrays.getNowPageArr().length]["textedit"]._x =_root.objectInner["object"+nowThis.nowid]["scale"]._width;
		_root.objectInner["object"+newarrays.getNowPageArr().length]["textedit"]._y = _root.objectInner["object"+nowThis.nowid]["scale"]._y;
		_root.objectInner["object"+newarrays.getNowPageArr().length]["textedit"].onRollOver = function() {
			if (_root.toolState.nowTool == "toolSelect") {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.setMouseD();
			}
		};
		_root.objectInner["object"+newarrays.getNowPageArr().length]["textedit"].onRollOut = function() {
			if (_root.toolState.nowTool == "toolSelect") {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.getMouseD();
			}
		};
		_root.objectInner["object"+newarrays.getNowPageArr().length]["textedit"].onRelease = function() {
			if (_root.toolState.nowTool == "toolSelect") {
				var newbasicFunction:basicFunction = new basicFunction();
				newbasicFunction.getMouseD();
				_root.createEmptyMovieClip("textEditMC", 20000);
				_root.textEditMC.attachMovie("textEdit", "textEdits", 1);
				_root.textEditMC._x = 300;
				_root.textEditMC._y = 200;
				var shadowNow:flash.filters.DropShadowFilter = new flash.filters.DropShadowFilter(10, 90, 0x000000, 0.8, 16, 16, 1, 3, false, false, false);
				_root.textEditMC.filters = [shadowNow];
				nowThis.testtext = _root["page"+_root.thisPage.nowPage][nowThis.nowid][5];
				var id = setInterval(function () {
					_root.textEditMC.textEdits.textTT.text = nowThis.testtext;
					clearInterval(id);
				}, 1000);
				_root.textEditMC.textEdits.succT.onRelease = function() {
					_root.objectInner["object"+nowThis.nowid]["object"].my_sb.htmlText = _root.textEditMC.textEdits.textTT.text;
					_root["page"+_root.thisPage.nowPage][nowThis.nowid][5] = _root.textEditMC.textEdits.textTT.text;
					_root.textEditMC.removeMovieClip();
					_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc.setSize(16, _root.objectInner["object"+nowThis.nowid]["object"].my_sb._height);
					if (_root.objectInner["object"+nowThis.nowid]["object"].my_sb.textHeight>_root.objectInner["object"+nowThis.nowid]["object"].my_sb._height) {
						_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc._alpha = 100;
					} else {
						_root.objectInner["object"+nowThis.nowid]["scrollb"].my_sc._alpha = 0;
					}
					var newexportXML:exportXML = new exportXML();
					_root.XMLSTRING = newexportXML.exportRunXML();
					_root.FILELIST = newexportXML.exportFileListXML();
					_root.PROJECT_XMLSTRING = newexportXML.exportProjectRunXML();
				};
				_root.textEditMC.textEdits.clearT.onRelease = function() {
					_root.textEditMC.textEdit.textTT.text = "";
				};
				_root.textEditMC.textEdits.offButton.onRelease = function() {
					_root.textEditMC.removeMovieClip();
				};
			}
		};
		thisNew = ["txt", _root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb._width, _root.objectInner["object"+newarrays.getNowPageArr().length]["object"].my_sb._height, _root.objectInner["object"+newarrays.getNowPageArr().length]._x, _root.objectInner["object"+newarrays.getNowPageArr().length]._y, textC, newarrays.getNowPageArr().length, 0, 0, 0, 0, 0, 0, 0, 0];
		createText(thisNew);
	}
}
