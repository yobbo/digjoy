﻿//这个类是实现新建立通用方法中的接口;
interface common {
	function createObjects(getArray:Array):Void;
	//工具建立物件在数组中的方法,此属于load类；
	function createObjectsI(urlAddress:String, xstation:Number, ystation:Number):Void;
	//工具建立物体实际方法,此属于load类,传递三个值 url 地址,x坐标与y坐标；
	function createMusics(getArray:Array):Void;
	//建立物件在数组中的方法，此属于 music 类；
	function createMusicsI(urlAddress:String, xstation:Number, ystation:Number):Void;
	//工具建立物体的实际方法，此属于 music 类；
	function createDrawCircle(getArray:Array):Void;
	//建立物件在数组中的方法，此属于 drawCircle 类；
	function createDrawCircleI(lineW:Number, lineColor:String, innerColor:String, xstation:Number, ystation:Number, xwidth:Number, yheight:Number, r:Number, xsc:Number, ysc:Number):Void;
	//建立物件的实际方法，此属于 drawCircle 类；
	function createDrawRect(getArray:Array):Void;
	//建立物件在数组中的方法，此属于 drawRect 类；
	function createDrawRectI(lineW:Number, lineColor:String, innerColor:String, xstation:Number, ystation:Number, xwidth:Number, yheight:Number):Void;
	//建立物件的实际方法，此属于 drawRect 类；
	function createText(getArray:Array):Void;
	//建立物件在数组中的方法，此属于 text 类；
	function createTextI(xstation:Number, ystation:Number, xwidth:Number, yheight:Number, textC:String):Void;
	//建立物件的实际方法，此属于 text类；
}
