﻿class controlSelectProjectMc {
	private var blurXT:Number = 100;
	private var blurYT:Number = 100;
	private var blurQT:Number = 3;
	private var selectMenu:Array = ["magazine_project", "album_project", "open_project"];
	public function controlSelectProjectMc() {
		var idt = setInterval(function () {
			if (_root.OPENFILE == "TRUE") {
				if (_root.OPENPROJECT_STRING != null) {
					clearInterval(idt);
					_root.gotoAndStop("beginWork");
				}
			}
		},300);
		var blur:flash.filters.BlurFilter = new flash.filters.BlurFilter(blurXT, blurYT, blurQT);
		_root.magazine_project.filters = [blur];
		_root.album_project.filters = [blur];
		_root.open_project.filters = [blur];
		magazine_project_alpha_asc();
		setInterval(this, "album_project_alpha_asc", 500);
		setInterval(this, "open_project_alpha_asc", 1000);
		_root.magazine_project.onRelease = function() {
			this._xscale += 20;
			this._yscale += 20;
			_root.sStencil.select = "magazine";
			_root.gotoAndStop("selectStencil");
		};
		_root.album_project.onRelease = function() {
			this._xscale += 20;
			this._yscale += 20;
			_root.sStencil.select = "album";
			_root.gotoAndStop("selectStencil");
		};
		_root.open_project.onRelease = function() {
			this._xscale += 20;
			this._yscale += 20;
			fscommand("OPEN_BG", "TRUE");
		};
		for (var i:Number = 0; i<selectMenu.length; i++) {
			_root[selectMenu[i]].onPress = function() {
				this._xscale -= 20;
				this._yscale -= 20;
				this.gotoAndStop(2);
			};
			_root[selectMenu[i]].onRollOut = function() {
				this._xscale -= 10;
				this._yscale -= 10;
				this.gotoAndStop(1);
			};
			_root[selectMenu[i]].onRollOver = function() {
				this._xscale += 10;
				this._yscale += 10;
				this.gotoAndStop(2);
			};
			_root[selectMenu[i]].onReleaseOutside = function() {
				this._xscale += 10;
				this._yscale += 10;
				this.gotoAndStop(1);
			};
		}
	}
	public function magazine_project_alpha_asc() {
		var TcontrolSelectProjectMc:controlSelectProjectMc = this;
		_root.magazine_project.onEnterFrame = function() {
			TcontrolSelectProjectMc.blurXT -= 10;
			TcontrolSelectProjectMc.blurYT -= 10;
			var blurArray:Array = new Array();
			this._alpha += 10;
			blurArray.push(TcontrolSelectProjectMc.blurXT, TcontrolSelectProjectMc.blurYT, TcontrolSelectProjectMc.blurQT);
			this.filters = [blurArray];
			if (this._alpha>=100) {
				delete this.onEnterFrame;
			}
		};
	}
	public function album_project_alpha_asc() {
		var TcontrolSelectProjectMc:controlSelectProjectMc = this;
		_root.album_project.onEnterFrame = function() {
			TcontrolSelectProjectMc.blurXT -= 10;
			TcontrolSelectProjectMc.blurYT -= 10;
			var blurArray:Array = new Array();
			this._alpha += 10;
			blurArray.push(TcontrolSelectProjectMc.blurXT, TcontrolSelectProjectMc.blurYT, TcontrolSelectProjectMc.blurQT);
			this.filters = [blurArray];
			if (this._alpha>=100) {
				delete this.onEnterFrame;
			}
		};
	}
	public function open_project_alpha_asc() {
		var TcontrolSelectProjectMc:controlSelectProjectMc = this;
		_root.open_project.onEnterFrame = function() {
			TcontrolSelectProjectMc.blurXT -= 10;
			TcontrolSelectProjectMc.blurYT -= 10;
			var blurArray:Array = new Array();
			this._alpha += 10;
			blurArray.push(TcontrolSelectProjectMc.blurXT, TcontrolSelectProjectMc.blurYT, TcontrolSelectProjectMc.blurQT);
			this.filters = [blurArray];
			if (this._alpha>=100) {
				delete this.onEnterFrame;
			}
		};
	}
}
